CREATE
OR REPLACE FUNCTION create_product_item(
    _product_id INTEGER,
    _name VARCHAR,
    _value VARCHAR
) RETURNS INTEGER AS $ $ DECLARE _item_id INTEGER;

BEGIN
INSERT INTO
    product_items (product_id, name, value)
VALUES
    (_product_id, _name, _value) RETURNING id INTO _item_id;

RETURN _item_id;

END;

$ $ LANGUAGE plpgsql;

CREATE
OR REPLACE FUNCTION get_product_item(_id INTEGER) RETURNS TABLE(
    id INTEGER,
    product_id INTEGER,
    name VARCHAR,
    value VARCHAR
) AS $ $ BEGIN RETURN QUERY
SELECT
    *
FROM
    product_items
WHERE
    id = _id;

END;

$ $ LANGUAGE plpgsql;

CREATE
OR REPLACE FUNCTION get_product_items_by_product(
    _product_id INTEGER,
    _offset INTEGER,
    _limit INTEGER
) RETURNS TABLE(
    id INTEGER,
    product_id INTEGER,
    name VARCHAR,
    value VARCHAR
) AS $ $ BEGIN RETURN QUERY
SELECT
    *
FROM
    product_items
WHERE
    product_id = _product_id
ORDER BY
    id OFFSET _offset
LIMIT
    _limit;

END;

$ $ LANGUAGE plpgsql;

CREATE
OR REPLACE FUNCTION get_total_items_by_product(_product_id INTEGER) RETURNS INTEGER AS $ $ DECLARE _total INTEGER;

BEGIN
SELECT
    pc.count_item INTO _total
FROM
    product_counters pc
WHERE
    pc.product_id = _product_id;

RETURN _total;

END;

CREATE
OR REPLACE FUNCTION update_product_item(_id INTEGER, _name VARCHAR, _value VARCHAR) RETURNS VOID AS $ $ BEGIN
UPDATE
    product_items
SET
    name = _name,
    value = _value
WHERE
    id = _id;

END;

$ $ LANGUAGE plpgsql;

CREATE
OR REPLACE FUNCTION delete_product_item(_id INTEGER) RETURNS VOID AS $ $ BEGIN
DELETE FROM
    product_items
WHERE
    id = _id;

END;

$ $ LANGUAGE plpgsql;