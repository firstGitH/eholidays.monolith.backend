-- users
CREATE
OR REPLACE FUNCTION create_user_counter() RETURNS TRIGGER AS $ $ BEGIN
INSERT INTO
    user_counters (
        user_id,
        count_subscription,
        count_order,
        total_order_sum
    )
VALUES
    (NEW.id, 0, 0, 0.0);

RETURN NEW;

END;

$ $ LANGUAGE plpgsql;

CREATE TRIGGER user_counter_trigger
AFTER
INSERT
    ON users FOR EACH ROW EXECUTE PROCEDURE create_user_counter();

-- products
CREATE
OR REPLACE FUNCTION create_product_counter() RETURNS TRIGGER AS $ $ BEGIN
INSERT INTO
    product_counters (
        product_id,
        count_item,
        count_order,
        count_review,
        count_notification,
        count_subscription
    )
VALUES
    (NEW.id, 0, 0, 0, 0, 0);

RETURN NEW;

END;

$ $ LANGUAGE plpgsql;

CREATE TRIGGER product_counter_trigger
AFTER
INSERT
    ON products FOR EACH ROW EXECUTE PROCEDURE create_product_counter();

-- categories
CREATE
OR REPLACE FUNCTION create_category_counter() RETURNS TRIGGER AS $ $ BEGIN
INSERT INTO
    category_counters (category_id, count_products)
VALUES
    (NEW.id, 0);

RETURN NEW;

END;

$ $ LANGUAGE plpgsql;

CREATE TRIGGER category_counter_trigger
AFTER
INSERT
    ON categories FOR EACH ROW EXECUTE PROCEDURE create_category_counter();

-- caegory_counters 
CREATE
OR REPLACE FUNCTION update_category_product_count() RETURNS TRIGGER AS $ $ BEGIN IF (TG_OP = 'INSERT') THEN
UPDATE
    category_counters
SET
    count_products = count_products + 1
WHERE
    category_id = NEW.category_id;

ELSIF (TG_OP = 'DELETE') THEN
UPDATE
    category_counters
SET
    count_products = count_products - 1
WHERE
    category_id = OLD.category_id;

END IF;

RETURN NULL;

END;

$ $ LANGUAGE plpgsql;

CREATE TRIGGER product_category_trigger
AFTER
INSERT
    OR DELETE ON products FOR EACH ROW EXECUTE PROCEDURE update_category_product_count();

-- order
CREATE
OR REPLACE FUNCTION update_order_item_count() RETURNS TRIGGER AS $ $ BEGIN IF (TG_OP = 'INSERT') THEN
UPDATE
    orders
SET
    count_items = count_items + 1
WHERE
    id = NEW.order_id;

ELSIF (TG_OP = 'DELETE') THEN
UPDATE
    orders
SET
    count_items = count_items - 1
WHERE
    id = OLD.order_id;

END IF;

RETURN NULL;

END;

$ $ LANGUAGE plpgsql;

CREATE TRIGGER order_item_trigger
AFTER
INSERT
    OR DELETE ON order_items FOR EACH ROW EXECUTE PROCEDURE update_order_item_count();

-- user_counters
CREATE
OR REPLACE FUNCTION update_order_count() RETURNS TRIGGER AS $ $ BEGIN IF (TG_OP = 'INSERT') THEN
UPDATE
    user_counters
SET
    count_order = count_order + 1,
    total_order_sum = total_order_sum + NEW.total_cost
WHERE
    user_id = NEW.user_id;

ELSIF (TG_OP = 'DELETE') THEN
UPDATE
    user_counters
SET
    count_order = count_order - 1,
    total_order_sum = total_order_sum - OLD.total_cost
WHERE
    user_id = OLD.user_id;

END IF;

RETURN NULL;

END;

$ $ LANGUAGE plpgsql;

CREATE TRIGGER order_trigger
AFTER
INSERT
    OR DELETE ON orders FOR EACH ROW EXECUTE PROCEDURE update_order_count();

-- Триггер для таблицы order_items
CREATE
OR REPLACE FUNCTION update_product_counters_on_order_item_change() RETURNS TRIGGER AS $ $ BEGIN IF (TG_OP = 'INSERT') THEN
UPDATE
    product_counters
SET
    count_item = count_item + NEW.quantity,
    count_order = count_order + 1
WHERE
    product_id = NEW.product_id;

ELSIF (TG_OP = 'DELETE') THEN
UPDATE
    product_counters
SET
    count_item = count_item - OLD.quantity,
    count_order = count_order - 1
WHERE
    product_id = OLD.product_id;

END IF;

RETURN NULL;

END;

$ $ LANGUAGE plpgsql;

CREATE TRIGGER order_item_trigger
AFTER
INSERT
    OR DELETE ON order_items FOR EACH ROW EXECUTE PROCEDURE update_product_counters_on_order_item_change();

-- Триггер для таблицы reviews
CREATE
OR REPLACE FUNCTION update_product_counters_on_review_change() RETURNS TRIGGER AS $ $ BEGIN IF (TG_OP = 'INSERT') THEN
UPDATE
    product_counters
SET
    count_review = count_review + 1
WHERE
    product_id = NEW.product_id;

ELSIF (TG_OP = 'DELETE') THEN
UPDATE
    product_counters
SET
    count_review = count_review - 1
WHERE
    product_id = OLD.product_id;

END IF;

RETURN NULL;

END;

$ $ LANGUAGE plpgsql;

CREATE TRIGGER review_trigger
AFTER
INSERT
    OR DELETE ON reviews FOR EACH ROW EXECUTE PROCEDURE update_product_counters_on_review_change();

-- Триггер для таблицы notifications
CREATE
OR REPLACE FUNCTION update_product_counters_on_notification_change() RETURNS TRIGGER AS $ $ BEGIN IF (TG_OP = 'INSERT') THEN
UPDATE
    product_counters
SET
    count_notification = count_notification + 1
WHERE
    product_id = NEW.product_id;

ELSIF (TG_OP = 'DELETE') THEN
UPDATE
    product_counters
SET
    count_notification = count_notification - 1
WHERE
    product_id = OLD.product_id;

END IF;

RETURN NULL;

END;

$ $ LANGUAGE plpgsql;

CREATE TRIGGER notification_trigger
AFTER
INSERT
    OR DELETE ON notifications FOR EACH ROW EXECUTE PROCEDURE update_product_counters_on_notification_change();

-- Триггер для таблицы subscriptions
CREATE
OR REPLACE FUNCTION update_product_counters_on_subscription_change() RETURNS TRIGGER AS $ $ BEGIN IF (TG_OP = 'INSERT') THEN
UPDATE
    product_counters
SET
    count_subscription = count_subscription + 1
WHERE
    product_id = NEW.product_id;

ELSIF (TG_OP = 'DELETE') THEN
UPDATE
    product_counters
SET
    count_subscription = count_subscription - 1
WHERE
    product_id = OLD.product_id;

END IF;

RETURN NULL;

END;

$ $ LANGUAGE plpgsql;

CREATE TRIGGER subscription_trigger
AFTER
INSERT
    OR DELETE ON subscriptions FOR EACH ROW EXECUTE PROCEDURE update_product_counters_on_subscription_change();

-- Триггер для таблицы order_items
CREATE
OR REPLACE FUNCTION update_product_counters_on_order_item_change() RETURNS TRIGGER AS $ $ BEGIN IF (TG_OP = 'INSERT') THEN
UPDATE
    product_counters
SET
    count_item = count_item + NEW.quantity,
    count_order = count_order + 1
WHERE
    product_id = NEW.product_id;

ELSIF (TG_OP = 'DELETE') THEN
UPDATE
    product_counters
SET
    count_item = count_item - OLD.quantity,
    count_order = count_order - 1
WHERE
    product_id = OLD.product_id;

END IF;

RETURN NULL;

END;

$ $ LANGUAGE plpgsql;

CREATE TRIGGER order_item_trigger
AFTER
INSERT
    OR DELETE ON order_items FOR EACH ROW EXECUTE PROCEDURE update_product_counters_on_order_item_change();

-- Триггер для таблицы reviews
CREATE
OR REPLACE FUNCTION update_product_counters_on_review_change() RETURNS TRIGGER AS $ $ BEGIN IF (TG_OP = 'INSERT') THEN
UPDATE
    product_counters
SET
    count_review = count_review + 1
WHERE
    product_id = NEW.product_id;

ELSIF (TG_OP = 'DELETE') THEN
UPDATE
    product_counters
SET
    count_review = count_review - 1
WHERE
    product_id = OLD.product_id;

END IF;

RETURN NULL;

END;

$ $ LANGUAGE plpgsql;

CREATE TRIGGER review_trigger
AFTER
INSERT
    OR DELETE ON reviews FOR EACH ROW EXECUTE PROCEDURE update_product_counters_on_review_change();

-- Триггер для таблицы notifications
CREATE
OR REPLACE FUNCTION update_product_counters_on_notification_change() RETURNS TRIGGER AS $ $ BEGIN IF (TG_OP = 'INSERT') THEN
UPDATE
    product_counters
SET
    count_notification = count_notification + 1
WHERE
    product_id = NEW.product_id;

ELSIF (TG_OP = 'DELETE') THEN
UPDATE
    product_counters
SET
    count_notification = count_notification - 1
WHERE
    product_id = OLD.product_id;

END IF;

RETURN NULL;

END;

$ $ LANGUAGE plpgsql;

CREATE TRIGGER notification_trigger
AFTER
INSERT
    OR DELETE ON notifications FOR EACH ROW EXECUTE PROCEDURE update_product_counters_on_notification_change();

-- Триггер для таблицы subscriptions
CREATE
OR REPLACE FUNCTION update_product_counters_on_subscription_change() RETURNS TRIGGER AS $ $ BEGIN IF (TG_OP = 'INSERT') THEN
UPDATE
    product_counters
SET
    count_subscription = count_subscription + 1
WHERE
    product_id = NEW.product_id;

ELSIF (TG_OP = 'DELETE') THEN
UPDATE
    product_counters
SET
    count_subscription = count_subscription - 1
WHERE
    product_id = OLD.product_id;

END IF;

RETURN NULL;

END;

$ $ LANGUAGE plpgsql;

CREATE TRIGGER subscription_trigger
AFTER
INSERT
    OR DELETE ON subscriptions FOR EACH ROW EXECUTE PROCEDURE update_product_counters_on_subscription_change();