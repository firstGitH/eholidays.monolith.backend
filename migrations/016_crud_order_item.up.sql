CREATE
OR REPLACE FUNCTION create_order_item(
    _order_id INTEGER,
    _product_id INTEGER,
    _quantity INTEGER
) RETURNS INTEGER AS $ $ BEGIN
INSERT INTO
    order_items (order_id, product_id, quantity)
VALUES
    (_order_id, _product_id, _quantity) RETURNING id;

END;

$ $ LANGUAGE plpgsql;

CREATE
OR REPLACE FUNCTION get_order_item(_id INTEGER) RETURNS TABLE(
    id INTEGER,
    order_id INTEGER,
    product_id INTEGER,
    quantity INTEGER,
    price NUMERIC(10, 2),
    name VARCHAR(50),
    current_price NUMERIC(10, 2),
) AS $ $ BEGIN RETURN QUERY
SELECT
    oi.id,
    oi.order_id,
    oi.product_id,
    oi.quantity,
    oi.price p.name,
    p.price
FROM
    order_items oi
    JOIN products p ON oi.product_id = p.id
WHERE
    id = _id;

END;

$ $ LANGUAGE plpgsql;

CREATE
OR REPLACE FUNCTION get_order_items_by_order(
    _order_id INTEGER,
    _offset INTEGER,
    _limit INTEGER
) RETURNS TABLE (
    id INTEGER,
    order_id INTEGER,
    product_id INTEGER,
    quantity INTEGER,
    price NUMERIC(10, 2),
    name VARCHAR(50),
    current_price NUMERIC(10, 2),
) AS $ $ BEGIN RETURN QUERY
SELECT
    oi.id,
    oi.order_id,
    oi.product_id,
    oi.quantity,
    oi.price p.name,
    p.price
FROM
    order_items oi
    JOIN products p ON oi.product_id = p.id
WHERE
    order_id = _order_id
ORDER BY
    id OFFSET _offset
LIMIT
    _limit;

END;

$ $ LANGUAGE plpgsql;

CREATE
OR REPLACE FUNCTION update_order_item(
    _id INTEGER,
    _quantity INTEGER,
    _price NUMERIC(10, 2)
) RETURNS VOID AS $ $ BEGIN
UPDATE
    order_items
SET
    quantity = _quantity,
    price = _price
WHERE
    id = _id;

END;

$ $ LANGUAGE plpgsql;

CREATE
OR REPLACE FUNCTION delete_order_item(_id INTEGER) RETURNS VOID AS $ $ BEGIN
DELETE FROM
    order_items
WHERE
    id = _id;

END;

$ $ LANGUAGE plpgsql;