CREATE
OR REPLACE FUNCTION create_order(
    _user_id INTEGER,
    _payment_method PAYMENT_METHOD,
    _delivery_location VARCHAR(250)
) RETURNS INTEGER AS $ $ BEGIN
INSERT INTO
    orders (user_id)
VALUES
    (_user_id) RETURNING id;

END;

$ $ LANGUAGE plpgsql;

CREATE
OR REPLACE FUNCTION get_orders(_offset INTEGER, _limit INTEGER) RETURNS TABLE (
    id INTEGER,
    user_id INTEGER,
    created_date TIMESTAMPTZ,
    total_cost NUMERIC(10, 2),
    status ORDER_STATUS,
    payment_method PAYMENT_METHOD,
    delivery_location VARCHAR(250),
    count_items INTEGER,
    created_date TIMESTAMPTZ,
    updated_date TIMESTAMPTZ
) AS $ $ BEGIN RETURN QUERY
SELECT
    id,
    user_id,
    created_date,
    total_cost,
    status,
    payment_method,
    delivery_location,
    count_items,
    created_date,
    updated_date
FROM
    orders
ORDER BY
    id OFFSET _offset
LIMIT
    _limit;

END;

$ $ LANGUAGE plpgsql;

CREATE
OR REPLACE FUNCTION get_orders_by_user(
    _user_id INTEGER,
    _offset INTEGER,
    _limit INTEGER
) RETURNS TABLE (
    id INTEGER,
    user_id INTEGER,
    created_date TIMESTAMPTZ,
    total_cost NUMERIC(10, 2),
    status ORDER_STATUS,
    payment_method PAYMENT_METHOD,
    delivery_location VARCHAR(250),
    count_items INTEGER,
    created_date TIMESTAMPTZ,
    updated_date TIMESTAMPTZ
) AS $ $ BEGIN RETURN QUERY
SELECT
    id,
    user_id,
    created_date,
    total_cost,
    status,
    payment_method,
    delivery_location,
    count_items,
    created_date,
    updated_date
FROM
    orders
WHERE
    user_id = _user_id
ORDER BY
    id OFFSET _offset
LIMIT
    _limit;

END;

$ $ LANGUAGE plpgsql;

CREATE
OR REPLACE FUNCTION get_total_orders_by_user(_user_id INTEGER) RETURNS INTEGER AS $ $ DECLARE _total INTEGER;

BEGIN
SELECT
    uc.count_order INTO _total
FROM
    user_counters uc
WHERE
    uc.user_id = _user_id;

RETURN _total;

END;

$ $ LANGUAGE plpgsql;

CREATE
OR REPLACE FUNCTION get_total_orders() RETURNS INTEGER AS $ $ DECLARE _total INTEGER;

BEGIN
SELECT
    COUNT(*) INTO _total
FROM
    orders;

RETURN _total;

END;

$ $ LANGUAGE plpgsql;

CREATE
OR REPLACE FUNCTION update_order_status(_id INTEGER, _status ORDER_STATUS) RETURNS VOID AS $ $ BEGIN
UPDATE
    orders
SET
    status = _status
WHERE
    id = _id;

END;

$ $ LANGUAGE plpgsql;

CREATE
OR REPLACE FUNCTION delete_order(_id INTEGER) RETURNS VOID AS $ $ BEGIN
DELETE FROM
    orders
WHERE
    id = _id;

END;

$ $ LANGUAGE plpgsql;

CREATE CREATE
OR REPLACE FUNCTION confirm_order(
    _id INTEGER,
    _payment_method PAYMENT_METHOD,
    _delivery_location VARCHAR(250)
) RETURNS VOID AS $ $ BEGIN
UPDATE
    orders
SET
    status = 'confirmed',
    payment_method = _payment_method,
    delivery_location = _delivery_location
WHERE
    id = _id;

END;

$ $ LANGUAGE plpgsql;

CREATE
OR REPLACE FUNCTION cancel_order(_id INTEGER) RETURNS VOID AS $ $ BEGIN
UPDATE
    orders
SET
    status = 'cancelled'
WHERE
    id = _id;

END;

$ $ LANGUAGE plpgsql;

CREATE
OR REPLACE FUNCTION return_order(_id INTEGER) RETURNS VOID AS $ $ BEGIN
UPDATE
    orders
SET
    status = 'returned'
WHERE
    id = _id;

END;

$ $ LANGUAGE plpgsql;