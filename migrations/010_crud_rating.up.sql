CREATE
OR REPLACE FUNCTION get_ratings(_offset INTEGER, _limit INTEGER) RETURNS TABLE (id INTEGER, value INTEGER) AS $ $ BEGIN RETURN QUERY
SELECT
    id,
    value
FROM
    ratings
ORDER BY
    value OFFSET _offset
LIMIT
    _limit;

END;

$ $ LANGUAGE plpgsql;