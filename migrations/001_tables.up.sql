CREATE TABLE IF NOT EXISTS roles (
id SERIAL PRIMARY KEY,
name VARCHAR(25) UNIQUE NOT NULL
);

CREATE TABLE IF NOT EXISTS users (
    id SERIAL PRIMARY KEY,
    login VARCHAR(25) UNIQUE NOT NULL CHECK (char_length(login) >= 5),
    password BYTEA NOT NULL,
    username VARCHAR(25) UNIQUE NOT NULL CHECK (char_length(username) >= 5),
    fullname VARCHAR(50),
    gender CHAR(1) DEFAULT NULL CHECK (gender IN ('m', 'w')),
    age INTEGER DEFAULT NULL CHECK (
        age >= 0
        AND age <= 120
    ),
    phone VARCHAR(40),
    about_me VARCHAR(250),
    created_date TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_date TIMESTAMPTZ
);

CREATE INDEX idx_users_nickname ON users (username);

CREATE INDEX idx_users_login ON users (login);

CREATE TABLE IF NOT EXISTS users_role (
id SERIAL PRIMARY KEY,
user_id INTEGER NOT NULL REFERENCES users (id) ON DELETE CASCADE,
role_id INTEGER NOT NULL REFERENCES roles (id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS categories (
id SERIAL PRIMARY KEY,
name VARCHAR(35) UNIQUE NOT NULL,
description VARCHAR(100)
);

CREATE TABLE IF NOT EXISTS products (
    id SERIAL PRIMARY KEY,
    category_id INTEGER NOT NULL REFERENCES categories (id) ON DELETE CASCADE,
    name VARCHAR(50) UNIQUE NOT NULL CHECK (char_length(name) >= 5),
    price INTEGER DEFAULT NULL CHECK (price >= 0),
    quantity INTEGER CHECK (quantity >= 0) DEFAULT 0,
    rating NUMERIC(2, 2) DEFAULT NULL,
    description VARCHAR(350),
    image UUID NOT NULL,
    created_date TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_date TIMESTAMPTZ
);

CREATE TABLE IF NOT EXISTS product_items (
    id SERIAL PRIMARY KEY,
    product_id INTEGER NOT NULL REFERENCES products(id) ON DELETE CASCADE,
    name VARCHAR(35) NOT NULL CHECK (char_length(name) >= 2),
    value VARCHAR(100) NOT NULL
);

CREATE INDEX idx_products_name ON products (name);

CREATE TABLE IF NOT EXISTS ratings (
id SERIAL PRIMARY KEY,
value INTEGER UNIQUE NOT NULL
);

CREATE TABLE IF NOT EXISTS reviews (
id SERIAL PRIMARY KEY,
user_id INTEGER NOT NULL REFERENCES users(id) ON DELETE CASCADE,
product_id INTEGER NOT NULL REFERENCES products(id) ON DELETE CASCADE,
rating_id INTEGER NOT NULL REFERENCES ratings(id) ON DELETE CASCADE,
date TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
content VARCHAR(250) NOT NULL,
UNIQUE (user_id, product_id)
);

CREATE INDEX idx_reviews_user ON reviews (user_id);

CREATE INDEX idx_reviews_product ON reviews (product_id);

CREATE TABLE IF NOT EXISTS subscriptions (
id SERIAL PRIMARY KEY,
user_id INTEGER NOT NULL REFERENCES users(id) ON DELETE CASCADE,
product_id INTEGER NOT NULL REFERENCES products(id) ON DELETE CASCADE,
created_date TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
UNIQUE (user_id, product_id)
);

CREATE TYPE NOTIFICATION_TYPE AS ENUM (
'created',
'increased',
'decreased',
'updated',
'deleted',
);

CREATE TABLE IF NOT EXISTS notifications (
    id SERIAL PRIMARY KEY,
    product_id INTEGER NOT NULL REFERENCES products (id) ON DELETE CASCADE,
    type NOTIFICATION_TYPE NOT NULL,
    title VARCHAR(35) NOT NULL,
    content VARCHAR(250) NOT NULL CHECK (char_length(content) >= 10),
    created_date TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS cart_items (
id SERIAL PRIMARY KEY,
user_id INTEGER NOT NULL REFERENCES users (id) ON DELETE CASCADE,
product_id INTEGER NOT NULL REFERENCES products (id) ON DELETE CASCADE,
UNIQUE (user_id, product_id)
);

CREATE TYPE ORDER_STATUS AS ENUM (
'created',
'drafted',
'empty',
'cancelled',
'confirmed',
'paid',
'returned'
);

CREATE TYPE PAYMENT_METHOD AS ENUM ('cash', 'card');

CREATE TABLE IF NOT EXISTS orders (
id SERIAL PRIMARY KEY,
user_id INTEGER NOT NULL REFERENCES users(id) ON DELETE CASCADE,
total_cost NUMERIC(10, 2) NOT NULL DEFAULT 0.0,
status ORDER_STATUS NOT NULL DEFAULT 'created',
payment_method PAYMENT_METHOD,
delivery_location VARCHAR(250),
count_items INTEGER NOT NULL DEFAULT 0,
created_date TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
updated_date TIMESTAMPTZ
);

CREATE TABLE IF NOT EXISTS order_items (
id SERIAL PRIMARY KEY,
order_id INTEGER NOT NULL REFERENCES orders(id) ON DELETE CASCADE,
product_id INTEGER NOT NULL REFERENCES products(id) ON DELETE CASCADE,
quantity INTEGER CHECK (quantity >= 1) DEFAULT 1,
price NUMERIC(10, 2)
);

CREATE TABLE IF NOT EXISTS user_counters (
user_id INTEGER NOT NULL UNIQUE REFERENCES users(id) ON DELETE CASCADE,
count_subscription INTEGER NOT NULL DEFAULT 0,
count_order INTEGER NOT NULL DEFAULT 0,
total_order_sum NUMERIC(10, 2) NOT NULL DEFAULT 0.0
);

CREATE INDEX idx_user_counters_id ON user_counters (user_id);

CREATE TABLE IF NOT EXISTS product_counters (
product_id INTEGER NOT NULL UNIQUE REFERENCES products(id) ON DELETE CASCADE,
count_item INTEGER NOT NULL DEFAULT 0 CHECK (order_count >= 0),
count_order INTEGER NOT NULL DEFAULT 0 CHECK (order_count >= 0),
count_review INTEGER NOT NULL DEFAULT 0 CHECK (review_count >= 0),
count_notification INTEGER NOT NULL DEFAULT 0 CHECK (notification_count >= 0),
count_subscription INTEGER NOT NULL DEFAULT 0 CHECK (subscription_count >= 0)
);

CREATE INDEX idx_product_counters_id ON product_counters (product_id);

CREATE TABLE IF NOT EXISTS category_counters (
category_id INTEGER NOT NULL UNIQUE REFERENCES categories(id) ON DELETE CASCADE,
count_products INTEGER NOT NULL DEFAULT 0,
);

CREATE INDEX idx_category_counters_id ON category_counters (user_id);