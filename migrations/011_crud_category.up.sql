CREATE
OR REPLACE FUNCTION create_category(_name VARCHAR, _description VARCHAR) RETURNS INTEGER AS $ $ BEGIN
INSERT INTO
    categories (name, description)
VALUES
    (_name, _description) RETURNING id;

END;

$ $ LANGUAGE plpgsql;

CREATE
OR REPLACE FUNCTION get_notifications_by_product(
    _user_id INTEGER,
    _offset INTEGER,
    _limit INTEGER
) RETURNS TABLE (
    id INTEGER,
    product_id INTEGER,
    type NOTIFICATION_TYPE,
    date TIMESTAMPTZ,
    title VARCHAR,
    content VARCHAR
) AS $ $ BEGIN RETURN QUERY
SELECT
    id,
    product_id,
    type,
    date,
    title,
    content
FROM
    cart_items
WHERE
    user_id = _user_id
ORDER BY
    id OFFSET _offset
LIMIT
    _limit;

END;

CREATE
OR REPLACE FUNCTION get_total_categories() RETURNS INTEGER AS $ $ DECLARE _total INTEGER;

BEGIN
SELECT
    COUNT(*) INTO _total
FROM
    categories;

RETURN _total;

END;

$ $ LANGUAGE plpgsql;

CREATE
OR REPLACE FUNCTION update_category(
    _id INTEGER,
    _name VARCHAR,
    _description VARCHAR
) RETURNS VOID AS $ $ BEGIN
UPDATE
    categories
SET
    name = _name,
    description = _description
WHERE
    id = _id;

END;

$ $ LANGUAGE plpgsql;

CREATE
OR REPLACE FUNCTION delete_category(_id INTEGER) RETURNS VOID AS $ $ BEGIN
DELETE FROM
    categories
WHERE
    id = _id;

END;

$ $ LANGUAGE plpgsql;