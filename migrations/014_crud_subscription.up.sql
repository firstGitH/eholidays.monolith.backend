CREATE
OR REPLACE FUNCTION create_subscription(_user_id INTEGER, _product_id INTEGER) RETURNS INTEGER AS $ $ BEGIN
INSERT INTO
    subscriptions (user_id, product_id)
VALUES
    (_user_id, _product_id) RETURNING id;

END;

$ $ LANGUAGE plpgsql;

CREATE
OR REPLACE FUNCTION get_subscriptions_by_user(
    _user_id INTEGER,
    _offset INTEGER,
    _limit INTEGER
) RETURNS TABLE (
    id INTEGER,
    user_id INTEGER,
    product_id INTEGER,
    product_name VARCHAR,
    created_date TIMESTAMPTZ
) AS $ $ BEGIN RETURN QUERY
SELECT
    s.id,
    s.user_id,
    p.name,
    s.created_date
FROM
    subscriptions s
    JOIN products p ON s.product_id = p.id
WHERE
    user_id = _user_id
ORDER BY
    s.created_date OFFSET _offset
LIMIT
    _limit;

END;

$ $ LANGUAGE plpgsql;

CREATE
OR REPLACE FUNCTION get_total_subscriptions_by_user(_user_id INTEGER) RETURNS INTEGER AS $ $ DECLARE _total INTEGER;

BEGIN
SELECT
    uc.count_subscription INTO _total
FROM
    user_counters uc
WHERE
    uc.user_id = _user_id;

RETURN _total;

END;

$ $ LANGUAGE plpgsql;

CREATE
OR REPLACE FUNCTION get_subscriptions(_offset INTEGER, _limit INTEGER) RETURNS TABLE (
    id INTEGER,
    user_id INTEGER,
    product_id INTEGER,
    product_name VARCHAR,
    created_date TIMESTAMPTZ
) AS $ $ BEGIN RETURN QUERY
SELECT
    s.id,
    s.user_id,
    p.name,
    s.created_date
FROM
    subscriptions s
    JOIN products p ON s.product_id = p.id
WHERE
    user_id = _user_id
ORDER BY
    s.created_date OFFSET _offset
LIMIT
    _limit;

END;

$ $ LANGUAGE plpgsql;

CREATE
OR REPLACE FUNCTION get_total_subscriptions() RETURNS INTEGER AS $ $ DECLARE _total INTEGER;

BEGIN
SELECT
    COUNT(*) INTO _total
FROM
    subscriptions;

RETURN _total;

END;

$ $ LANGUAGE plpgsql;

-- TODO: remake input structe 
CREATE OR REPLACE FUNCTION delete_subscription(_user_id INTEGER, _product_id INTEGER) RETURNS VOID AS $$
BEGIN
    DELETE FROM subscriptions
    WHERE user_id = _user_id AND product_id = _product_id;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION has_subscription(_user_id INTEGER, _product_id INTEGER) RETURNS BOOLEAN AS $$
DECLARE _is_subscribed BOOLEAN;
BEGIN
    SELECT EXISTS (
        SELECT 1 FROM subscriptions
        WHERE user_id = _user_id AND product_id = _product_id
    ) INTO _is_subscribed;
    RETURN _is_subscribed;
END;
$$ LANGUAGE plpgsql;

