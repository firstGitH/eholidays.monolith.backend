-- CREATE
CREATE
OR REPLACE FUNCTION create_product(
    _category_id INTEGER,
    _name VARCHAR,
    _price NUMERIC(10, 2),
    _description VARCHAR,
    _quantity INTEGER,
    _image UUID
) RETURNS INTEGER AS $ $ BEGIN
INSERT INTO
    products (
        category_id,
        name,
        price,
        description,
        quantity,
        image
    )
VALUES
    (
        _category_id,
        _name,
        _price,
        _description,
        _quantity,
        _image
    ) RETURNING id;

END;

$ $ LANGUAGE plpgsql;

CREATE
OR REPLACE FUNCTION create_product_with_items(
    _category_id INTEGER,
    _name VARCHAR,
    _price NUMERIC(10, 2),
    _description VARCHAR,
    _quantity INTEGER,
    _image UUID,
    items_data JSONB []
) RETURNS INTEGER AS $ $ DECLARE item_element JSONB;

new_product_id INTEGER;

BEGIN BEGIN -- INSERT DATA OF PRODUCT
INSERT INTO
    products (
        category_id,
        name,
        price,
        description,
        quantity,
        image
    )
VALUES
    (
        _category_id,
        _name,
        _price,
        _description,
        _quantity,
        _image
    ) RETURNING id INTO new_product_id;

-- INSERT PRODUCT ITEMS
FOREACH item_element IN ARRAY items_data LOOP
INSERT INTO
    product_items (product_id, name, value)
VALUES
    (
        new_product_id,
        item_element ->> 'name',
        item_element ->> 'value'
    );

END LOOP;

EXCEPTION
WHEN OTHERS THEN ROLLBACK;

RAISE;

END;

END;

$ $ LANGUAGE plpgsql;

-- GET
CREATE
OR REPLACE FUNCTION get_product_full_info(_id INTEGER) RETURNS TABLE (
    id INTEGER,
    category_id INTEGER,
    name VARCHAR,
    price NUMERIC(10, 2),
    description VARCHAR,
    quantity INTEGER,
    rating NUMERIC(2, 2),
    count_subscription INTEGER,
    image UUID,
    created_date TIMESTAMPTZ,
    updated_date TIMESTAMPTZ,
    item_count INTEGER,
    subscription_count INTEGER,
    review_count INTEGER,
    notification_count INTEGER
) AS $ $ BEGIN RETURN QUERY
SELECT
    p.id,
    p.category_id,
    p.name,
    p.price,
    p.description,
    p.quantity,
    p.rating,
    p.image,
    p.created_date,
    p.updated_date,
    pc.count_item,
    pc.count_subscription,
    pc.count_review,
    pc.count_notification
FROM
    products p
    JOIN product_counters pc ON p.id = pc.product_id
WHERE
    p.id = _id;

END;

$ $ LANGUAGE plpgsql;

CREATE
OR REPLACE FUNCTION get_products_by_category(
    _category_id INTEGER,
    _offset INTEGER,
    _limit INTEGER
) RETURNS TABLE (
    id INTEGER,
    category_id INTEGER,
    name VARCHAR,
    price NUMERIC(10, 2),
    rating NUMERIC(2, 2),
    image UUID
) AS $ $ BEGIN RETURN QUERY
SELECT
    id,
    category_id,
    name,
    price,
    rating,
    image
FROM
    products
WHERE
    category_id = _category_id
ORDER BY
    created_date DESC,
    rating DESC OFFSET _offset
LIMIT
    _limit;

END;

$ $ LANGUAGE plpgsql;

CREATE
OR REPLACE FUNCTION get_total_products_by_category(_category_id INTEGER) RETURNS INTEGER AS $ $ DECLARE _total INTEGER;

BEGIN
SELECT
    COUNT(*) INTO _total
FROM
    products
WHERE
    category_id = _category_id;

RETURN _total;

END;

$ $ LANGUAGE plpgsql;

CREATE
OR REPLACE FUNCTION get_products(_offset INTEGER, _limit INTEGER) RETURNS TABLE (
    id INTEGER,
    category_id INTEGER,
    name VARCHAR,
    price NUMERIC(10, 2),
    rating NUMERIC(2, 2),
    image UUID,
    created_date TIMESTAMPTZ,
    updated_date TIMESTAMPTZ
) AS $ $ BEGIN RETURN QUERY
SELECT
    id,
    category_id,
    name,
    price,
    rating,
    image,
    created_date,
    updated_date
FROM
    products
ORDER BY
    created_date DESC,
    rating DESC OFFSET _offset
LIMIT
    _limit;

END;

$ $ LANGUAGE plpgsql;

CREATE
OR REPLACE FUNCTION get_total_products() RETURNS INTEGER AS $ $ DECLARE _total INTEGER;

BEGIN
SELECT
    COUNT(*) INTO _total
FROM
    products;

RETURN _total;

END;

$ $ LANGUAGE plpgsql;

CREATE
OR REPLACE FUNCTION get_products_by_user(
    _user_id INTEGER,
    _limit INTEGER,
    _offset INTEGER
) RETURNS TABLE(
    id SERIAL,
    category_id INTEGER,
    name VARCHAR(50),
    price INTEGER,
    rating NUMERIC(2, 2),
    image UUID
) AS $ $ BEGIN RETURN QUERY
SELECT
    p.id,
    p.category_id,
    p.name,
    p.price,
    p.rating,
    p.image
FROM
    products p
    JOIN subscriptions s ON p.id = s.product_id
WHERE
    s.user_id = _user_id
ORDER BY
    p.created_date DESC,
    p.rating DESC
LIMIT
    _limit OFFSET _offset;

END;

$ $ LANGUAGE plpgsql;

CREATE
OR REPLACE FUNCTION get_total_products_by_user(_user_id INTEGER) RETURNS INTEGER AS $ $ DECLARE total INTEGER;

BEGIN
SELECT
    uc.count_subscription INTO total
FROM
    user_counters uc
WHERE
    uc.user_id = _user_id;

RETURN total;

END;

$ $ LANGUAGE plpgsql;

CREATE
OR REPLACE FUNCTION update_product_description(_id INTEGER, _description VARCHAR) RETURNS VOID AS $ $ BEGIN
UPDATE
    products
SET
    description = _description
WHERE
    id = _id;

END;

$ $ LANGUAGE plpgsql;

CREATE
OR REPLACE FUNCTION update_product_price(_id INTEGER, _price NUMERIC(10, 2)) RETURNS VOID AS $ $ BEGIN
UPDATE
    products
SET
    price = _price
WHERE
    id = _id;

END;

$ $ LANGUAGE plpgsql;

CREATE
OR REPLACE FUNCTION update_product_category(_id INTEGER, _category_id INTEGER) RETURNS VOID AS $ $ BEGIN
UPDATE
    products
SET
    category_id = _category_id
WHERE
    id = _id;

END;

$ $ LANGUAGE plpgsql;

CREATE
OR REPLACE FUNCTION update_product_name(_id INTEGER, _name VARCHAR) RETURNS VOID AS $ $ BEGIN
UPDATE
    products
SET
    name = _name
WHERE
    id = _id;

END;

$ $ LANGUAGE plpgsql;

CREATE
OR REPLACE FUNCTION increase_product_quantity(_id INTEGER, _quantity INTEGER) RETURNS INTEGER AS $ $ DECLARE _new_quantity INTEGER;

BEGIN
UPDATE
    products
SET
    quantity = quantity + _quantity
WHERE
    id = _id;

SELECT
    quantity INTO _new_quantity
FROM
    products
WHERE
    id = _id;

RETURN _new_quantity;

END;

$ $ LANGUAGE plpgsql;

CREATE
OR REPLACE FUNCTION decrease_product_quantity(_id INTEGER, _quantity INTEGER) RETURNS INTEGER AS $ $ DECLARE _current_quantity INTEGER;

BEGIN
SELECT
    quantity INTO _current_quantity
FROM
    products
WHERE
    id = _id;

IF _current_quantity < _quantity THEN RETURN _current_quantity - _quantity;

END IF;

UPDATE
    products
SET
    quantity = quantity - _quantity
WHERE
    id = _id;

SELECT
    quantity INTO _current_quantity
FROM
    products
WHERE
    id = _id;

RETURN _current_quantity;

END;

$ $ LANGUAGE plpgsql;

CREATE
OR REPLACE FUNCTION update_product_image(_id INTEGER, _image UUID) RETURNS VOID AS $ $ BEGIN
UPDATE
    products
SET
    image = _image
WHERE
    id = _id;

END;

$ $ LANGUAGE plpgsql;

CREATE
OR REPLACE FUNCTION delete_product(_id INTEGER) RETURNS VOID AS $ $ BEGIN
DELETE FROM
    products
WHERE
    id = _id;

END;

$ $ LANGUAGE plpgsql;