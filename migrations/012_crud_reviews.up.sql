CREATE
OR REPLACE FUNCTION create_review(
    _user_id INTEGER,
    _product_id INTEGER,
    _rating_id INTEGER,
    _date TIMESTAMPTZ,
    _content VARCHAR
) RETURNS INTEGER AS $ $ BEGIN
INSERT INTO
    reviews(user_id, product_id, rating_id, date, content)
VALUES
    (
        _user_id,
        _product_id,
        _rating_id,
        _date,
        _content
    ) RETURNING id;

END;

$ $ LANGUAGE plpgsql;

CREATE
OR REPLACE FUNCTION get_reviews_by_product(
    _product_id INTEGER,
    _limit INTEGER,
    _offset INTEGER
) RETURNS TABLE(
    id INTEGER,
    user_id INTEGER,
    product_id INTEGER,
    rating_id INTEGER,
    username VARCHAR(25),
    rating INTEGER,
    date TIMESTAMPTZ,
    content VARCHAR
) AS $ $ BEGIN RETURN QUERY
SELECT
    r.id,
    r.user_id,
    r.product_id,
    r.rating_id,
    u.username,
    rt.rating,
    r.date,
    r.content
FROM
    reviews r
    JOIN ratings rt ON r.rating_id = rt.id
    JOIN users u ON u.user_id = rt.user_id
WHERE
    r.product_id = _product_id
LIMIT
    _limit OFFSET _offset;

END;

$ $ LANGUAGE plpgsql;

CREATE
OR REPLACE FUNCTION get_total_reviews_by_product(_product_id INTEGER) RETURNS INTEGER AS $ $ DECLARE _total INTEGER;

BEGIN
SELECT
    pc.count_review INTO _total
FROM
    product_counters pc
WHERE
    pc.product_id = _product_id;

RETURN _total;

END;

$ $ LANGUAGE plpgsql;

CREATE
OR REPLACE FUNCTION update_review(
    _id INTEGER,
    _rating_id INTEGER,
    _content VARCHAR
) RETURNS VOID AS $ $ BEGIN
UPDATE
    reviews
SET
    rating_id = _rating_id,
    date = CURRENT_TIMESTAMPTZ,
    content = _content
WHERE
    id = _id;

END;

$ $ LANGUAGE plpgsql;

CREATE
OR REPLACE FUNCTION delete_review(_id INTEGER) RETURNS VOID AS $ $ BEGIN
DELETE FROM
    reviews
WHERE
    id = _id;

END;

$ $ LANGUAGE plpgsql;