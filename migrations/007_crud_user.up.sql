CREATE
OR REPLACE FUNCTION create_user(
    _login VARCHAR,
    _password BYTEA,
    _username VARCHAR,
    _fullname VARCHAR,
    _gender CHAR(1),
    _age INTEGER,
    _phone VARCHAR,
    _about_me VARCHAR
) RETURNS INTEGER AS $ $ BEGIN
INSERT INTO
    users (
        login,
        password,
        username,
        fullname,
        gender,
        age,
        phone,
        about_me
    )
VALUES
    (
        _login,
        _password,
        _username,
        _fullname,
        _gender,
        _age,
        _phone,
        _about_me
    ) RETURNING id;

END;

$ $ LANGUAGE plpgsql;

CREATE
OR REPLACE FUNCTION contains_user(_login VARCHAR, _password BYTEA) RETURNS INTEGER AS $ $ DECLARE _user_id INTEGER;

BEGIN
SELECT
    id INTO _user_id
FROM
    users
WHERE
    login = _login
    AND password = _password;

RETURN _user_id;

EXCEPTION
WHEN NO_DATA_FOUND THEN RETURN 0;

END;

$ $ LANGUAGE plpgsql;

CREATE
OR REPLACE FUNCTION user_has_role_by_id(_user_id INTEGER, _role_id INTEGER) RETURNS BOOLEAN LANGUAGE plpgsql AS $ $ DECLARE _exists BOOLEAN;

BEGIN
SELECT
    EXISTS (
        SELECT
            1
        FROM
            users_role
        WHERE
            user_id = _user_id
            AND role_id = _role_id
    ) INTO _exists;

RETURN _exists;

END;

$ $;

CREATE
OR REPLACE FUNCTION user_has_role_by_name(_user_id INTEGER, _role_name VARCHAR) RETURNS BOOLEAN LANGUAGE plpgsql AS $ $ DECLARE _exists BOOLEAN BEGIN
SELECT
    EXISTS (
        SELECT
            1
        FROM
            users_role ur
            JOIN roles r ON ur.role_id = r.id
        WHERE
            ur.user_id = _user_id
            AND r.name = _role_name
    ) INTO _exists;

RETURN _exists;

END;

$ $;

CREATE
OR REPLACE FUNCTION get_self_info(_id INTEGER) RETURNS TABLE (
    id INTEGER,
    login VARCHAR,
    username VARCHAR,
    fullname VARCHAR,
    gender CHAR(1),
    age INTEGER,
    phone VARCHAR,
    about_me VARCHAR
) AS $ $ BEGIN RETURN QUERY
SELECT
    id,
    login,
    username,
    fullname,
    gender,
    age,
    phone,
    about_me
FROM
    users
WHERE
    id = _id;

END;

$ $ LANGUAGE plpgsql;

CREATE
OR REPLACE FUNCTION get_user_info(_id INTEGER) RETURNS TABLE (
    id INTEGER,
    username VARCHAR,
    fullname VARCHAR,
    gender CHAR(1),
    age INTEGER,
    phone VARCHAR,
    about_me VARCHAR
) AS $ $ BEGIN RETURN QUERY
SELECT
    id,
    username,
    fullname,
    gender,
    age,
    phone,
    about_me
FROM
    users
WHERE
    id = _id;

END;

$ $ LANGUAGE plpgsql;

CREATE
OR REPLACE FUNCTION get_users(_offset INTEGER, _limit INTEGER) RETURNS TABLE (
    id INTEGER,
    username VARCHAR,
    fullname VARCHAR,
    gender CHAR(1),
    age INTEGER,
    phone VARCHAR,
    about_me VARCHAR,
    count_order INTEGER,
    total_order_sum NUMERIC(10, 2),
    count_subscription INTEGER
) AS $ $ BEGIN RETURN QUERY
SELECT
    id,
    username,
    fullname,
    gender,
    age,
    phone,
    about_me,
    count_order,
    total_order_sum,
    count_subscription
FROM
    users
ORDER BY
    id OFFSET _offset
LIMIT
    _limit;

END;

$ $ LANGUAGE plpgsql;

CREATE
OR REPLACE FUNCTION get_total_users() RETURNS INTEGER AS $ $ DECLARE _total INTEGER;

BEGIN
SELECT
    COUNT(*) INTO _total
FROM
    users;

RETURN _total;

END;

$ $ LANGUAGE plpgsql;

CREATE
OR REPLACE FUNCTION update_user(
    _id INTEGER,
    _fullname VARCHAR,
    _gender CHAR,
    _age INTEGER,
    _phone VARCHAR,
    _about_me VARCHAR
) RETURNS VOID AS $ $ BEGIN
UPDATE
    users
SET
    fullname = _fullname,
    gender = _gender,
    age = _age,
    phone = _phone,
    about_me = _about_me
WHERE
    id = _id;

END;

$ $ LANGUAGE plpgsql;

CREATE
OR REPLACE FUNCTION delete_user(_id INTEGER) RETURNS VOID AS $ $ BEGIN
DELETE FROM
    users
WHERE
    id = _id;

END;

$ $ LANGUAGE plpgsql;

CREATE
OR REPLACE FUNCTION update_login(_id INTEGER, _new_login VARCHAR, _password BYTEA) RETURNS VOID LANGUAGE plpgsql AS $ $ BEGIN IF EXISTS (
    SELECT
        1
    FROM
        users
    WHERE
        id = _id
        AND password = _password
) THEN
UPDATE
    users
SET
    login = _new_login
WHERE
    id = _id;

ELSE RAISE EXCEPTION 'Incorrect password';

END IF;

END;

$ $;

CREATE
OR REPLACE FUNCTION update_password(
    _id INTEGER,
    _old_password BYTEA,
    _new_password BYTEA
) RETURNS VOID LANGUAGE plpgsql AS $ $ BEGIN IF EXISTS (
    SELECT
        1
    FROM
        users
    WHERE
        id = _id
        AND password = _old_password
) THEN
UPDATE
    users
SET
    password = _new_password
WHERE
    id = _id;

ELSE RAISE EXCEPTION 'Incorrect old password';

END IF;

END;

$ $;

CREATE
OR REPLACE FUNCTION update_username(
    _id INTEGER,
    _new_username VARCHAR,
    _password BYTEA
) RETURNS VOID LANGUAGE plpgsql AS $ $ BEGIN IF EXISTS (
    SELECT
        1
    FROM
        users
    WHERE
        id = _id
        AND password = _password
) THEN
UPDATE
    users
SET
    username = _new_username
WHERE
    id = _id;

ELSE RAISE EXCEPTION 'Incorrect password';

END IF;

END;

$ $;