CREATE
OR REPLACE FUNCTION create_notification(
    _product_id INTEGER,
    _type NOTIFICATION_TYPE,
    _title VARCHAR,
    _content VARCHAR
) RETURNS INTEGER AS $ $ BEGIN
INSERT INTO
    notifications (product_id, type, title, content)
VALUES
    (_product_id, _type, _title, _content) RETURNING id;

END;

$ $ LANGUAGE plpgsql;

CREATE
OR REPLACE FUNCTION get_notifications(_offset INTEGER, _limit INTEGER) RETURNS TABLE (
    id INTEGER,
    product_id INTEGER,
    type NOTIFICATION_TYPE,
    date TIMESTAMPTZ,
    title VARCHAR,
    content VARCHAR
) AS $ $ BEGIN RETURN QUERY
SELECT
    n.id,
    n.product_id,
    p.name,
    n.type,
    n.date,
    n.title,
    n.content
FROM
    notifications n
    JOIN products p ON n.product_id = p.id
ORDER BY
    id OFFSET _offset
LIMIT
    _limit;

END;

$ $ LANGUAGE plpgsql;

CREATE
OR REPLACE FUNCTION get_total_notifications() RETURNS INTEGER AS $ $ DECLARE _total INTEGER;

BEGIN
SELECT
    COUNT(*) INTO _total
FROM
    notifications;

RETURN _total;

END;

$ $ LANGUAGE plpgsql;

CREATE
OR REPLACE FUNCTION get_notifications_by_product(
    _product_id INTEGER,
    _offset INTEGER,
    _limit INTEGER
) RETURNS TABLE (
    id INTEGER,
    product_id INTEGER,
    type NOTIFICATION_TYPE,
    date TIMESTAMPTZ,
    title VARCHAR,
    content VARCHAR
) AS $ $ BEGIN RETURN QUERY
SELECT
    id,
    product_id,
    type,
    date,
    title,
    content
FROM
    notifications
WHERE
    product_id = _product_id
ORDER BY
    id OFFSET _offset
LIMIT
    _limit;

END;

$ $ LANGUAGE plpgsql;

CREATE
OR REPLACE FUNCTION get_total_notifications_by_product(_product_id INTEGER) RETURNS INTEGER AS $ $ DECLARE _total INTEGER;

BEGIN
SELECT
    pc.count_notification INTO _total
FROM
    product_counters pc
WHERE
    pc.product_id = _product_id;

RETURN _total;

END;

$ $ LANGUAGE plpgsql;

CREATE
OR REPLACE FUNCTION update_notification(_id INTEGER, _title VARCHAR, _content VARCHAR) RETURNS VOID AS $ $ BEGIN
UPDATE
    notifications
SET
    title = _title,
    content = _content
WHERE
    id = _id;

END;

$ $ LANGUAGE plpgsql;

CREATE OR_REPLACE FUNCTION delete_notification(_id INTEGER) LANGUAGE plpgsql AS $ $ BEGIN
DELETE FROM
    notifications
WHERE
    id = _id;

END;

$ $;