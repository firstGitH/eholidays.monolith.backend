
CREATE OR REPLACE FUNCTION update_product_rating() RETURNS TRIGGER AS $$
BEGIN
    IF (TG_OP = 'DELETE') THEN
        UPDATE products
        SET rating = (SELECT AVG(ratings.value) FROM reviews JOIN ratings ON reviews.rating_id = ratings.id WHERE reviews.product_id = OLD.product_id)
        WHERE id = OLD.product_id;
    ELSE
        UPDATE products
        SET rating = (SELECT AVG(ratings.value) FROM reviews JOIN ratings ON reviews.rating_id = ratings.id WHERE reviews.product_id = NEW.product_id)
        WHERE id = NEW.product_id;
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER update_product_rating
AFTER INSERT OR UPDATE OR DELETE ON reviews
FOR EACH ROW EXECUTE PROCEDURE update_product_rating();


CREATE OR REPLACE FUNCTION update_order_status() RETURNS TRIGGER AS $$
BEGIN
    IF (OLD.status = 'created' AND NEW.status NOT IN ('drafted', 'empty')) OR
       (OLD.status = 'drafted' AND NEW.status NOT IN ('confirmed', 'cancelled', 'empty')) OR
       (OLD.status = 'confirmed' AND NEW.status != 'returned') OR
       (OLD.status = 'cancelled' AND NEW.status NOT IN ('drafted', 'empty')) THEN
        RAISE EXCEPTION 'Invalid status transition';
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER order_status_update
BEFORE UPDATE OF status ON orders
FOR EACH ROW EXECUTE PROCEDURE update_order_status();


-- Триггер для таблицы order_items
CREATE OR REPLACE FUNCTION update_product_counters_on_order_item_change()
RETURNS TRIGGER AS $$
BEGIN
    IF (TG_OP = 'INSERT') THEN
        UPDATE product_counters
        SET count_item = count_item + NEW.quantity,
            count_order = count_order + 1
        WHERE product_id = NEW.product_id;
    ELSIF (TG_OP = 'DELETE') THEN
        UPDATE product_counters
        SET count_item = count_item - OLD.quantity,
            count_order = count_order - 1
        WHERE product_id = OLD.product_id;
    END IF;
    RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER order_item_trigger
AFTER INSERT OR DELETE ON order_items
FOR EACH ROW EXECUTE PROCEDURE update_product_counters_on_order_item_change();

-- Триггер для таблицы reviews
CREATE OR REPLACE FUNCTION update_product_counters_on_review_change()
RETURNS TRIGGER AS $$
BEGIN
    IF (TG_OP = 'INSERT') THEN
        UPDATE product_counters
        SET count_review = count_review + 1
        WHERE product_id = NEW.product_id;
    ELSIF (TG_OP = 'DELETE') THEN
        UPDATE product_counters
        SET count_review = count_review - 1
        WHERE product_id = OLD.product_id;
    END IF;
    RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER review_trigger
AFTER INSERT OR DELETE ON reviews
FOR EACH ROW EXECUTE PROCEDURE update_product_counters_on_review_change();

-- Триггер для таблицы notifications
CREATE OR REPLACE FUNCTION update_product_counters_on_notification_change()
RETURNS TRIGGER AS $$
BEGIN
    IF (TG_OP = 'INSERT') THEN
        UPDATE product_counters
        SET count_notification = count_notification + 1
        WHERE product_id = NEW.product_id;
    ELSIF (TG_OP = 'DELETE') THEN
        UPDATE product_counters
        SET count_notification = count_notification - 1
        WHERE product_id = OLD.product_id;
    END IF;
    RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER notification_trigger
AFTER INSERT OR DELETE ON notifications
FOR EACH ROW EXECUTE PROCEDURE update_product_counters_on_notification_change();

-- Триггер для таблицы subscriptions
CREATE OR REPLACE FUNCTION update_product_counters_on_subscription_change()
RETURNS TRIGGER AS $$
BEGIN
    IF (TG_OP = 'INSERT') THEN
        UPDATE product_counters
        SET count_subscription = count_subscription + 1
        WHERE product_id = NEW.product_id;
    ELSIF (TG_OP = 'DELETE') THEN
        UPDATE product_counters
        SET count_subscription = count_subscription - 1
        WHERE product_id = OLD.product_id;
    END IF;
    RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER subscription_trigger
AFTER INSERT OR DELETE ON subscriptions
FOR EACH ROW EXECUTE PROCEDURE update_product_counters_on_subscription_change();

