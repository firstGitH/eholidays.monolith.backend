CREATE OR REPLACE FUNCTION update_order_and_order_item_on_status_change()
RETURNS TRIGGER AS $$
DECLARE
    total_cost NUMERIC;
BEGIN
    IF (NEW.status = 'confirmed' AND OLD.status != 'confirmed') THEN
        -- Обновление цены в order_items и расчет общей стоимости заказа
        UPDATE order_items
        SET price = products.price
        FROM products
        WHERE order_items.product_id = products.id AND order_items.order_id = NEW.id;

        SELECT SUM(quantity * price) INTO total_cost
        FROM order_items
        WHERE order_id = NEW.id;

        UPDATE orders
        SET total_cost = total_cost + total_cost
        WHERE id = NEW.id;

        -- Обновление количества товара в products
        UPDATE products
        SET quantity = quantity - 
            (SELECT SUM(quantity) FROM order_items WHERE order_id = NEW.id)
        WHERE id IN (SELECT product_id FROM order_items WHERE order_id = NEW.id);
    ELSIF (NEW.status = 'returned' AND OLD.status != 'returned') THEN
        -- Обнуление цены в order_items и общей стоимости заказа
        UPDATE order_items
        SET price = 0
        WHERE order_id = NEW.id;

        UPDATE orders
        SET total_cost = 0
        WHERE id = NEW.id;

        -- Возврат количества товара в products
        UPDATE products
        SET quantity = quantity + 
            (SELECT SUM(quantity) FROM order_items WHERE order_id = NEW.id)
        WHERE id IN (SELECT product_id FROM order_items WHERE order_id = NEW.id);
    END IF;
    RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER order_status_update_trigger
AFTER UPDATE OF status ON orders
FOR EACH ROW WHEN (OLD.status IS DISTINCT FROM NEW.status)
EXECUTE PROCEDURE update_order_and_order_item_on_status_change();

CREATE OR REPLACE FUNCTION update_order_item_count_and_status()
RETURNS TRIGGER AS $$
BEGIN
    IF (TG_OP = 'INSERT') THEN
        UPDATE orders
        SET count_items = count_items + 1,
            status = CASE WHEN count_items + 1 > 0 THEN status ELSE 'empty' END
        WHERE id = NEW.order_id;
    ELSIF (TG_OP = 'DELETE') THEN
        UPDATE orders
        SET count_items = count_items - 1,
            status = CASE WHEN count_items - 1 = 0 THEN 'empty' ELSE status END
        WHERE id = OLD.order_id;
    END IF;
    RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER order_item_trigger
AFTER INSERT OR DELETE ON order_items
FOR EACH ROW EXECUTE PROCEDURE update_order_item_count_and_status();

CREATE OR REPLACE FUNCTION prevent_order_update_on_confirmed_or_returned()
RETURNS TRIGGER AS $$
BEGIN
    IF (OLD.status IN ('confirmed', 'returned')) THEN
        RAISE EXCEPTION 'Cannot update order with status confirmed or returned';
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER order_update_trigger
BEFORE UPDATE ON orders
FOR EACH ROW EXECUTE PROCEDURE prevent_order_update_on_confirmed_or_returned();
