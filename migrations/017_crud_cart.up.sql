CREATE
OR REPLACE FUNCTION create_cart_item(_user_id INTEGER, _product_id INTEGER) RETURNS INTEGER AS $ $ BEGIN
INSERT INTO
    categories (user_id, product_id)
VALUES
    (_user_id, _product_id) RETURNING id;

END;

$ $ LANGUAGE plpgsql;

CREATE
OR REPLACE FUNCTION get_cart_items_by_user(
    _user_id INTEGER,
    _offset INTEGER,
    _limit INTEGER
) RETURNS TABLE (
    id INTEGER,
    user_id INTEGER,
    product_id INTEGER,
    name VARCHAR,
    price NUMERIC(10, 2)
) AS $ $ BEGIN RETURN QUERY
SELECT
    ct.id,
    ct.user_id,
    ct.product_id,
    p.name,
    p.price
FROM
    cart_items ct
    JOIN products p ON ct.product_id = p.id
WHERE
    ct.user_id = _user_id
ORDER BY
    id OFFSET _offset
LIMIT
    _limit;

END;

$ $ LANGUAGE plpgsql;

CREATE
OR REPLACE FUNCTION get_total_cart_items_by_user(_user_id INTEGER) RETURNS INTEGER AS $ $ DECLARE _total INTEGER;

BEGIN
SELECT
    COUNT(*) INTO _total
FROM
    cart_items
WHERE
    user_id = _user_id;

RETURN _total;

END;

$ $ LANGUAGE plpgsql;

CREATE
OR REPLACE FUNCTION delete_cart_item(_id INTEGER) RETURNS VOID AS $ $ BEGIN
DELETE FROM
    cart_items
WHERE
    id = _id;

END;

$ $ LANGUAGE plpgsql;