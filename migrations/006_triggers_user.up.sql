-- Триггер для таблицы subscriptions
CREATE OR REPLACE FUNCTION update_user_counters_on_subscription_change()
RETURNS TRIGGER AS $$
BEGIN
    IF (TG_OP = 'INSERT') THEN
        UPDATE user_counters
        SET count_subscription = count_subscription + 1
        WHERE user_id = NEW.user_id;
    ELSIF (TG_OP = 'DELETE') THEN
        UPDATE user_counters
        SET count_subscription = count_subscription - 1
        WHERE user_id = OLD.user_id;
    END IF;
    RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER subscription_trigger
AFTER INSERT OR DELETE ON subscriptions
FOR EACH ROW EXECUTE PROCEDURE update_user_counters_on_subscription_change();

-- Триггер для таблицы orders
CREATE OR REPLACE FUNCTION update_user_counters_on_order_change()
RETURNS TRIGGER AS $$
BEGIN
    IF (TG_OP = 'INSERT') THEN
        UPDATE user_counters
        SET count_order = count_order + 1
        WHERE user_id = NEW.user_id;
    ELSIF (TG_OP = 'DELETE') THEN
        UPDATE user_counters
        SET count_order = count_order - 1
        WHERE user_id = OLD.user_id;
    END IF;
    RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER order_trigger
AFTER INSERT OR DELETE ON orders
FOR EACH ROW EXECUTE PROCEDURE update_user_counters_on_order_change();