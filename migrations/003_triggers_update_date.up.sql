CREATE
OR REPLACE FUNCTION set_updated_date() RETURNS TRIGGER AS $ $ BEGIN NEW.updated_date = CURRENT_TIMESTAMP;

RETURN NEW;

END;

$ $ LANGUAGE plpgsql;

CREATE TRIGGER update_orders_updated_date
AFTER UPDATE
ON orders 
FOR EACH ROW EXECUTE PROCEDURE set_updated_date();

CREATE TRIGGER update_products_updated_date
AFTER
UPDATE
    ON products FOR EACH ROW EXECUTE PROCEDURE set_updated_date();

CREATE TRIGGER update_users_updated_date
AFTER
UPDATE
    ON users FOR EACH ROW EXECUTE PROCEDURE set_updated_date();