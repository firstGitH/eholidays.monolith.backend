package repositories

import (
	"context"
	"database/sql"
	"fmt"

	dtos "eholidays.monolith/src/domain/DTOs"
	"eholidays.monolith/src/storage"
	"eholidays.monolith/src/storage/postgres"
	_ "github.com/lib/pq"
)

type CategoryRepository interface {
	Create(ctx context.Context, req dtos.CreateCategoryRequest) (dtos.CreateCategoryResponse, error)
	Get(ctx context.Context, req dtos.GetCategoryRequest) (dtos.GetCategoryResponse, error)
	GetItems(ctx context.Context, req dtos.GetCategoriesRequest) (dtos.GetCategoriesResponse, error)
	GetTotalItems(ctx context.Context) (dtos.GetTotalCategoriesResponse, error)
	Update(ctx context.Context, req dtos.UpdateCategoryRequest) error
	Delete(ctx context.Context, req dtos.DeleteCategoryRequest) error
}

type CategoryPgRepository struct {
	db *sql.DB
}

func NewCategoryPgRepository(db *sql.DB) CategoryRepository {
	return &CategoryPgRepository{
		db: db,
	}
}

func (r *CategoryPgRepository) Create(ctx context.Context,
	req dtos.CreateCategoryRequest) (dtos.CreateCategoryResponse, error) {
	tx, err := r.db.BeginTx(ctx, nil)
	if err != nil {
		return dtos.CreateCategoryResponse{}, fmt.Errorf("%w: %v", storage.ErrBeginTx, err)
	}

	stmt, err := tx.PrepareContext(ctx, `SELECT create_category($1, $2)`)
	if err != nil {
		tx.Rollback()
		return dtos.CreateCategoryResponse{}, fmt.Errorf("%w: %v", storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	var id uint
	err = stmt.QueryRowContext(ctx, req.Name, req.Description).Scan(&id)
	if err != nil {
		tx.Rollback()
		return dtos.CreateCategoryResponse{}, fmt.Errorf("%w: %v",
			storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}

	err = tx.Commit()
	if err != nil {
		return dtos.CreateCategoryResponse{}, fmt.Errorf("%w: %v", storage.ErrCommitTx, err)
	}

	return dtos.CreateCategoryResponse{ID: id}, nil
}

func (r *CategoryPgRepository) Get(ctx context.Context,
	req dtos.GetCategoryRequest) (dtos.GetCategoryResponse, error) {
	stmt, err := r.db.PrepareContext(ctx, `SELECT * FROM get_category($1)`)
	if err != nil {
		return dtos.GetCategoryResponse{}, fmt.Errorf("%w: %v", storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	var res dtos.GetCategoryResponse
	err = stmt.QueryRowContext(ctx, req.ID).Scan(&res.ID, &res.Name, &res.Description)
	if err != nil {
		return dtos.GetCategoryResponse{}, fmt.Errorf("%w: %v",
			storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}

	return res, nil
}

func (r *CategoryPgRepository) GetItems(ctx context.Context,
	req dtos.GetCategoriesRequest) (dtos.GetCategoriesResponse, error) {
	stmt, err := r.db.PrepareContext(ctx, `SELECT * FROM get_categories($1, $2)`)
	if err != nil {
		return dtos.GetCategoriesResponse{}, fmt.Errorf("%w: %v", storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	rows, err := stmt.QueryContext(ctx, req.Offset, req.Limit)
	if err != nil {
		return dtos.GetCategoriesResponse{}, fmt.Errorf("%w: %v",
			storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}
	defer rows.Close()

	var res dtos.GetCategoriesResponse
	for rows.Next() {
		var category dtos.CategoryResponse
		err := rows.Scan(&category.ID, &category.Name, &category.Description)
		if err != nil {
			return dtos.GetCategoriesResponse{}, fmt.Errorf("%w: %v",
				storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
		}
		res.Categories = append(res.Categories, category)
	}

	return res, nil
}

func (r *CategoryPgRepository) GetTotalItems(ctx context.Context) (dtos.GetTotalCategoriesResponse,
	error) {
	stmt, err := r.db.PrepareContext(ctx, `SELECT get_total_categories()`)
	if err != nil {
		return dtos.GetTotalCategoriesResponse{}, fmt.Errorf("%w: %v", storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	var total uint
	err = stmt.QueryRowContext(ctx).Scan(&total)
	if err != nil {
		return dtos.GetTotalCategoriesResponse{}, fmt.Errorf("%w: %v",
			storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}

	return dtos.GetTotalCategoriesResponse{Total: total}, nil
}

func (r *CategoryPgRepository) Update(ctx context.Context, req dtos.UpdateCategoryRequest) error {
	tx, err := r.db.BeginTx(ctx, nil)
	if err != nil {
		return fmt.Errorf("%w: %v", storage.ErrBeginTx, err)
	}

	stmt, err := r.db.PrepareContext(ctx, `CALL update_category($1, $2, $3)`)
	if err != nil {
		return fmt.Errorf("%w: %v", storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, req.ID, req.Name, req.Description)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("%w: %v",
			storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}

	err = tx.Commit()
	if err != nil {
		return fmt.Errorf("%w: %v", storage.ErrCommitTx, err)
	}

	return nil
}

func (r *CategoryPgRepository) Delete(ctx context.Context, req dtos.DeleteCategoryRequest) error {
	stmt, err := r.db.PrepareContext(ctx, `CALL delete_category($1)`)
	if err != nil {
		return fmt.Errorf("%w: %v", storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, req.ID)
	if err != nil {
		return fmt.Errorf("%w: %v",
			storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}

	return nil
}
