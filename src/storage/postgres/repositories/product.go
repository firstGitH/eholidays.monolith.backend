package repositories

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"

	errors "eholidays.monolith/src/domain"
	dtos "eholidays.monolith/src/domain/DTOs"
	"eholidays.monolith/src/storage"
	"eholidays.monolith/src/storage/postgres"
	_ "github.com/lib/pq"
)

type ProductRepository interface {
	Create(ctx context.Context, req dtos.CreateProductRequest) (dtos.CreateProductResponse, error)
	CreateWithItems(ctx context.Context,
		req dtos.CreateProductRequest) (dtos.CreateProductResponse, error)
	GetFullInfo(ctx context.Context,
		req dtos.GetProductFullInfoRequest) (dtos.GetProductFullInfoResponse, error)
	GetItemsByCategory(ctx context.Context,
		req dtos.GetProductsByCategoryRequest) (dtos.GetProductsByCategoryResponse, error)
	GetTotalItemsByCategory(ctx context.Context,
		req dtos.GetTotalProductsByCategoryRequest) (dtos.GetTotalProductsByCategoryResponse, error)
	GetItems(ctx context.Context, req dtos.GetProductsRequest) (dtos.GetProductsResponse, error)
	GetTotalItems(ctx context.Context) (dtos.GetTotalProductsResponse, error)
	GetItemsByUser(ctx context.Context,
		req dtos.GetProductsByUserRequest) (dtos.GetProductsByUserResponse, error)
	GetTotalItemsByUser(ctx context.Context,
		req dtos.GetTotalProductsByUserRequest) (dtos.GetTotalProductsByUserResponse, error)
	Delete(ctx context.Context, req dtos.DeleteProductRequest) error
}

type ProductUpdateRepository interface {
	UpdateDescription(ctx context.Context, req dtos.UpdateProductDescriptionRequest) error
	UpdatePrice(ctx context.Context, req dtos.UpdateProductPriceRequest) error
	UpdateCategory(ctx context.Context, req dtos.UpdateProductCategoryRequest) error
	UpdateName(ctx context.Context, req dtos.UpdateProductNameRequest) error
	IncreaseQuantity(ctx context.Context,
		req dtos.IncreaseProductQuantityRequest) (dtos.IncreaseProductQuantityResponse, error)
	DecreaseQuantity(ctx context.Context,
		req dtos.DecreaseProductQuantityRequest) (dtos.DecreaseProductQuantityResponse, error)
	UpdateImage(ctx context.Context, req dtos.UpdateProductImageRequest) error
}

type ProductPgRepository struct {
	db *sql.DB
}

func NewProductPgRepository(db *sql.DB) ProductRepository {
	return &ProductPgRepository{
		db: db,
	}
}

func (r *ProductPgRepository) Create(ctx context.Context,
	req dtos.CreateProductRequest) (dtos.CreateProductResponse, error) {
	tx, err := r.db.BeginTx(ctx, nil)
	if err != nil {
		return dtos.CreateProductResponse{}, fmt.Errorf("%w: %v", storage.ErrBeginTx, err)
	}

	stmt, err := tx.PrepareContext(ctx, `SELECT create_product($1, $2, $3, $4, $5, $6)`)
	if err != nil {
		tx.Rollback()
		return dtos.CreateProductResponse{}, fmt.Errorf("%w: %v", storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	var id uint
	err = stmt.QueryRowContext(ctx,
		req.CategoryID,
		req.Name,
		req.Price,
		req.Description,
		req.Quantity,
		req.Image.Name).Scan(&id)
	if err != nil {
		tx.Rollback()
		return dtos.CreateProductResponse{}, fmt.Errorf("%w: %v",
			storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}

	err = tx.Commit()
	if err != nil {
		return dtos.CreateProductResponse{}, fmt.Errorf("%w: %v", storage.ErrCommitTx, err)
	}

	return dtos.CreateProductResponse{ID: id}, nil
}

func (r *ProductPgRepository) CreateWithItems(ctx context.Context,
	req dtos.CreateProductRequest) (dtos.CreateProductResponse, error) {
	tx, err := r.db.BeginTx(ctx, nil)
	if err != nil {
		return dtos.CreateProductResponse{}, fmt.Errorf("%w: %v", storage.ErrBeginTx, err)
	}

	stmt, err := tx.PrepareContext(ctx,
		`SELECT create_product_with_items($1, $2, $3, $4, $5, $6, $7)`)
	if err != nil {
		tx.Rollback()
		return dtos.CreateProductResponse{}, fmt.Errorf("%w: %v", storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	itemsData, err := json.Marshal(req.Items)
	if err != nil {
		return dtos.CreateProductResponse{}, fmt.Errorf("error marshalling items data: %v", err)
	}

	var id uint
	err = stmt.QueryRowContext(ctx,
		req.CategoryID,
		req.Name,
		req.Price,
		req.Description,
		req.Quantity,
		req.Image.Name,
		itemsData).Scan(&id)
	if err != nil {
		tx.Rollback()
		return dtos.CreateProductResponse{}, fmt.Errorf("%w: %v",
			storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}

	err = tx.Commit()
	if err != nil {
		return dtos.CreateProductResponse{}, fmt.Errorf("%w: %v", storage.ErrCommitTx, err)
	}

	return dtos.CreateProductResponse{ID: id}, nil
}

func (r *ProductPgRepository) GetFullInfo(ctx context.Context,
	req dtos.GetProductFullInfoRequest) (dtos.GetProductFullInfoResponse, error) {
	stmt, err := r.db.PrepareContext(ctx, `SELECT * FROM get_product_full_info($1)`)
	if err != nil {
		return dtos.GetProductFullInfoResponse{}, fmt.Errorf("%w: %v",
			storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	var res dtos.GetProductFullInfoResponse
	err = stmt.QueryRowContext(ctx, req.ProductID).Scan(&res.ID,
		&res.CategoryID,
		&res.Name,
		&res.Price,
		&res.Description,
		&res.Quantity,
		&res.Rating,
		&res.Image.Name,
		&res.CreatedDate,
		&res.UpdatedDate,
		&res.ItemCount,
		&res.SubscriptionCount,
		&res.ReviewCount,
		&res.NotificationCount)
	if err != nil {
		return dtos.GetProductFullInfoResponse{}, fmt.Errorf("%w: %v",
			storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}

	return res, nil
}

func (r *ProductPgRepository) GetItemsByCategory(ctx context.Context,
	req dtos.GetProductsByCategoryRequest) (dtos.GetProductsByCategoryResponse, error) {
	stmt, err := r.db.PrepareContext(ctx, `SELECT * FROM get_products_by_category($1, $2, $3)`)
	if err != nil {
		return dtos.GetProductsByCategoryResponse{}, fmt.Errorf("%w: %v",
			storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	rows, err := stmt.QueryContext(ctx, req.CategoryID, req.Offset, req.Limit)
	if err != nil {
		return dtos.GetProductsByCategoryResponse{}, fmt.Errorf("%w: %v",
			storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}
	defer rows.Close()

	products := make([]dtos.ProductResponse, 0, req.Limit)
	for rows.Next() {
		var product dtos.ProductResponse
		err := rows.Scan(&product.ID,
			&product.CategoryID,
			&product.Name,
			&product.Price,
			&product.Rating,
			&product.Image.Name)
		if err != nil {
			return dtos.GetProductsByCategoryResponse{}, fmt.Errorf("%w: %v",
				storage.ErrScanRow, err)
		}
		products = append(products, product)
	}

	return dtos.GetProductsByCategoryResponse{Products: products}, nil
}

func (r *ProductPgRepository) GetTotalItemsByCategory(ctx context.Context,
	req dtos.GetTotalProductsByCategoryRequest) (dtos.GetTotalProductsByCategoryResponse, error) {
	stmt, err := r.db.PrepareContext(ctx, `SELECT get_total_products_by_category($1)`)
	if err != nil {
		return dtos.GetTotalProductsByCategoryResponse{}, fmt.Errorf("%w: %v",
			storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	var total uint
	err = stmt.QueryRowContext(ctx, req.CategoryID).Scan(&total)
	if err != nil {
		return dtos.GetTotalProductsByCategoryResponse{}, fmt.Errorf("%w: %v",
			storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}

	return dtos.GetTotalProductsByCategoryResponse{Total: total}, nil
}

func (r *ProductPgRepository) GetItems(ctx context.Context,
	req dtos.GetProductsRequest) (dtos.GetProductsResponse, error) {
	stmt, err := r.db.PrepareContext(ctx, `SELECT * FROM get_products($1, $2)`)
	if err != nil {
		return dtos.GetProductsResponse{}, fmt.Errorf("%w: %v", storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	rows, err := stmt.QueryContext(ctx, req.Offset, req.Limit)
	if err != nil {
		return dtos.GetProductsResponse{}, fmt.Errorf("%w: %v",
			storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}
	defer rows.Close()

	products := make([]dtos.ProductResponse, 0, req.Limit)
	for rows.Next() {
		var product dtos.ProductResponse
		err := rows.Scan(&product.ID,
			&product.CategoryID,
			&product.Name,
			&product.Price,
			&product.Rating,
			&product.Image.Name)
		if err != nil {
			return dtos.GetProductsResponse{}, fmt.Errorf("%w: %v", storage.ErrScanRow, err)
		}
		products = append(products, product)
	}

	return dtos.GetProductsResponse{Products: products}, nil
}

func (r *ProductPgRepository) GetTotalItems(ctx context.Context) (
	dtos.GetTotalProductsResponse, error) {
	stmt, err := r.db.PrepareContext(ctx, `SELECT get_total_products()`)
	if err != nil {
		return dtos.GetTotalProductsResponse{}, fmt.Errorf("%w: %v", storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	var total uint
	err = stmt.QueryRowContext(ctx).Scan(&total)
	if err != nil {
		return dtos.GetTotalProductsResponse{}, fmt.Errorf("%w: %v",
			storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}

	return dtos.GetTotalProductsResponse{Total: total}, nil
}

func (r *ProductPgRepository) GetItemsByUser(ctx context.Context,
	req dtos.GetProductsByUserRequest) (dtos.GetProductsByUserResponse, error) {
	stmt, err := r.db.PrepareContext(ctx, `SELECT * FROM get_products_by_user($1, $2, $3)`)
	if err != nil {
		return dtos.GetProductsByUserResponse{}, fmt.Errorf("%w: %v", storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	rows, err := stmt.QueryContext(ctx, req.UserID, req.Limit, req.Offset)
	if err != nil {
		return dtos.GetProductsByUserResponse{}, fmt.Errorf("%w: %v",
			storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}
	defer rows.Close()

	products := make([]dtos.ProductResponse, 0, req.Limit)
	for rows.Next() {
		var product dtos.ProductResponse
		err := rows.Scan(&product.ID,
			&product.CategoryID,
			&product.Name,
			&product.Price,
			&product.Rating,
			&product.Image.Name)
		if err != nil {
			return dtos.GetProductsByUserResponse{}, fmt.Errorf("%w: %v",
				storage.ErrScanRow, err)
		}
		products = append(products, product)
	}

	return dtos.GetProductsByUserResponse{Products: products}, nil
}

func (r *ProductPgRepository) GetTotalItemsByUser(ctx context.Context,
	req dtos.GetTotalProductsByUserRequest) (dtos.GetTotalProductsByUserResponse, error) {
	stmt, err := r.db.PrepareContext(ctx, `SELECT get_total_products_by_user($1)`)
	if err != nil {
		return dtos.GetTotalProductsByUserResponse{}, fmt.Errorf("%w: %v",
			storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	var total uint
	err = stmt.QueryRowContext(ctx, req.UserID).Scan(&total)
	if err != nil {
		return dtos.GetTotalProductsByUserResponse{}, fmt.Errorf("%w: %v",
			storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}

	return dtos.GetTotalProductsByUserResponse{Total: total}, nil
}

func (r *ProductPgRepository) UpdateDescription(ctx context.Context,
	req dtos.UpdateProductDescriptionRequest) error {
	tx, err := r.db.BeginTx(ctx, nil)
	if err != nil {
		return fmt.Errorf("%w: %v", storage.ErrBeginTx, err)
	}

	stmt, err := tx.PrepareContext(ctx, `SELECT update_product_description($1, $2)`)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("%w: %v", storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, req.ID, req.Description)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("%w: %v",
			storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}

	err = tx.Commit()
	if err != nil {
		return fmt.Errorf("%w: %v", storage.ErrCommitTx, err)
	}

	return nil
}

func (r *ProductPgRepository) UpdatePrice(ctx context.Context,
	req dtos.UpdateProductPriceRequest) error {
	tx, err := r.db.BeginTx(ctx, nil)
	if err != nil {
		return fmt.Errorf("%w: %v", storage.ErrBeginTx, err)
	}

	stmt, err := tx.PrepareContext(ctx, `SELECT update_product_price($1, $2)`)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("%w: %v", storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, req.ID, req.Price)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("%w: %v",
			storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}

	err = tx.Commit()
	if err != nil {
		return fmt.Errorf("%w: %v", storage.ErrCommitTx, err)
	}

	return nil
}

func (r *ProductPgRepository) UpdateCategory(ctx context.Context,
	req dtos.UpdateProductCategoryRequest) error {
	tx, err := r.db.BeginTx(ctx, nil)
	if err != nil {
		return fmt.Errorf("%w: %v", storage.ErrBeginTx, err)
	}

	stmt, err := tx.PrepareContext(ctx, `SELECT update_product_category($1, $2)`)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("%w: %v", storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, req.ID, req.CategoryID)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("%w: %v",
			storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}

	err = tx.Commit()
	if err != nil {
		return fmt.Errorf("%w: %v", storage.ErrCommitTx, err)
	}

	return nil
}

func (r *ProductPgRepository) UpdateName(ctx context.Context,
	req dtos.UpdateProductNameRequest) error {
	tx, err := r.db.BeginTx(ctx, nil)
	if err != nil {
		return fmt.Errorf("%w: %v", storage.ErrBeginTx, err)
	}

	stmt, err := tx.PrepareContext(ctx, `SELECT update_product_name($1, $2)`)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("%w: %v", storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, req.ID, req.Name)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("%w: %v",
			storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}

	err = tx.Commit()
	if err != nil {
		return fmt.Errorf("%w: %v", storage.ErrCommitTx, err)
	}

	return nil
}

func (r *ProductPgRepository) IncreaseQuantity(ctx context.Context,
	req dtos.IncreaseProductQuantityRequest) (dtos.IncreaseProductQuantityResponse, error) {
	tx, err := r.db.BeginTx(ctx, nil)
	if err != nil {
		return dtos.IncreaseProductQuantityResponse{}, fmt.Errorf("%w: %v",
			storage.ErrBeginTx, err)
	}

	stmt, err := tx.PrepareContext(ctx, `SELECT increase_product_quantity($1, $2)`)
	if err != nil {
		tx.Rollback()
		return dtos.IncreaseProductQuantityResponse{}, fmt.Errorf("%w: %v",
			storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	var newQuantity int
	err = stmt.QueryRowContext(ctx, req.ID, req.Quantity).Scan(&newQuantity)
	if err != nil {
		tx.Rollback()
		return dtos.IncreaseProductQuantityResponse{}, fmt.Errorf("%w: %v",
			storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}

	err = tx.Commit()
	if err != nil {
		return dtos.IncreaseProductQuantityResponse{}, fmt.Errorf("%w: %v",
			storage.ErrCommitTx, err)
	}

	return dtos.IncreaseProductQuantityResponse{NewQuantity: newQuantity}, nil
}

func (r *ProductPgRepository) DecreaseQuantity(ctx context.Context,
	req dtos.DecreaseProductQuantityRequest) (dtos.DecreaseProductQuantityResponse, error) {
	tx, err := r.db.BeginTx(ctx, nil)
	if err != nil {
		return dtos.DecreaseProductQuantityResponse{}, fmt.Errorf("%w: %v",
			storage.ErrBeginTx, err)
	}

	stmt, err := tx.PrepareContext(ctx, `SELECT decrease_product_quantity($1, $2)`)
	if err != nil {
		tx.Rollback()
		return dtos.DecreaseProductQuantityResponse{}, fmt.Errorf("%w: %v",
			storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	var newQuantity int
	err = stmt.QueryRowContext(ctx, req.ID, req.Quantity).Scan(&newQuantity)
	if err != nil {
		tx.Rollback()
		return dtos.DecreaseProductQuantityResponse{}, fmt.Errorf("%w: %v",
			storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}
	if newQuantity < 0 {
		return dtos.DecreaseProductQuantityResponse{}, fmt.Errorf("%w: %v",
			storage.ErrExecQuery, errors.ErrProductQuantityDecreaseFailed)
	}

	err = tx.Commit()
	if err != nil {
		return dtos.DecreaseProductQuantityResponse{}, fmt.Errorf("%w: %v",
			storage.ErrCommitTx, err)
	}

	return dtos.DecreaseProductQuantityResponse{NewQuantity: newQuantity}, nil
}

func (r *ProductPgRepository) UpdateImage(ctx context.Context,
	req dtos.UpdateProductImageRequest) error {
	tx, err := r.db.BeginTx(ctx, nil)
	if err != nil {
		return fmt.Errorf("%w: %v", storage.ErrBeginTx, err)
	}

	stmt, err := tx.PrepareContext(ctx, `SELECT update_product_image($1, $2)`)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("%w: %v", storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, req.ProductID, req.NewImage.Name)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("%w: %v",
			storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}

	err = tx.Commit()
	if err != nil {
		return fmt.Errorf("%w: %v", storage.ErrCommitTx, err)
	}

	return nil
}

func (r *ProductPgRepository) Delete(ctx context.Context,
	req dtos.DeleteProductRequest) error {
	tx, err := r.db.BeginTx(ctx, nil)
	if err != nil {
		return fmt.Errorf("%w: %v", storage.ErrBeginTx, err)
	}

	stmt, err := tx.PrepareContext(ctx, `SELECT delete_product($1)`)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("%w: %v", storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, req.ID)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("%w: %v",
			storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}

	err = tx.Commit()
	if err != nil {
		return fmt.Errorf("%w: %v", storage.ErrCommitTx, err)
	}

	return nil
}
