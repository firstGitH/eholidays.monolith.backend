package repositories

import (
	"context"
	"database/sql"
	"fmt"

	dtos "eholidays.monolith/src/domain/DTOs"
	"eholidays.monolith/src/storage"
	"eholidays.monolith/src/storage/postgres"
	_ "github.com/lib/pq"
)

type SubscriptionRepository interface {
	Create(ctx context.Context,
		req dtos.CreateSubscriptionRequest) (dtos.CreateSubscriptionResponse, error)
	GetItemsByUser(ctx context.Context, req dtos.GetSubscriptionsByUserRequest) (
		dtos.GetSubscriptionsByUserResponse, error)
	GetTotalItemsByUser(ctx context.Context, req dtos.GetTotalSubscriptionsByUserRequest) (
		dtos.GetTotalSubscriptionsByUserResponse, error)
	GetItems(ctx context.Context,
		req dtos.GetSubscriptionsRequest) (dtos.GetSubscriptionsResponse, error)
	GetTotalItems(ctx context.Context) (dtos.GetTotalSubscriptionsResponse, error)
	Delete(ctx context.Context, req dtos.DeleteSubscriptionRequest) error
	HasSubscription(ctx context.Context, req dtos.HasSubscriptionRequest) (
		dtos.HasSubscriptionResponse, error)
}

type SubscriptionPgRepository struct {
	db *sql.DB
}

func NewSubscriptionPgRepository(db *sql.DB) SubscriptionRepository {
	return &SubscriptionPgRepository{
		db: db,
	}
}

func (r *SubscriptionPgRepository) Create(ctx context.Context,
	req dtos.CreateSubscriptionRequest) (dtos.CreateSubscriptionResponse, error) {
	tx, err := r.db.BeginTx(ctx, nil)
	if err != nil {
		return dtos.CreateSubscriptionResponse{}, fmt.Errorf("%w: %v", storage.ErrBeginTx, err)
	}

	stmt, err := tx.PrepareContext(ctx, `SELECT create_subscription($1, $2)`)
	if err != nil {
		tx.Rollback()
		return dtos.CreateSubscriptionResponse{}, fmt.Errorf("%w: %v", storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	var id uint
	err = stmt.QueryRowContext(ctx, req.UserID, req.ProductID).Scan(&id)
	if err != nil {
		tx.Rollback()
		return dtos.CreateSubscriptionResponse{}, fmt.Errorf("%w: %v",
			storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}

	err = tx.Commit()
	if err != nil {
		return dtos.CreateSubscriptionResponse{}, fmt.Errorf("%w: %v", storage.ErrCommitTx, err)
	}

	return dtos.CreateSubscriptionResponse{ID: id}, nil
}

func (r *SubscriptionPgRepository) GetItemsByUser(ctx context.Context,
	req dtos.GetSubscriptionsByUserRequest) (dtos.GetSubscriptionsByUserResponse, error) {
	stmt, err := r.db.PrepareContext(ctx, `SELECT * FROM get_subscriptions_by_user($1, $2, $3)`)
	if err != nil {
		return dtos.GetSubscriptionsByUserResponse{}, fmt.Errorf("%w: %v",
			storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	rows, err := stmt.QueryContext(ctx, req.UserID, req.Offset, req.Limit)
	if err != nil {
		return dtos.GetSubscriptionsByUserResponse{}, fmt.Errorf("%w: %v",
			storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}
	defer rows.Close()

	subscriptions := make([]dtos.SubscriptionResponse, 0, (req.Limit))
	for rows.Next() {
		var subscription dtos.SubscriptionResponse
		err = rows.Scan(&subscription.ID,
			&subscription.UserID,
			&subscription.ProductID,
			&subscription.ProductName,
			&subscription.CreatedDate)
		if err != nil {
			return dtos.GetSubscriptionsByUserResponse{}, fmt.Errorf("%w: %v",
				storage.ErrExecQuery, err)
		}
		subscriptions = append(subscriptions, subscription)
	}

	return dtos.GetSubscriptionsByUserResponse{Subscriptions: subscriptions}, nil
}

func (r *SubscriptionPgRepository) GetTotalItemsByUser(ctx context.Context,
	req dtos.GetTotalSubscriptionsByUserRequest) (dtos.GetTotalSubscriptionsByUserResponse, error) {
	stmt, err := r.db.PrepareContext(ctx, `SELECT get_total_subscriptions_by_user($1)`)
	if err != nil {
		return dtos.GetTotalSubscriptionsByUserResponse{}, fmt.Errorf("%w: %v",
			storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	var total uint
	err = stmt.QueryRowContext(ctx, req.UserID).Scan(&total)
	if err != nil {
		return dtos.GetTotalSubscriptionsByUserResponse{}, fmt.Errorf("%w: %v",
			storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}

	return dtos.GetTotalSubscriptionsByUserResponse{Total: total}, nil
}

func (r *SubscriptionPgRepository) GetItems(ctx context.Context,
	req dtos.GetSubscriptionsRequest) (dtos.GetSubscriptionsResponse, error) {
	stmt, err := r.db.PrepareContext(ctx, `SELECT * FROM get_subscriptions($1, $2)`)
	if err != nil {
		return dtos.GetSubscriptionsResponse{}, fmt.Errorf("%w: %v",
			storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	rows, err := stmt.QueryContext(ctx, req.Offset, req.Limit)
	if err != nil {
		return dtos.GetSubscriptionsResponse{}, fmt.Errorf("%w: %v",
			storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}
	defer rows.Close()

	subscriptions := make([]dtos.SubscriptionResponse, 0, (req.Limit))
	for rows.Next() {
		var subscription dtos.SubscriptionResponse
		err = rows.Scan(&subscription.ID,
			&subscription.UserID,
			&subscription.ProductID,
			&subscription.ProductName,
			&subscription.CreatedDate)
		if err != nil {
			return dtos.GetSubscriptionsResponse{}, fmt.Errorf("%w: %v",
				storage.ErrExecQuery, err)
		}
		subscriptions = append(subscriptions, subscription)
	}

	return dtos.GetSubscriptionsResponse{Subscriptions: subscriptions}, nil
}

func (r *SubscriptionPgRepository) GetTotalItems(ctx context.
	Context) (dtos.GetTotalSubscriptionsResponse, error) {
	stmt, err := r.db.PrepareContext(ctx, `SELECT get_total_subscriptions()`)
	if err != nil {
		return dtos.GetTotalSubscriptionsResponse{}, fmt.Errorf("%w: %v",
			storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	var total uint
	err = stmt.QueryRowContext(ctx).Scan(&total)
	if err != nil {
		return dtos.GetTotalSubscriptionsResponse{}, fmt.Errorf("%w: %v",
			storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}

	return dtos.GetTotalSubscriptionsResponse{Total: total}, nil
}

func (r *SubscriptionPgRepository) Delete(ctx context.Context,
	req dtos.DeleteSubscriptionRequest) error {
	tx, err := r.db.BeginTx(ctx, nil)
	if err != nil {
		return fmt.Errorf("%w: %v", storage.ErrBeginTx, err)
	}

	stmt, err := tx.PrepareContext(ctx, `SELECT delete_subscription($1, $2)`)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("%w: %v", storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, req.UserID, req.ProductID)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("%w: %v",
			storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}

	err = tx.Commit()
	if err != nil {
		return fmt.Errorf("%w: %v", storage.ErrCommitTx, err)
	}

	return nil
}

func (r *SubscriptionPgRepository) HasSubscription(ctx context.Context,
	req dtos.HasSubscriptionRequest) (dtos.HasSubscriptionResponse, error) {
	stmt, err := r.db.PrepareContext(ctx, `SELECT has_subscription($1, $2)`)
	if err != nil {
		return dtos.HasSubscriptionResponse{}, fmt.Errorf("%w: %v",
			storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	var has bool
	err = stmt.QueryRowContext(ctx, req.UserID, req.ProductID).Scan(&has)
	if err != nil {
		return dtos.HasSubscriptionResponse{}, fmt.Errorf("%w: %v",
			storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}

	return dtos.HasSubscriptionResponse{Has: has}, nil
}
