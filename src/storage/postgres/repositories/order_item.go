package repositories

import (
	"context"
	"database/sql"
	"fmt"

	dtos "eholidays.monolith/src/domain/DTOs"
	"eholidays.monolith/src/storage"
	"eholidays.monolith/src/storage/postgres"
	_ "github.com/lib/pq"
)

type OrderItemRepository interface {
	Create(ctx context.Context,
		req dtos.CreateOrderItemRequest) (dtos.CreateOrderItemResponse, error)
	Get(ctx context.Context,
		req dtos.GetOrderItemRequest) (dtos.GetOrderItemResponse, error)
	GetItems(ctx context.Context,
		req dtos.GetOrderItemsByOrderRequest) (dtos.GetOrderItemsByOrderResponse, error)
	Update(ctx context.Context, req dtos.UpdateOrderItemRequest) error
	Delete(ctx context.Context, req dtos.DeleteOrderItemRequest) error
}

type OrderItemPgRepository struct {
	db *sql.DB
}

func NewOrderItemPgRepository(db *sql.DB) OrderItemRepository {
	return &OrderItemPgRepository{
		db: db,
	}
}

func (r *OrderItemPgRepository) Create(ctx context.Context,
	req dtos.CreateOrderItemRequest) (dtos.CreateOrderItemResponse, error) {
	tx, err := r.db.BeginTx(ctx, nil)
	if err != nil {
		return dtos.CreateOrderItemResponse{}, fmt.Errorf("%w: %v", storage.ErrBeginTx, err)
	}

	stmt, err := tx.PrepareContext(ctx, `SELECT create_order_item($1, $2, $3)`)
	if err != nil {
		tx.Rollback()
		return dtos.CreateOrderItemResponse{}, fmt.Errorf("%w: %v", storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	var id uint
	err = stmt.QueryRowContext(ctx, req.OrderID, req.ProductID, req.Quantity).Scan(&id)
	if err != nil {
		tx.Rollback()
		return dtos.CreateOrderItemResponse{}, fmt.Errorf("%w: %v",
			storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}

	err = tx.Commit()
	if err != nil {
		return dtos.CreateOrderItemResponse{}, fmt.Errorf("%w: %v", storage.ErrCommitTx, err)
	}

	return dtos.CreateOrderItemResponse{ID: id}, nil
}

func (r *OrderItemPgRepository) Get(ctx context.Context,
	req dtos.GetOrderItemRequest) (dtos.GetOrderItemResponse, error) {
	stmt, err := r.db.PrepareContext(ctx, `SELECT * FROM get_order_item($1)`)
	if err != nil {
		return dtos.GetOrderItemResponse{}, fmt.Errorf("%w: %v", storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	var orderItem dtos.GetOrderItemResponse
	err = stmt.QueryRowContext(ctx, req.ID).
		Scan(&orderItem.ID,
			&orderItem.OrderID,
			&orderItem.ProductID,
			&orderItem.Quantity,
			&orderItem.Price,
			&orderItem.Name,
			&orderItem.CurrentPrice)
	if err != nil {
		return dtos.GetOrderItemResponse{}, fmt.Errorf("%w: %v",
			storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}

	return orderItem, nil
}

func (r *OrderItemPgRepository) GetItems(ctx context.Context,
	req dtos.GetOrderItemsByOrderRequest) (dtos.GetOrderItemsByOrderResponse, error) {
	stmt, err := r.db.PrepareContext(ctx, `SELECT * FROM get_order_items_by_order($1, $2, $3)`)
	if err != nil {
		return dtos.GetOrderItemsByOrderResponse{}, fmt.Errorf("%w: %v",
			storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	rows, err := stmt.QueryContext(ctx, req.OrderID, req.Offset, req.Limit)
	if err != nil {
		return dtos.GetOrderItemsByOrderResponse{}, fmt.Errorf("%w: %v",
			storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}
	defer rows.Close()

	orderItems := make([]dtos.GetOrderItemResponse, 0, (req.Limit))
	for rows.Next() {
		var orderItem dtos.GetOrderItemResponse
		err = rows.Scan(&orderItem.ID,
			&orderItem.OrderID,
			&orderItem.ProductID,
			&orderItem.Quantity,
			&orderItem.Price,
			&orderItem.Name,
			&orderItem.CurrentPrice)
		if err != nil {
			return dtos.GetOrderItemsByOrderResponse{},
				fmt.Errorf("%w: %v", storage.ErrScanRow, err)
		}
		orderItems = append(orderItems, orderItem)
	}

	return dtos.GetOrderItemsByOrderResponse{Items: orderItems}, nil
}

func (r *OrderItemPgRepository) Update(ctx context.Context,
	req dtos.UpdateOrderItemRequest) error {
	tx, err := r.db.BeginTx(ctx, nil)
	if err != nil {
		return fmt.Errorf("%w: %v", storage.ErrBeginTx, err)
	}

	stmt, err := tx.PrepareContext(ctx, `CALL update_order_item($1, $2, $3)`)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("%w: %v", storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, req.ID, req.Quantity, req.Price)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("%w: %v", storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}

	err = tx.Commit()
	if err != nil {
		return fmt.Errorf("%w: %v", storage.ErrCommitTx, err)
	}

	return nil
}

func (r *OrderItemPgRepository) Delete(ctx context.Context,
	req dtos.DeleteOrderItemRequest) error {
	tx, err := r.db.BeginTx(ctx, nil)
	if err != nil {
		return fmt.Errorf("%w: %v", storage.ErrBeginTx, err)
	}

	stmt, err := tx.PrepareContext(ctx, `CALL delete_order_item($1)`)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("%w: %v", storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, req.ID)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("%w: %v", storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}

	err = tx.Commit()
	if err != nil {
		return fmt.Errorf("%w: %v", storage.ErrCommitTx, err)
	}

	return nil
}
