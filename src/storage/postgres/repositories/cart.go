package repositories

import (
	"context"
	"database/sql"
	"fmt"

	dtos "eholidays.monolith/src/domain/DTOs"
	"eholidays.monolith/src/storage"
	"eholidays.monolith/src/storage/postgres"
	_ "github.com/lib/pq"
)

type CartRepository interface {
	Create(ctx context.Context, req dtos.CreateCartItemRequest) (dtos.CreateCartItemResponse, error)
	Delete(ctx context.Context, req dtos.DeleteCartItemRequest) error
	GetItems(ctx context.Context, req dtos.GetCartItemsRequest) (dtos.GetCartItemsResponse, error)
	GetTotalItems(ctx context.Context,
		req dtos.GetTotalCartItemRequest) (dtos.GetTotalCartItemsResponse, error)
}

type CartPgRepository struct {
	db *sql.DB
}

func NewCartPgRepository(db *sql.DB) CartRepository {
	return &CartPgRepository{
		db: db,
	}
}

func (r *CartPgRepository) Create(ctx context.Context,
	req dtos.CreateCartItemRequest) (dtos.CreateCartItemResponse, error) {
	tx, err := r.db.BeginTx(ctx, nil)
	if err != nil {
		return dtos.CreateCartItemResponse{}, fmt.Errorf("%w: %v", storage.ErrBeginTx, err)
	}

	stmt, err := tx.PrepareContext(ctx, `SELECT create_cart_item($1, $2)`)
	if err != nil {
		tx.Rollback()
		return dtos.CreateCartItemResponse{}, fmt.Errorf("%w: %v", storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	var id uint
	err = stmt.QueryRowContext(ctx, req.UserID, req.ProductID).Scan(&id)
	if err != nil {
		tx.Rollback()
		return dtos.CreateCartItemResponse{}, fmt.Errorf("%w: %v",
			storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}

	err = tx.Commit()
	if err != nil {
		return dtos.CreateCartItemResponse{}, fmt.Errorf("%w: %v", storage.ErrCommitTx, err)
	}

	return dtos.CreateCartItemResponse{ID: id}, nil
}

func (r *CartPgRepository) Delete(ctx context.Context, req dtos.DeleteCartItemRequest) error {
	tx, err := r.db.BeginTx(ctx, nil)
	if err != nil {
		return fmt.Errorf("%w: %v", storage.ErrBeginTx, err)
	}

	stmt, err := tx.PrepareContext(ctx, `SELECT delete_cart_item($1)`)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("%w: %v", storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, req.ID)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("%w: %v", storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}

	err = tx.Commit()
	if err != nil {
		return fmt.Errorf("%w: %v", storage.ErrCommitTx, err)
	}

	return nil
}

func (r *CartPgRepository) GetItems(ctx context.Context,
	req dtos.GetCartItemsRequest) (dtos.GetCartItemsResponse, error) {
	stmt, err := r.db.PrepareContext(ctx, `SELECT * FROM get_cart_items_by_user($1, $2, $3)`)
	if err != nil {
		return dtos.GetCartItemsResponse{}, fmt.Errorf("%w: %v", storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	rows, err := stmt.QueryContext(ctx, req.UserID, req.Offset, req.Limit)
	if err != nil {
		return dtos.GetCartItemsResponse{}, fmt.Errorf("%w: %v",
			storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}
	defer rows.Close()

	items := make([]dtos.GetCartItemResponse, 0, (req.Limit))
	for rows.Next() {
		var item dtos.GetCartItemResponse
		err = rows.Scan(&item.ID, &item.UserID, &item.ProductID, &item.Name, &item.Price)
		if err != nil {
			return dtos.GetCartItemsResponse{}, fmt.Errorf("%w: %v", storage.ErrScanRow, err)
		}
		items = append(items, item)
	}

	return dtos.GetCartItemsResponse{Items: items}, nil
}

func (r *CartPgRepository) GetTotalItems(ctx context.Context,
	req dtos.GetTotalCartItemRequest) (dtos.GetTotalCartItemsResponse, error) {
	stmt, err := r.db.PrepareContext(ctx, `SELECT get_total_cart_items_by_user($1)`)
	if err != nil {
		return dtos.GetTotalCartItemsResponse{}, fmt.Errorf("%w: %v", storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	var total uint
	err = stmt.QueryRowContext(ctx, req.UserID).Scan(&total)
	if err != nil {
		return dtos.GetTotalCartItemsResponse{}, fmt.Errorf("%w: %v",
			storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}

	return dtos.GetTotalCartItemsResponse{Total: total}, nil
}
