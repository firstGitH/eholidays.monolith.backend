package repositories

import (
	"context"
	"database/sql"
	"fmt"

	dtos "eholidays.monolith/src/domain/DTOs"
	"eholidays.monolith/src/storage"
	"eholidays.monolith/src/storage/postgres"
	_ "github.com/lib/pq"
)

type ProductItemRepository interface {
	Create(ctx context.Context,
		req dtos.CreateSingleProductItemRequest) (dtos.CreateProductItemResponse, error)
	Get(ctx context.Context, req dtos.GetProductItemRequest) (dtos.ProductItemResponse, error)
	GetItems(ctx context.Context,
		req dtos.GetProductItemsByProductRequest) (dtos.GetProductItemsByProductResponse, error)
	GetTotalItems(ctx context.Context,
		req dtos.GetTotalItemsByProductRequest) (dtos.GetTotalItemsByProductResponse, error)
	Update(ctx context.Context, req dtos.UpdateProductItemRequest) error
	Delete(ctx context.Context, req dtos.DeleteProductItemRequest) error
}

type ProductItemPgRepository struct {
	db *sql.DB
}

func NewProductItemPgRepository(db *sql.DB) ProductItemRepository {
	return &ProductItemPgRepository{
		db: db,
	}
}

func (r *ProductItemPgRepository) Create(ctx context.Context,
	req dtos.CreateSingleProductItemRequest) (dtos.CreateProductItemResponse, error) {
	tx, err := r.db.BeginTx(ctx, nil)
	if err != nil {
		return dtos.CreateProductItemResponse{}, fmt.Errorf("%w: %v", storage.ErrBeginTx, err)
	}

	stmt, err := tx.PrepareContext(ctx, `SELECT create_product_item($1, $2, $3)`)
	if err != nil {
		tx.Rollback()
		return dtos.CreateProductItemResponse{}, fmt.Errorf("%w: %v", storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	var id uint
	err = stmt.QueryRowContext(ctx, req.ProductID, req.Name, req.Value).Scan(&id)
	if err != nil {
		tx.Rollback()
		return dtos.CreateProductItemResponse{}, fmt.Errorf("%w: %v",
			storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}

	err = tx.Commit()
	if err != nil {
		return dtos.CreateProductItemResponse{}, fmt.Errorf("%w: %v", storage.ErrCommitTx, err)
	}

	return dtos.CreateProductItemResponse{ID: id}, nil
}

func (r *ProductItemPgRepository) Get(ctx context.Context,
	req dtos.GetProductItemRequest) (dtos.ProductItemResponse, error) {
	stmt, err := r.db.PrepareContext(ctx, `SELECT * FROM get_product_item($1)`)
	if err != nil {
		return dtos.ProductItemResponse{}, fmt.Errorf("%w: %v", storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	var item dtos.ProductItemResponse
	err = stmt.QueryRowContext(ctx, req.ID).
		Scan(&item.ID, &item.ProductID, &item.Name, &item.Value)
	if err != nil {
		return dtos.ProductItemResponse{}, fmt.Errorf("%w: %v",
			storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}

	return item, nil
}

func (r *ProductItemPgRepository) GetItems(ctx context.Context,
	req dtos.GetProductItemsByProductRequest) (dtos.GetProductItemsByProductResponse, error) {
	stmt, err := r.db.PrepareContext(ctx, `SELECT * FROM get_product_items_by_product($1, $2, $3)`)
	if err != nil {
		return dtos.GetProductItemsByProductResponse{}, fmt.Errorf("%w: %v",
			storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	rows, err := stmt.QueryContext(ctx, req.ProductID, req.Offset, req.Limit)
	if err != nil {
		return dtos.GetProductItemsByProductResponse{}, fmt.Errorf("%w: %v",
			storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}
	defer rows.Close()

	items := make([]dtos.ProductItemResponse, 0, (req.Limit))
	for rows.Next() {
		var item dtos.ProductItemResponse
		err = rows.Scan(&item.ID, &item.ProductID, &item.Name, &item.Value)
		if err != nil {
			return dtos.GetProductItemsByProductResponse{}, fmt.Errorf("%w: %v",
				storage.ErrScanRow, err)
		}
		items = append(items, item)
	}

	return dtos.GetProductItemsByProductResponse{Items: items}, nil
}

func (r *ProductItemPgRepository) GetTotalItems(ctx context.Context,
	req dtos.GetTotalItemsByProductRequest) (dtos.GetTotalItemsByProductResponse, error) {
	stmt, err := r.db.PrepareContext(ctx, `SELECT get_total_items_by_product($1)`)
	if err != nil {
		return dtos.GetTotalItemsByProductResponse{}, fmt.Errorf("%w: %v",
			storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	var total uint
	err = stmt.QueryRowContext(ctx, req.ProductID).Scan(&total)
	if err != nil {
		return dtos.GetTotalItemsByProductResponse{}, fmt.Errorf("%w: %v",
			storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}

	return dtos.GetTotalItemsByProductResponse{Total: total}, nil
}

func (r *ProductItemPgRepository) Update(ctx context.Context,
	req dtos.UpdateProductItemRequest) error {
	tx, err := r.db.BeginTx(ctx, nil)
	if err != nil {
		return fmt.Errorf("%w: %v", storage.ErrBeginTx, err)
	}

	stmt, err := tx.PrepareContext(ctx, `SELECT update_product_item($1, $2, $3)`)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("%w: %v", storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, req.ID, req.Name, req.Value)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("%w: %v", storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}

	err = tx.Commit()
	if err != nil {
		return fmt.Errorf("%w: %v", storage.ErrCommitTx, err)
	}

	return nil
}

func (r *ProductItemPgRepository) Delete(ctx context.Context,
	req dtos.DeleteProductItemRequest) error {
	tx, err := r.db.BeginTx(ctx, nil)
	if err != nil {
		return fmt.Errorf("%w: %v", storage.ErrBeginTx, err)
	}

	stmt, err := tx.PrepareContext(ctx, `SELECT delete_product_item($1)`)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("%w: %v", storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, req.ID)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("%w: %v", storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}

	err = tx.Commit()
	if err != nil {
		return fmt.Errorf("%w: %v", storage.ErrCommitTx, err)
	}

	return nil
}
