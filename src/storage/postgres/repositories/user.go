package repositories

import (
	"context"
	"database/sql"
	"fmt"

	dtos "eholidays.monolith/src/domain/DTOs"
	"eholidays.monolith/src/storage"
	"eholidays.monolith/src/storage/postgres"
	_ "github.com/lib/pq"
)

type UserRepository interface {
	Create(ctx context.Context, req dtos.CreateUserRequest) (dtos.CreateUserResponse, error)
	Contains(ctx context.Context,
		req dtos.ContainsUserRequest) (dtos.ContainsUserResponse, error)
	HasRoleByID(ctx context.Context,
		req dtos.UserHasRoleByIDRequest) (dtos.UserHasRoleByIDResponse, error)
	HasRoleByName(ctx context.Context,
		req dtos.UserHasRoleByNameRequest) (dtos.UserHasRoleByNameResponse, error)
	GetSelfInfo(ctx context.Context, req dtos.GetSelfInfoRequest) (dtos.GetSelfInfoResponse, error)
	GetUserInfo(ctx context.Context, req dtos.GetUserInfoRequest) (dtos.GetUserInfoResponse, error)
	GetUsers(ctx context.Context, req dtos.GetUsersRequest) (dtos.GetUsersResponse, error)
	GetTotalUsers(ctx context.Context) (dtos.GetTotalUsersResponse, error)
	Update(ctx context.Context, req dtos.UpdateUserRequest) error
	Delete(ctx context.Context, req dtos.DeleteUserRequest) error
	UpdateLogin(ctx context.Context, req dtos.UpdateLoginRequest) error
	UpdatePassword(ctx context.Context, req dtos.UpdatePasswordRequest) error
	UpdateUsername(ctx context.Context, req dtos.UpdateUsernameRequest) error
}

type UserPgRepository struct {
	db *sql.DB
}

func NewUserPgRepository(db *sql.DB) UserRepository {
	return &UserPgRepository{
		db: db,
	}
}

func (r *UserPgRepository) Create(ctx context.Context,
	req dtos.CreateUserRequest) (dtos.CreateUserResponse, error) {
	tx, err := r.db.BeginTx(ctx, nil)
	if err != nil {
		return dtos.CreateUserResponse{}, fmt.Errorf("%w: %v", storage.ErrBeginTx, err)
	}

	stmt, err := tx.PrepareContext(ctx, `SELECT create_user($1, $2, $3, $4, $5, $6, $7, $8)`)
	if err != nil {
		tx.Rollback()
		return dtos.CreateUserResponse{}, fmt.Errorf("%w: %v", storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	var id uint
	err = stmt.QueryRowContext(ctx,
		req.Login,
		req.Password,
		req.Username,
		req.Fullname,
		req.Gender,
		req.Age,
		req.Phone,
		req.AboutMe).Scan(&id)
	if err != nil {
		tx.Rollback()
		return dtos.CreateUserResponse{}, fmt.Errorf("%w: %v", storage.ErrExecQuery,
			postgres.MapSQLErrorToErrorCode(err))
	}

	err = tx.Commit()
	if err != nil {
		return dtos.CreateUserResponse{}, fmt.Errorf("%w: %v", storage.ErrCommitTx, err)
	}

	return dtos.CreateUserResponse{UserID: id}, nil
}

func (r *UserPgRepository) Contains(ctx context.Context,
	req dtos.ContainsUserRequest) (dtos.ContainsUserResponse, error) {
	tx, err := r.db.BeginTx(ctx, nil)
	if err != nil {
		return dtos.ContainsUserResponse{}, fmt.Errorf("%w: %v", storage.ErrBeginTx, err)
	}

	stmt, err := tx.PrepareContext(ctx, `SELECT contains_user($1, $2)`)
	if err != nil {
		tx.Rollback()
		return dtos.ContainsUserResponse{}, fmt.Errorf("%w: %v", storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	var userID uint
	err = stmt.QueryRowContext(ctx, req.Login, req.Password).Scan(&userID)
	if err != nil {
		tx.Rollback()
		return dtos.ContainsUserResponse{}, fmt.Errorf("%w: %v",
			storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}

	err = tx.Commit()
	if err != nil {
		return dtos.ContainsUserResponse{}, fmt.Errorf("%w: %v", storage.ErrCommitTx, err)
	}

	return dtos.ContainsUserResponse{UserID: userID}, nil
}

func (r *UserPgRepository) HasRoleByID(ctx context.Context,
	req dtos.UserHasRoleByIDRequest) (dtos.UserHasRoleByIDResponse, error) {
	stmt, err := r.db.PrepareContext(ctx, `SELECT user_has_role_by_id($1, $2)`)
	if err != nil {
		return dtos.UserHasRoleByIDResponse{}, fmt.Errorf("%w: %v", storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	var exists bool
	err = stmt.QueryRowContext(ctx, req.UserID, req.RoleID).Scan(&exists)
	if err != nil {
		return dtos.UserHasRoleByIDResponse{}, fmt.Errorf("%w: %v",
			storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}

	return dtos.UserHasRoleByIDResponse{Exists: exists}, nil
}

func (r *UserPgRepository) HasRoleByName(ctx context.Context,
	req dtos.UserHasRoleByNameRequest) (dtos.UserHasRoleByNameResponse, error) {
	stmt, err := r.db.PrepareContext(ctx, `SELECT user_has_role_by_name($1, $2)`)
	if err != nil {
		return dtos.UserHasRoleByNameResponse{}, fmt.Errorf("%w: %v", storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	var exists bool
	err = stmt.QueryRowContext(ctx, req.UserID, req.RoleName).Scan(&exists)
	if err != nil {
		return dtos.UserHasRoleByNameResponse{}, fmt.Errorf("%w: %v",
			storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}

	return dtos.UserHasRoleByNameResponse{Exists: exists}, nil
}

func (r *UserPgRepository) GetSelfInfo(ctx context.Context,
	req dtos.GetSelfInfoRequest) (dtos.GetSelfInfoResponse, error) {
	stmt, err := r.db.PrepareContext(ctx, `SELECT * FROM get_self_info($1)`)
	if err != nil {
		return dtos.GetSelfInfoResponse{}, fmt.Errorf("%w: %v", storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	var user dtos.UserSelfInfoResponse
	err = stmt.QueryRowContext(ctx, req.UserID).Scan(&user.ID,
		&user.Login,
		&user.Username,
		&user.Fullname,
		&user.Gender,
		&user.Age,
		&user.Phone,
		&user.AboutMe)
	if err != nil {
		return dtos.GetSelfInfoResponse{}, fmt.Errorf("%w: %v",
			storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}

	return dtos.GetSelfInfoResponse{User: user}, nil
}

func (r *UserPgRepository) GetUserInfo(ctx context.Context,
	req dtos.GetUserInfoRequest) (dtos.GetUserInfoResponse, error) {
	stmt, err := r.db.PrepareContext(ctx, `SELECT * FROM get_user_info($1)`)
	if err != nil {
		return dtos.GetUserInfoResponse{}, fmt.Errorf("%w: %v", storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	var user dtos.UserInfoResponse
	err = stmt.QueryRowContext(ctx, req.UserID).Scan(&user.ID,
		&user.Username,
		&user.Fullname,
		&user.Gender,
		&user.Age,
		&user.Phone,
		&user.AboutMe)
	if err != nil {
		return dtos.GetUserInfoResponse{}, fmt.Errorf("%w: %v",
			storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}

	return dtos.GetUserInfoResponse{User: user}, nil
}

func (r *UserPgRepository) GetUsers(ctx context.Context,
	req dtos.GetUsersRequest) (dtos.GetUsersResponse, error) {
	stmt, err := r.db.PrepareContext(ctx, `SELECT * FROM get_users($1, $2)`)
	if err != nil {
		return dtos.GetUsersResponse{}, fmt.Errorf("%w: %v", storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	rows, err := stmt.QueryContext(ctx, req.Offset, req.Limit)
	if err != nil {
		return dtos.GetUsersResponse{}, fmt.Errorf("%w: %v",
			storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}
	defer rows.Close()

	users := make([]dtos.UserInfoResponse, 0, (req.Limit))
	for rows.Next() {
		var user dtos.UserInfoResponse
		err = rows.Scan(&user.ID,
			&user.Username,
			&user.Fullname,
			&user.Gender,
			&user.Age,
			&user.Phone,
			&user.AboutMe)
		if err != nil {
			return dtos.GetUsersResponse{}, fmt.Errorf("%w: %v", storage.ErrScanRow, err)
		}
		users = append(users, user)
	}

	return dtos.GetUsersResponse{Users: users}, nil
}

func (r *UserPgRepository) GetTotalUsers(ctx context.Context) (dtos.GetTotalUsersResponse, error) {
	stmt, err := r.db.PrepareContext(ctx, `SELECT get_total_users()`)
	if err != nil {
		return dtos.GetTotalUsersResponse{}, fmt.Errorf("%w: %v", storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	var total uint
	err = stmt.QueryRowContext(ctx).Scan(&total)
	if err != nil {
		return dtos.GetTotalUsersResponse{}, fmt.Errorf("%w: %v",
			storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}

	return dtos.GetTotalUsersResponse{Total: total}, nil
}

func (r *UserPgRepository) Update(ctx context.Context, req dtos.UpdateUserRequest) error {
	tx, err := r.db.BeginTx(ctx, nil)
	if err != nil {
		return fmt.Errorf("%w: %v", storage.ErrBeginTx, err)
	}

	stmt, err := tx.PrepareContext(ctx, `SELECT update_user($1, $2, $3, $4, $5, $6)`)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("%w: %v", storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx,
		req.ID,
		req.Fullname,
		req.Gender,
		req.Age,
		req.Phone,
		req.AboutMe)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("%w: %v", storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}

	err = tx.Commit()
	if err != nil {
		return fmt.Errorf("%w: %v", storage.ErrCommitTx, err)
	}

	return nil
}

func (r *UserPgRepository) Delete(ctx context.Context, req dtos.DeleteUserRequest) error {
	tx, err := r.db.BeginTx(ctx, nil)
	if err != nil {
		return fmt.Errorf("%w: %v", storage.ErrBeginTx, err)
	}

	stmt, err := tx.PrepareContext(ctx, `SELECT delete_user($1)`)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("%w: %v", storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, req.UserID)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("%w: %v", storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}

	err = tx.Commit()
	if err != nil {
		return fmt.Errorf("%w: %v", storage.ErrCommitTx, err)
	}

	return nil
}

func (r *UserPgRepository) UpdateLogin(ctx context.Context, req dtos.UpdateLoginRequest) error {

	tx, err := r.db.BeginTx(ctx, nil)
	if err != nil {
		return fmt.Errorf("%w: %v", storage.ErrBeginTx, err)
	}

	stmt, err := tx.PrepareContext(ctx, `SELECT update_login($1, $2, $3)`)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("%w: %v", storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, req.UserID, req.NewLogin, req.Password)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("%w: %v", storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}

	err = tx.Commit()
	if err != nil {
		return fmt.Errorf("%w: %v", storage.ErrCommitTx, err)
	}

	return nil
}

func (r *UserPgRepository) UpdatePassword(ctx context.Context,
	req dtos.UpdatePasswordRequest) error {

	tx, err := r.db.BeginTx(ctx, nil)
	if err != nil {
		return fmt.Errorf("%w: %v", storage.ErrBeginTx, err)
	}

	stmt, err := tx.PrepareContext(ctx, `SELECT update_password($1, $2, $3)`)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("%w: %v", storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, req.UserID, req.OldPassword, req.NewPassword)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("%w: %v", storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}

	err = tx.Commit()
	if err != nil {
		return fmt.Errorf("%w: %v", storage.ErrCommitTx, err)
	}

	return nil
}

func (r *UserPgRepository) UpdateUsername(ctx context.Context,
	req dtos.UpdateUsernameRequest) error {

	tx, err := r.db.BeginTx(ctx, nil)
	if err != nil {
		return fmt.Errorf("%w: %v", storage.ErrBeginTx, err)
	}

	stmt, err := tx.PrepareContext(ctx, `SELECT update_username($1, $2, $3)`)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("%w: %v", storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, req.UserID, req.Username, req.Password)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("%w: %v", storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}

	err = tx.Commit()
	if err != nil {
		return fmt.Errorf("%w: %v", storage.ErrCommitTx, err)
	}

	return nil
}
