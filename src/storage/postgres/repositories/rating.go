package repositories

import (
	"context"
	"database/sql"
	"fmt"

	dtos "eholidays.monolith/src/domain/DTOs"
	"eholidays.monolith/src/storage"
	"eholidays.monolith/src/storage/postgres"
	_ "github.com/lib/pq"
)

type RatingRepository interface {
	GetItems(ctx context.Context,
		req dtos.GetRatingsRequest) (dtos.GetRatingsResponse, error)
}

type RatingPgRepository struct {
	db *sql.DB
}

func NewRatingPgRepository(db *sql.DB) RatingRepository {
	return &RatingPgRepository{
		db: db,
	}
}

func (r *RatingPgRepository) GetItems(ctx context.Context,
	req dtos.GetRatingsRequest) (dtos.GetRatingsResponse, error) {

	stmt, err := r.db.PrepareContext(ctx, `SELECT * FROM get_ratings($1, $2)`)
	if err != nil {
		return dtos.GetRatingsResponse{}, fmt.Errorf("%w: %v", storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	rows, err := stmt.QueryContext(ctx, req.Offset, req.Limit)
	if err != nil {
		return dtos.GetRatingsResponse{}, fmt.Errorf("%w: %v",
			storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}
	defer rows.Close()

	ratings := make([]dtos.RatingResponse, 0, (req.Limit))
	for rows.Next() {
		var rating dtos.RatingResponse
		err = rows.Scan(&rating.ID, &rating.Value)
		if err != nil {
			return dtos.GetRatingsResponse{}, fmt.Errorf("%w: %v", storage.ErrScanRow, err)
		}
		ratings = append(ratings, rating)
	}

	return dtos.GetRatingsResponse{Ratings: ratings}, nil
}
