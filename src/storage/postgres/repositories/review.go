package repositories

import (
	"context"
	"database/sql"
	"fmt"

	dtos "eholidays.monolith/src/domain/DTOs"
	"eholidays.monolith/src/storage"
	"eholidays.monolith/src/storage/postgres"
	_ "github.com/lib/pq"
)

type ReviewRepository interface {
	Create(ctx context.Context,
		req dtos.CreateReviewRequest) (dtos.CreateReviewResponse, error)
	GetItems(ctx context.Context,
		req dtos.GetReviewsByProductRequest) (dtos.GetReviewsByProductResponse, error)
	GetTotalItems(ctx context.Context,
		req dtos.GetTotalReviewsByProductRequest) (dtos.GetTotalReviewsByProductResponse, error)
	Update(ctx context.Context, req dtos.UpdateReviewRequest) error
	Delete(ctx context.Context, req dtos.DeleteReviewRequest) error
}

type ReviewPgRepository struct {
	db *sql.DB
}

func NewReviewPgRepository(db *sql.DB) ReviewRepository {
	return &ReviewPgRepository{
		db: db,
	}
}

func (r *ReviewPgRepository) Create(ctx context.Context,
	req dtos.CreateReviewRequest) (dtos.CreateReviewResponse, error) {
	tx, err := r.db.BeginTx(ctx, nil)
	if err != nil {
		return dtos.CreateReviewResponse{}, fmt.Errorf("%w: %v", storage.ErrBeginTx, err)
	}

	stmt, err := tx.PrepareContext(ctx, `SELECT create_review($1, $2, $3, $4, $5)`)
	if err != nil {
		tx.Rollback()
		return dtos.CreateReviewResponse{}, fmt.Errorf("%w: %v", storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	var id uint
	err = stmt.QueryRowContext(ctx, req.UserID,
		req.ProductID,
		req.RatingID,
		req.Date,
		req.Content).Scan(&id)
	if err != nil {
		tx.Rollback()
		return dtos.CreateReviewResponse{}, fmt.Errorf("%w: %v", storage.ErrExecQuery,
			postgres.MapSQLErrorToErrorCode(err))
	}

	err = tx.Commit()
	if err != nil {
		return dtos.CreateReviewResponse{}, fmt.Errorf("%w: %v", storage.ErrCommitTx, err)
	}

	return dtos.CreateReviewResponse{ID: id}, nil
}

func (r *ReviewPgRepository) GetItems(ctx context.Context,
	req dtos.GetReviewsByProductRequest) (dtos.GetReviewsByProductResponse, error) {
	stmt, err := r.db.PrepareContext(ctx, `SELECT * FROM get_reviews_by_product($1, $2, $3)`)
	if err != nil {
		return dtos.GetReviewsByProductResponse{}, fmt.Errorf("%w: %v",
			storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	rows, err := stmt.QueryContext(ctx, req.ProductID, req.Limit, req.Offset)
	if err != nil {
		return dtos.GetReviewsByProductResponse{}, fmt.Errorf("%w: %v",
			storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}
	defer rows.Close()

	reviews := make([]dtos.ReviewResponse, 0, (req.Limit))
	for rows.Next() {
		var review dtos.ReviewResponse
		err = rows.Scan(&review.ID,
			&review.UserID,
			&review.ProductID,
			&review.RatingID,
			&review.Username,
			&review.Rating,
			&review.Date,
			&review.Content)
		if err != nil {
			return dtos.GetReviewsByProductResponse{}, fmt.Errorf("%w: %v",
				storage.ErrScanRow, err)
		}
		reviews = append(reviews, review)
	}

	return dtos.GetReviewsByProductResponse{Reviews: reviews}, nil
}

func (r *ReviewPgRepository) GetTotalItems(ctx context.Context,
	req dtos.GetTotalReviewsByProductRequest) (dtos.GetTotalReviewsByProductResponse, error) {
	stmt, err := r.db.PrepareContext(ctx, `SELECT get_total_reviews_by_product($1)`)
	if err != nil {
		return dtos.GetTotalReviewsByProductResponse{}, fmt.Errorf("%w: %v",
			storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	var total uint
	err = stmt.QueryRowContext(ctx, req.ProductID).Scan(&total)
	if err != nil {
		return dtos.GetTotalReviewsByProductResponse{}, fmt.Errorf("%w: %v",
			storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}

	return dtos.GetTotalReviewsByProductResponse{Total: total}, nil
}

func (r *ReviewPgRepository) Update(ctx context.Context,
	req dtos.UpdateReviewRequest) error {
	tx, err := r.db.BeginTx(ctx, nil)
	if err != nil {
		return fmt.Errorf("%w: %v", storage.ErrBeginTx, err)
	}

	stmt, err := tx.PrepareContext(ctx, `SELECT update_review($1, $2, $3)`)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("%w: %v", storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, req.ID, req.RatingID, req.Content)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("%w: %v", storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}

	err = tx.Commit()
	if err != nil {
		return fmt.Errorf("%w: %v", storage.ErrCommitTx, err)
	}

	return nil
}

func (r *ReviewPgRepository) Delete(ctx context.Context,
	req dtos.DeleteReviewRequest) error {
	tx, err := r.db.BeginTx(ctx, nil)
	if err != nil {
		return fmt.Errorf("%w: %v", storage.ErrBeginTx, err)
	}

	stmt, err := tx.PrepareContext(ctx, `SELECT delete_review($1)`)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("%w: %v", storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, req.ID)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("%w: %v", storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}

	err = tx.Commit()
	if err != nil {
		return fmt.Errorf("%w: %v", storage.ErrCommitTx, err)
	}

	return nil
}
