package repositories

import (
	"context"
	"database/sql"
	"fmt"

	dtos "eholidays.monolith/src/domain/DTOs"
	"eholidays.monolith/src/storage"
	"eholidays.monolith/src/storage/postgres"
	_ "github.com/lib/pq"
)

type OrderRepository interface {
	Create(ctx context.Context,
		req dtos.CreateOrderRequest) (dtos.CreateOrderResponse, error)
	GetItems(ctx context.Context,
		req dtos.GetOrdersRequest) (dtos.GetOrdersResponse, error)
	GetTotalItems(ctx context.Context) (dtos.GetTotalOrdersResponse, error)
	GetItemsByUser(ctx context.Context, req dtos.GetOrdersByUserRequest) (dtos.GetOrdersResponse, error)
	GetTotalItemsByUser(ctx context.Context,
		req dtos.GetTotalOrdersByUserRequest) (dtos.GetTotalOrdersByUserResponse, error)
	UpdateStatus(ctx context.Context, req dtos.UpdateOrderStatusRequest) error
	Delete(ctx context.Context, req dtos.DeleteOrderRequest) error
	Confirm(ctx context.Context, req dtos.ConfirmOrderRequest) error
	Cancel(ctx context.Context, req dtos.CancelOrderRequest) error
	Return(ctx context.Context, req dtos.ReturnOrderRequest) error
}

type OrderPgRepository struct {
	db *sql.DB
}

func NewOrderPgRepository(db *sql.DB) OrderRepository {
	return &OrderPgRepository{
		db: db,
	}
}

func (r *OrderPgRepository) Create(ctx context.Context,
	req dtos.CreateOrderRequest) (dtos.CreateOrderResponse, error) {
	tx, err := r.db.BeginTx(ctx, nil)
	if err != nil {
		return dtos.CreateOrderResponse{}, fmt.Errorf("%w: %v", storage.ErrBeginTx, err)
	}

	stmt, err := tx.PrepareContext(ctx, `SELECT create_order($1, $2, $3)`)
	if err != nil {
		tx.Rollback()
		return dtos.CreateOrderResponse{}, fmt.Errorf("%w: %v", storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	var id uint
	err = stmt.QueryRowContext(ctx, req.UserID, req.PaymentMethod, req.DeliveryLocation).Scan(&id)
	if err != nil {
		tx.Rollback()
		return dtos.CreateOrderResponse{}, fmt.Errorf("%w: %v",
			storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}

	err = tx.Commit()
	if err != nil {
		return dtos.CreateOrderResponse{}, fmt.Errorf("%w: %v", storage.ErrCommitTx, err)
	}

	return dtos.CreateOrderResponse{ID: id}, nil
}

func (r *OrderPgRepository) GetItems(ctx context.Context,
	req dtos.GetOrdersRequest) (dtos.GetOrdersResponse, error) {
	stmt, err := r.db.PrepareContext(ctx, `SELECT * FROM get_orders($1, $2)`)
	if err != nil {
		return dtos.GetOrdersResponse{}, fmt.Errorf("%w: %v", storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	rows, err := stmt.QueryContext(ctx, req.Offset, req.Limit)
	if err != nil {
		return dtos.GetOrdersResponse{}, fmt.Errorf("%w: %v",
			storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}
	defer rows.Close()

	orders := make([]dtos.OrderReponse, 0, (req.Limit))
	for rows.Next() {
		var order dtos.OrderReponse
		err = rows.Scan(&order.ID,
			&order.UserID,
			&order.CreatedDate,
			&order.TotalCost,
			&order.Status,
			&order.PaymentMethod,
			&order.DeliveryLocation,
			&order.UpdatedDate)
		if err != nil {
			return dtos.GetOrdersResponse{}, fmt.Errorf("%w: %v", storage.ErrScanRow, err)
		}
		orders = append(orders, order)
	}

	return dtos.GetOrdersResponse{Orders: orders}, nil
}

func (r *OrderPgRepository) GetTotalItems(ctx context.Context) (
	dtos.GetTotalOrdersResponse, error) {
	stmt, err := r.db.PrepareContext(ctx, `SELECT get_total_orders()`)
	if err != nil {
		return dtos.GetTotalOrdersResponse{}, fmt.Errorf("%w: %v", storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	var total uint
	err = stmt.QueryRowContext(ctx).Scan(&total)
	if err != nil {
		return dtos.GetTotalOrdersResponse{}, fmt.Errorf("%w: %v", storage.ErrExecQuery,
			postgres.MapSQLErrorToErrorCode(err))
	}

	return dtos.GetTotalOrdersResponse{Total: total}, nil
}

func (r *OrderPgRepository) GetItemsByUser(ctx context.Context,
	req dtos.GetOrdersByUserRequest) (dtos.GetOrdersResponse, error) {
	stmt, err := r.db.PrepareContext(ctx, `SELECT * FROM get_orders_by_user($1, $2, $3)`)
	if err != nil {
		return dtos.GetOrdersResponse{}, fmt.Errorf("%w: %v", storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	rows, err := stmt.QueryContext(ctx, req.UserID, req.Offset, req.Limit)
	if err != nil {
		return dtos.GetOrdersResponse{}, fmt.Errorf("%w: %v", storage.ErrExecQuery,
			postgres.MapSQLErrorToErrorCode(err))
	}
	defer rows.Close()

	orders := make([]dtos.OrderReponse, 0, (req.Limit))
	for rows.Next() {
		var order dtos.OrderReponse
		err = rows.Scan(&order.ID,
			&order.UserID,
			&order.CreatedDate,
			&order.TotalCost,
			&order.Status,
			&order.PaymentMethod,
			&order.DeliveryLocation,
			&order.UpdatedDate)
		if err != nil {
			return dtos.GetOrdersResponse{}, fmt.Errorf("%w: %v", storage.ErrScanRow, err)
		}
		orders = append(orders, order)
	}

	return dtos.GetOrdersResponse{Orders: orders}, nil
}

func (r *OrderPgRepository) GetTotalItemsByUser(ctx context.Context,
	req dtos.GetTotalOrdersByUserRequest) (dtos.GetTotalOrdersByUserResponse, error) {
	stmt, err := r.db.PrepareContext(ctx, `SELECT get_total_orders_by_user($1)`)
	if err != nil {
		return dtos.GetTotalOrdersByUserResponse{},
			fmt.Errorf("%w: %v", storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	var total uint
	err = stmt.QueryRowContext(ctx, req.UserID).Scan(&total)
	if err != nil {
		return dtos.GetTotalOrdersByUserResponse{},
			fmt.Errorf("%w: %v", storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}

	return dtos.GetTotalOrdersByUserResponse{Total: total}, nil
}

func (r *OrderPgRepository) UpdateStatus(ctx context.Context,
	req dtos.UpdateOrderStatusRequest) error {
	tx, err := r.db.BeginTx(ctx, nil)
	if err != nil {
		return fmt.Errorf("%w: %v", storage.ErrBeginTx, err)
	}

	stmt, err := tx.PrepareContext(ctx, `CALL update_order_status($1, $2)`)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("%w: %v", storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, req.ID, req.Status)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("%w: %v", storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}

	err = tx.Commit()
	if err != nil {
		return fmt.Errorf("%w: %v", storage.ErrCommitTx, err)
	}

	return nil
}

func (r *OrderPgRepository) Delete(ctx context.Context, req dtos.DeleteOrderRequest) error {
	tx, err := r.db.BeginTx(ctx, nil)
	if err != nil {
		return fmt.Errorf("%w: %v", storage.ErrBeginTx, err)
	}

	stmt, err := tx.PrepareContext(ctx, `CALL delete_order($1)`)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("%w: %v", storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, req.ID)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("%w: %v", storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}

	err = tx.Commit()
	if err != nil {
		return fmt.Errorf("%w: %v", storage.ErrCommitTx, err)
	}

	return nil
}

func (r *OrderPgRepository) Confirm(ctx context.Context, req dtos.ConfirmOrderRequest) error {
	tx, err := r.db.BeginTx(ctx, nil)
	if err != nil {
		return fmt.Errorf("%w: %v", storage.ErrBeginTx, err)
	}

	stmt, err := tx.PrepareContext(ctx, `CALL confirm_order($1, $2, $3)`)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("%w: %v", storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, req.ID, req.PaymentMethod, req.DeliveryLocation)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("%w: %v", storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}

	err = tx.Commit()
	if err != nil {
		return fmt.Errorf("%w: %v", storage.ErrCommitTx, err)
	}

	return nil
}

func (r *OrderPgRepository) Cancel(ctx context.Context, req dtos.CancelOrderRequest) error {
	tx, err := r.db.BeginTx(ctx, nil)
	if err != nil {
		return fmt.Errorf("%w: %v", storage.ErrBeginTx, err)
	}

	stmt, err := tx.PrepareContext(ctx, `CALL cancel_order($1)`)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("%w: %v", storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, req.ID)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("%w: %v", storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}

	err = tx.Commit()
	if err != nil {
		return fmt.Errorf("%w: %v", storage.ErrCommitTx, err)
	}

	return nil
}

func (r *OrderPgRepository) Return(ctx context.Context, req dtos.ReturnOrderRequest) error {
	tx, err := r.db.BeginTx(ctx, nil)
	if err != nil {
		return fmt.Errorf("%w: %v", storage.ErrBeginTx, err)
	}

	stmt, err := tx.PrepareContext(ctx, `CALL return_order($1)`)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("%w: %v", storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, req.ID)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("%w: %v", storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}

	err = tx.Commit()
	if err != nil {
		return fmt.Errorf("%w: %v", storage.ErrCommitTx, err)
	}

	return nil
}
