package repositories

import (
	"context"
	"database/sql"
	"fmt"

	dtos "eholidays.monolith/src/domain/DTOs"
	"eholidays.monolith/src/storage"
	"eholidays.monolith/src/storage/postgres"
	_ "github.com/lib/pq"
)

type NotificationRepository interface {
	Create(ctx context.Context,
		req dtos.CreateNotificationRequest) (dtos.CreateNotificationResponse, error)
	GetItems(ctx context.Context,
		req dtos.GetNotificationsRequest) (dtos.GetNotificationsResponse, error)
	GetTotalItems(ctx context.Context) (dtos.GetTotalNotificationsResponse, error)
	GetItemsByProduct(ctx context.Context,
		req dtos.GetNotificationsByProductRequest) (dtos.GetNotificationsByProductResponse, error)
	GetTotalItemsByProduct(ctx context.Context,
		req dtos.GetTotalNotificationsByProductRequest) (
		dtos.GetTotalNotificationsByProductResponse, error)
	Update(ctx context.Context, req dtos.UpdateNotificationRequest) error
	Delete(ctx context.Context, req dtos.DeleteNotificationRequest) error
}

type NotificationPgRepository struct {
	db *sql.DB
}

func NewNotificationPgRepository(db *sql.DB) *NotificationPgRepository {
	return &NotificationPgRepository{
		db: db,
	}
}

func (r *NotificationPgRepository) Create(ctx context.Context,
	req dtos.CreateNotificationRequest) (dtos.CreateNotificationResponse, error) {
	tx, err := r.db.BeginTx(ctx, nil)
	if err != nil {
		return dtos.CreateNotificationResponse{}, fmt.Errorf("%w: %v", storage.ErrBeginTx, err)
	}

	stmt, err := tx.PrepareContext(ctx, `SELECT create_notification($1, $2, $3, $4)`)
	if err != nil {
		tx.Rollback()
		return dtos.CreateNotificationResponse{}, fmt.Errorf("%w: %v", storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	var id uint
	err = stmt.QueryRowContext(ctx, req.ProductID, req.Type, req.Title, req.Content).Scan(&id)
	if err != nil {
		tx.Rollback()
		return dtos.CreateNotificationResponse{}, fmt.Errorf("%w: %v", storage.ErrScanRow, err)
	}

	err = tx.Commit()
	if err != nil {
		return dtos.CreateNotificationResponse{}, fmt.Errorf("%w: %v", storage.ErrCommitTx, err)
	}

	return dtos.CreateNotificationResponse{ID: id}, nil
}

func (r *NotificationPgRepository) GetItems(ctx context.Context,
	req dtos.GetNotificationsRequest) (dtos.GetNotificationsResponse, error) {
	tx, err := r.db.BeginTx(ctx, nil)
	if err != nil {
		return dtos.GetNotificationsResponse{}, fmt.Errorf("%w: %v", storage.ErrBeginTx, err)
	}

	stmt, err := tx.PrepareContext(ctx, `SELECT * FROM get_notifications($1, $2)`)
	if err != nil {
		tx.Rollback()
		return dtos.GetNotificationsResponse{}, fmt.Errorf("%w: %v",
			storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	rows, err := stmt.QueryContext(ctx, req.Offset, req.Limit)
	if err != nil {
		tx.Rollback()
		return dtos.GetNotificationsResponse{}, fmt.Errorf("%w: %v",
			storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}
	defer rows.Close()

	notifications := make([]dtos.NotificationResponse, 0, (req.Limit))
	for rows.Next() {
		var notification dtos.NotificationResponse
		err = rows.Scan(&notification.ID,
			&notification.ProductID,
			&notification.Type,
			&notification.Date,
			&notification.Title,
			&notification.Content)
		if err != nil {
			return dtos.GetNotificationsResponse{},
				fmt.Errorf("%w: %v", storage.ErrScanRow, err)
		}
		notifications = append(notifications, notification)
	}

	err = tx.Commit()
	if err != nil {
		return dtos.GetNotificationsResponse{}, fmt.Errorf("%w: %v", storage.ErrCommitTx, err)
	}

	return dtos.GetNotificationsResponse{Notifications: notifications}, nil
}

func (r *NotificationPgRepository) GetTotalItems(
	ctx context.Context) (dtos.GetTotalNotificationsResponse, error) {

	tx, err := r.db.BeginTx(ctx, nil)
	if err != nil {
		return dtos.GetTotalNotificationsResponse{}, fmt.Errorf("%w: %v",
			storage.ErrBeginTx, err)
	}

	stmt, err := tx.PrepareContext(ctx, `SELECT get_total_notifications()`)
	if err != nil {
		tx.Rollback()
		return dtos.GetTotalNotificationsResponse{}, fmt.Errorf("%w: %v",
			storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	var total uint
	err = stmt.QueryRowContext(ctx).Scan(&total)
	if err != nil {
		tx.Rollback()
		return dtos.GetTotalNotificationsResponse{}, fmt.Errorf("%w: %v",
			storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}

	err = tx.Commit()
	if err != nil {
		return dtos.GetTotalNotificationsResponse{}, fmt.Errorf("%w: %v",
			storage.ErrCommitTx, err)
	}

	return dtos.GetTotalNotificationsResponse{Total: total}, nil
}

func (r *NotificationPgRepository) GetItemsByProduct(ctx context.Context,
	req dtos.GetNotificationsByProductRequest) (dtos.GetNotificationsByProductResponse, error) {
	tx, err := r.db.BeginTx(ctx, nil)
	if err != nil {
		return dtos.GetNotificationsByProductResponse{}, fmt.Errorf("%w: %v",
			storage.ErrBeginTx, err)
	}

	stmt, err := tx.PrepareContext(ctx, `SELECT * FROM get_notifications_by_product($1, $2, $3)`)
	if err != nil {
		tx.Rollback()
		return dtos.GetNotificationsByProductResponse{}, fmt.Errorf("%w: %v",
			storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	rows, err := stmt.QueryContext(ctx, req.ProductID, req.Offset, req.Limit)
	if err != nil {
		tx.Rollback()
		return dtos.GetNotificationsByProductResponse{}, fmt.Errorf("%w: %v",
			storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}
	defer rows.Close()

	notifications := make([]dtos.NotificationResponse, 0, (req.Limit))
	for rows.Next() {
		var notification dtos.NotificationResponse
		err = rows.Scan(&notification.ID,
			&notification.ProductID,
			&notification.Type,
			&notification.Date,
			&notification.Title,
			&notification.Content)
		if err != nil {
			return dtos.GetNotificationsByProductResponse{}, fmt.Errorf("%w: %v",
				storage.ErrScanRow, err)
		}
		notifications = append(notifications, notification)
	}

	err = tx.Commit()
	if err != nil {
		return dtos.GetNotificationsByProductResponse{}, fmt.Errorf("%w: %v",
			storage.ErrCommitTx, err)
	}

	return dtos.GetNotificationsByProductResponse{Notifications: notifications}, nil
}

func (r *NotificationPgRepository) GetTotalItemsByProduct(ctx context.Context,
	req dtos.GetTotalNotificationsByProductRequest) (
	dtos.GetTotalNotificationsByProductResponse, error) {
	tx, err := r.db.BeginTx(ctx, nil)
	if err != nil {
		return dtos.GetTotalNotificationsByProductResponse{}, fmt.Errorf("%w: %v",
			storage.ErrBeginTx, err)
	}

	stmt, err := tx.PrepareContext(ctx, `SELECT get_total_notifications_by_product($1)`)
	if err != nil {
		tx.Rollback()
		return dtos.GetTotalNotificationsByProductResponse{}, fmt.Errorf("%w: %v",
			storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	var total uint
	err = stmt.QueryRowContext(ctx, req.ProductID).Scan(&total)
	if err != nil {
		tx.Rollback()
		return dtos.GetTotalNotificationsByProductResponse{}, fmt.Errorf("%w: %v",
			storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}

	err = tx.Commit()
	if err != nil {
		return dtos.GetTotalNotificationsByProductResponse{}, fmt.Errorf("%w: %v",
			storage.ErrCommitTx, err)
	}

	return dtos.GetTotalNotificationsByProductResponse{Total: total}, nil
}

func (r *NotificationPgRepository) Update(ctx context.Context,
	req dtos.UpdateNotificationRequest) error {
	tx, err := r.db.BeginTx(ctx, nil)
	if err != nil {
		return fmt.Errorf("%w: %v", storage.ErrBeginTx, err)
	}

	stmt, err := tx.PrepareContext(ctx, `SELECT update_notification($1, $2, $3)`)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("%w: %v", storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, req.ID, req.Title, req.Content)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("%w: %v", storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}

	err = tx.Commit()
	if err != nil {
		return fmt.Errorf("%w: %v", storage.ErrCommitTx, err)
	}

	return nil
}

func (r *NotificationPgRepository) Delete(ctx context.Context,
	req dtos.DeleteNotificationRequest) error {
	tx, err := r.db.BeginTx(ctx, nil)
	if err != nil {
		return fmt.Errorf("%w: %v", storage.ErrBeginTx, err)
	}

	stmt, err := tx.PrepareContext(ctx, `SELECT delete_notification($1)`)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("%w: %v", storage.ErrPrepareStmt, err)
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, req.ID)
	if err != nil {
		tx.Rollback()
		return fmt.Errorf("%w: %v", storage.ErrExecQuery, postgres.MapSQLErrorToErrorCode(err))
	}

	err = tx.Commit()
	if err != nil {
		return fmt.Errorf("%w: %v", storage.ErrCommitTx, err)
	}

	return nil
}
