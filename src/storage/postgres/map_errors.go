package postgres

import (
	"fmt"
	"strings"

	"eholidays.monolith/src/storage"
	"github.com/lib/pq"
)

func MapSQLErrorToErrorCode(err error) error {
	if err, ok := err.(*pq.Error); ok {
		switch err.Code {
		case "23505":
			return fmt.Errorf("%w: %v", storage.ErrUniqueValue, err)
		case "23503":
			return fmt.Errorf("%w: %v", storage.ErrForeignKey, err)
		case "23502":
			return fmt.Errorf("%w: %v", storage.ErrNotNull, err)
		case "23514":
			return fmt.Errorf("%w: %v", storage.ErrCheckConstraint, err)
		case "P0002":
			return fmt.Errorf("%w: %v", storage.ErrDataNotFound, err)
		case "22000":
			return fmt.Errorf("%w: %v", storage.ErrDataException, err)
		case "42501":
			return fmt.Errorf("%w: %v", storage.ErrInsufficientPrivilege, err)
		case "25000":
			return fmt.Errorf("%w: %v", storage.ErrInvalidTransactionState, err)
		default:
			switch {
			case strings.Contains(strings.ToLower(err.Error()), "password"):
				return fmt.Errorf("%w: %v", storage.ErrIncorrectPassword, err)
			default:
				return fmt.Errorf("%w: %v", storage.ErrInternalDataBase, err)
			}
		}
	}

	return err
}
