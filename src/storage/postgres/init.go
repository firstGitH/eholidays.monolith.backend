package postgres

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/lib/pq"
)

func NewDatabase(dsn string) *sql.DB {
	db, err := sql.Open("postgres", dsn)
	if err != nil {
		log.Fatal(fmt.Errorf("error opening database: %v", err))
	}

	err = db.Ping()
	if err != nil {
		log.Fatal(fmt.Errorf("error connect database: %v", err))
	}

	return db
}
