package storage

import (
	"errors"
)

var (
	// storage
	ErrBeginTx     = errors.New("failed to begin transaction")
	ErrPrepareStmt = errors.New("failed to prepare statement")
	ErrExecQuery   = errors.New("failed to execute query")
	ErrCommitTx    = errors.New("failed to commit transaction")
	ErrScan        = errors.New("failed to scan data")
	ErrScanRow     = errors.New("failed to scan row")

	ErrInvalidID        = errors.New("invalid ID")
	ErrInternalDataBase = errors.New("internal db error")
	ErrUniqueValue      = errors.New("duplicate unique value")
	ErrForeignKey       = errors.New("foreign key error")
	ErrNotNull          = errors.New("cannot be empty")
	ErrCheckConstraint  = errors.New("check error")
	ErrDataNotFound     = errors.New("data not found")

	ErrDataException            = errors.New("invalid data value")
	ErrIncorrectPassword        = errors.New("invalid password")
	ErrIncorrectPasswordOrLogin = errors.New("invalid password or login")
	ErrInsufficientPrivilege    = errors.New("insufficient privilege")
	ErrInvalidTransactionState  = errors.New("invalid transaction state")
	// file
	ErrImageSaveFailed   = errors.New("image save failed")
	ErrImageGetFailed    = errors.New("image get failed")
	ErrImageUpdateFailed = errors.New("image update failed")
	ErrImageRemoveFailed = errors.New("image remove failed")

	ErrFileExists       = errors.New("file already exists")
	ErrFileDoesNotExist = errors.New("file does not exist")
	ErrNoPermission     = errors.New("has no permissions")
	ErrNoSpace          = errors.New("no space available")
	ErrFileBusy         = errors.New("file is busy in another process")
)
