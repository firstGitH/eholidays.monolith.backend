package middlewares

import (
	"net/http"
	"strings"

	dtos "eholidays.monolith/src/domain/DTOs"
	"eholidays.monolith/src/lib/tokens"
	"eholidays.monolith/src/services"
)

type AuthMiddleware struct {
	jwtService  tokens.JwtService
	roleChecker *services.RoleCheckerService
}

func NewAuthMiddleware(jwtService tokens.JwtService,
	roleChecker *services.RoleCheckerService) *AuthMiddleware {
	return &AuthMiddleware{
		jwtService:  jwtService,
		roleChecker: roleChecker,
	}
}

func (m *AuthMiddleware) WithAuth(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		authHeader := r.Header.Get("Authorization")
		if authHeader == "" {
			http.Error(w, "Authorization header required", http.StatusUnauthorized)
			return
		}

		token := strings.TrimPrefix(authHeader, "Bearer ")
		_, err := m.jwtService.Check(token)
		if err != nil {
			http.Error(w, "Invalid token", http.StatusUnauthorized)
			return
		}

		next.ServeHTTP(w, r)
	})
}

func (m *AuthMiddleware) WithAdmin(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		authHeader := r.Header.Get("Authorization")
		if authHeader == "" {
			http.Error(w, "Authorization header required", http.StatusUnauthorized)
			return
		}

		token := strings.TrimPrefix(authHeader, "Bearer ")

		userID, err := m.jwtService.Check(token)
		if err != nil {
			http.Error(w, "Invalid token", http.StatusUnauthorized)
			return
		}

		hasRoleResp, err := m.roleChecker.HasRoleByName(r.Context(),
			dtos.UserHasRoleByNameRequest{UserID: userID, RoleName: "ADMIN"})
		if err != nil {
			http.Error(w, "Invalid token", http.StatusUnauthorized)
			return
		}

		if !hasRoleResp.Exists {
			http.Error(w, "Admin privilege required", http.StatusForbidden)
			return
		}

		next.ServeHTTP(w, r)
	})
}
