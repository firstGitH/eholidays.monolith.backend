package handlers

import (
	"eholidays.monolith/src/http/middlewares"
	"github.com/go-chi/chi"
)

func InitRoutes(
	r *chi.Mux,
	mw *middlewares.AuthMiddleware,
	authHandler AuthHandler,
	userHandler UserHandler,
	cartHandler CartHandler,
	categoryHandler CategoryHandler,
	notificationHandler NotificationHandler,
	orderHandler OrderHandler,
	orderItemHandler OrderItemHandler,
	productHandler ProductHandler,
	productInfoHandler ProductInfoHandler,
	productItemHandler ProductItemHandler,
	reviewHandler ReviewHandler,
	subscriptionHandler SubscriptionHandler,
) {
	r.Route("/api", func(r chi.Router) {

		r.Route("/auth", func(r chi.Router) {
			r.Post("/signin", authHandler.SignIn)
			r.Post("/signup", authHandler.SignUp)
			r.With(mw.WithAuth).Delete("/signout", authHandler.Logout)
			r.With(mw.WithAuth).Post("/refresh", authHandler.Refresh)
		})

		r.Route("/products", func(r chi.Router) {
			r.With(mw.WithAdmin).Post("/", productHandler.Create)
			r.With(mw.WithAdmin).Delete("/{product_id}", productHandler.Delete)
			r.Get("/items", productHandler.GetItems)
			r.Get("/total", productHandler.GetTotalItems)
			r.Get("/items/category/{category_id}", productHandler.GetItemsByCategory)
			r.Get("/total/category/{category_id}", productHandler.GetTotalItemsByCategory)
			r.Get("/ratings", productHandler.GetRatings)

			r.Route("/categories", func(r chi.Router) {
				r.With(mw.WithAdmin).Post("/", categoryHandler.Create)
				r.With(mw.WithAdmin).Delete("/{id}", categoryHandler.Delete)
				r.Get("/items", categoryHandler.GetItems)
				r.Get("/total", categoryHandler.GetTotalItems)
			})

			r.Route("/{id}", func(r chi.Router) {
				r.Get("/", productHandler.GetFullInfo)

				r.Route("/notifications", func(r chi.Router) {
					r.With(mw.WithAuth).Post("/", notificationHandler.Create)
					r.With(mw.WithAuth).Delete("/{id}", notificationHandler.Delete)
					r.Get("/items", notificationHandler.GetItemsByProduct)
					r.Get("/total", notificationHandler.GetTotalItemsByProduct)
					r.With(mw.WithAuth).Post("/{id}", notificationHandler.Update)
				})

				r.Route("/info", func(r chi.Router) {
					r.With(mw.WithAdmin).Post("/decrease", productInfoHandler.DecreaseQuantity)
					r.With(mw.WithAdmin).Post("/increase", productInfoHandler.IncreaseQuantity)
					r.With(mw.WithAdmin).Post("/category", productInfoHandler.UpdateCategory)
					r.With(mw.WithAdmin).Post("/description", productInfoHandler.UpdateDescription)
					r.With(mw.WithAdmin).Post("/image", productInfoHandler.UpdateImage)
					r.With(mw.WithAdmin).Post("/name", productInfoHandler.UpdateName)
					r.With(mw.WithAdmin).Post("/price", productInfoHandler.UpdatePrice)
				})

				r.Route("/items", func(r chi.Router) {
					r.With(mw.WithAdmin).Post("/", productItemHandler.Create)
					r.With(mw.WithAdmin).Delete("/{id}", productItemHandler.Delete)
					r.Get("/{id}", productItemHandler.Get)
					r.Get("/items", productItemHandler.GetItems)
					r.Get("/total", productItemHandler.GetTotalItems)
					r.With(mw.WithAuth).Post("/{id}", productItemHandler.Update)
				})

				r.Route("/reviews", func(r chi.Router) {
					r.With(mw.WithAuth).Post("/", reviewHandler.Create)
					r.With(mw.WithAuth).Delete("/{review_id}", reviewHandler.Delete)
					r.Get("/items", reviewHandler.GetItems)
					r.Get("/total", reviewHandler.GetTotalItems)
					r.With(mw.WithAuth).Post("/{id}", reviewHandler.Update)
				})

				r.Route("/subscriptions", func(r chi.Router) {
					r.With(mw.WithAuth).Post("/", subscriptionHandler.Create)
					r.With(mw.WithAuth).Delete("/{subscription_id}", subscriptionHandler.Delete)
					r.With(mw.WithAuth).Get("/items", subscriptionHandler.GetItems)
					r.With(mw.WithAuth).Get("/total", subscriptionHandler.GetTotalItems)
				})
			})
		})

		r.Route("/admin", func(r chi.Router) {
			r.With(mw.WithAdmin).Route("/users", func(r chi.Router) {
				r.Get("/items", userHandler.GetUsers)
				r.Get("/total", userHandler.GetTotalUsers)
				r.Delete("/{user_id}", userHandler.Delete)
				r.Get("/info/{user_id}", userHandler.GetUserInfo)
			})

			r.With(mw.WithAdmin).Route("/notifications", func(r chi.Router) {
				r.Get("/items", notificationHandler.GetItems)
				r.Get("/total", notificationHandler.GetTotalItems)
				r.Delete("/{notification_id}", notificationHandler.Delete)
			})
		})
	})
}
