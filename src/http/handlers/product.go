package handlers

import (
	"encoding/json"
	"net/http"
	"strconv"

	dtos "eholidays.monolith/src/domain/DTOs"
	"eholidays.monolith/src/services"
	"github.com/go-chi/chi"
)

type ProductHandlerImpl struct {
	productService services.ProductService
}

type ProductHandler interface {
	Create(w http.ResponseWriter, r *http.Request)
	Delete(w http.ResponseWriter, r *http.Request)
	GetFullInfo(w http.ResponseWriter, r *http.Request)
	GetItems(w http.ResponseWriter, r *http.Request)
	GetItemsByCategory(w http.ResponseWriter, r *http.Request)
	GetItemsByUser(w http.ResponseWriter, r *http.Request)
	GetTotalItems(w http.ResponseWriter, r *http.Request)
	GetTotalItemsByCategory(w http.ResponseWriter, r *http.Request)
	GetTotalItemsByUser(w http.ResponseWriter, r *http.Request)
	GetRatings(w http.ResponseWriter, r *http.Request)
}

func NewProductHandler(productService services.ProductService) ProductHandler {
	return &ProductHandlerImpl{
		productService: productService,
	}
}

func (h *ProductHandlerImpl) Create(w http.ResponseWriter, r *http.Request) {
	var req dtos.CreateProductRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	resp, err := h.productService.Create(r.Context(), req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(resp)
}

func (h *ProductHandlerImpl) Delete(w http.ResponseWriter, r *http.Request) {
	var req dtos.DeleteProductRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = h.productService.Delete(r.Context(), req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func (h *ProductHandlerImpl) GetFullInfo(w http.ResponseWriter, r *http.Request) {
	productID, err := strconv.Atoi(chi.URLParam(r, "id"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	req := dtos.GetProductFullInfoRequest{ProductID: uint(productID)}

	resp, err := h.productService.GetFullInfo(r.Context(), req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(resp)
}

func (h *ProductHandlerImpl) GetItems(w http.ResponseWriter, r *http.Request) {
	var req dtos.GetProductsRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	resp, err := h.productService.GetItems(r.Context(), req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(resp)
}

func (h *ProductHandlerImpl) GetItemsByCategory(w http.ResponseWriter, r *http.Request) {
	categoryID, err := strconv.Atoi(chi.URLParam(r, "category_id"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	req := dtos.GetProductsByCategoryRequest{CategoryID: uint(categoryID)}

	resp, err := h.productService.GetItemsByCategory(r.Context(), req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(resp)
}

func (h *ProductHandlerImpl) GetItemsByUser(w http.ResponseWriter, r *http.Request) {
	userID, err := strconv.Atoi(chi.URLParam(r, "user_id"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	req := dtos.GetProductsByUserRequest{UserID: uint(userID)}

	resp, err := h.productService.GetItemsByUser(r.Context(), req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(resp)
}

func (h *ProductHandlerImpl) GetTotalItems(w http.ResponseWriter, r *http.Request) {

	resp, err := h.productService.GetTotalItems(r.Context())
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(resp)
}

func (h *ProductHandlerImpl) GetTotalItemsByCategory(w http.ResponseWriter, r *http.Request) {
	categoryID, err := strconv.Atoi(chi.URLParam(r, "category_id"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	req := dtos.GetTotalProductsByCategoryRequest{CategoryID: uint(categoryID)}

	resp, err := h.productService.GetTotalItemsByCategory(r.Context(), req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(resp)
}

func (h *ProductHandlerImpl) GetTotalItemsByUser(w http.ResponseWriter, r *http.Request) {
	userID, err := strconv.Atoi(chi.URLParam(r, "user_id"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	req := dtos.GetTotalProductsByUserRequest{UserID: uint(userID)}

	resp, err := h.productService.GetTotalItemsByUser(r.Context(), req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(resp)
}

func (h *ProductHandlerImpl) GetRatings(w http.ResponseWriter, r *http.Request) {
	var req dtos.GetRatingsRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	resp, err := h.productService.GetRatings(r.Context(), req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(resp)
}
