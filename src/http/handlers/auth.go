package handlers

import (
	"encoding/json"
	"net/http"
	"strings"

	dtos "eholidays.monolith/src/domain/DTOs"
	"eholidays.monolith/src/lib/tokens"
	"eholidays.monolith/src/services"
)

type AuthHandler interface {
	SignIn(w http.ResponseWriter, r *http.Request)
	SignUp(w http.ResponseWriter, r *http.Request)
	Logout(w http.ResponseWriter, r *http.Request)
	Refresh(w http.ResponseWriter, r *http.Request)
}

type AuthHandlerImpl struct {
	authService services.AuthService
	jwtService  tokens.JwtService
}

func NewAuthHandler(authService services.AuthService, jwtService tokens.JwtService) AuthHandler {
	return &AuthHandlerImpl{
		authService: authService,
		jwtService:  jwtService,
	}
}

func (h *AuthHandlerImpl) SignIn(w http.ResponseWriter, r *http.Request) {
	var req dtos.ContainsUserRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	resp, err := h.authService.SignIn(r.Context(), req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	token, err := h.jwtService.Create(&tokens.JwtUserDate{ID: resp.UserID})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Authorization", "Bearer "+token)
	json.NewEncoder(w).Encode(resp)
}

func (h *AuthHandlerImpl) SignUp(w http.ResponseWriter, r *http.Request) {
	var req dtos.CreateUserRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	resp, err := h.authService.SignUp(r.Context(), req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	token, err := h.jwtService.Create(&tokens.JwtUserDate{ID: resp.UserID})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Authorization", "Bearer "+token)
	json.NewEncoder(w).Encode(resp)
}

func (h *AuthHandlerImpl) Logout(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Authorization", "Unauthorized")
	w.WriteHeader(http.StatusOK)
}

func (h *AuthHandlerImpl) Refresh(w http.ResponseWriter, r *http.Request) {
	authHeader := r.Header.Get("Authorization")
	if authHeader == "" {
		http.Error(w, "Authorization header required", http.StatusUnauthorized)
		return
	}

	token := strings.TrimPrefix(authHeader, "Bearer ")
	newToken, err := h.jwtService.Refresh(token)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Authorization", "Bearer "+newToken)
	json.NewEncoder(w).Encode(newToken)
}
