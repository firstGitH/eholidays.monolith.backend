package handlers

import (
	"encoding/json"
	"net/http"

	dtos "eholidays.monolith/src/domain/DTOs"
	"eholidays.monolith/src/lib/tokens"
	"eholidays.monolith/src/services"
)

type ProductInfoHandler interface {
	DecreaseQuantity(w http.ResponseWriter, r *http.Request)
	IncreaseQuantity(w http.ResponseWriter, r *http.Request)
	UpdateCategory(w http.ResponseWriter, r *http.Request)
	UpdateDescription(w http.ResponseWriter, r *http.Request)
	UpdateImage(w http.ResponseWriter, r *http.Request)
	UpdateName(w http.ResponseWriter, r *http.Request)
	UpdatePrice(w http.ResponseWriter, r *http.Request)
}

type ProductInfoHandlerImpl struct {
	productService services.ProductInfoService
}

func NewProductInfoHandler(productService services.ProductInfoService,
	jwtService tokens.JwtService) ProductInfoHandler {
	return &ProductInfoHandlerImpl{
		productService: productService,
	}
}

func (h *ProductInfoHandlerImpl) DecreaseQuantity(w http.ResponseWriter, r *http.Request) {
	var req dtos.DecreaseProductQuantityRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	resp, err := h.productService.DecreaseQuantity(r.Context(), req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(resp)
}

func (h *ProductInfoHandlerImpl) IncreaseQuantity(w http.ResponseWriter, r *http.Request) {
	var req dtos.IncreaseProductQuantityRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	resp, err := h.productService.IncreaseQuantity(r.Context(), req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(resp)
}

func (h *ProductInfoHandlerImpl) UpdateCategory(w http.ResponseWriter, r *http.Request) {
	var req dtos.UpdateProductCategoryRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = h.productService.UpdateCategory(r.Context(), req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func (h *ProductInfoHandlerImpl) UpdateDescription(w http.ResponseWriter, r *http.Request) {
	var req dtos.UpdateProductDescriptionRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = h.productService.UpdateDescription(r.Context(), req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func (h *ProductInfoHandlerImpl) UpdateImage(w http.ResponseWriter, r *http.Request) {
	var req dtos.UpdateProductImageRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = h.productService.UpdateImage(r.Context(), req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func (h *ProductInfoHandlerImpl) UpdateName(w http.ResponseWriter, r *http.Request) {
	var req dtos.UpdateProductNameRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = h.productService.UpdateName(r.Context(), req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func (h *ProductInfoHandlerImpl) UpdatePrice(w http.ResponseWriter, r *http.Request) {
	var req dtos.UpdateProductPriceRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = h.productService.UpdatePrice(r.Context(), req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}
