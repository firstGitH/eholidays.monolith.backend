package handlers

import (
	"encoding/json"
	"net/http"
	"strconv"

	dtos "eholidays.monolith/src/domain/DTOs"
	"eholidays.monolith/src/services"
	"github.com/go-chi/chi"
)

type ReviewHandler interface {
	Create(w http.ResponseWriter, r *http.Request)
	Delete(w http.ResponseWriter, r *http.Request)
	GetItems(w http.ResponseWriter, r *http.Request)
	GetTotalItems(w http.ResponseWriter, r *http.Request)
	Update(w http.ResponseWriter, r *http.Request)
}

type ReviewHandlerImpl struct {
	reviewService services.ReviewService
}

func NewReviewHandler(reviewService services.ReviewService) ReviewHandler {
	return &ReviewHandlerImpl{
		reviewService: reviewService,
	}
}

func (h *ReviewHandlerImpl) Create(w http.ResponseWriter, r *http.Request) {
	var req dtos.CreateReviewRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	resp, err := h.reviewService.Create(r.Context(), req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(resp)
}

func (h *ReviewHandlerImpl) Delete(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.Atoi(chi.URLParam(r, "id"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	req := dtos.DeleteReviewRequest{ID: uint(id)}
	err = h.reviewService.Delete(r.Context(), req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func (h *ReviewHandlerImpl) GetItems(w http.ResponseWriter, r *http.Request) {
	var req dtos.GetReviewsByProductRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	resp, err := h.reviewService.GetItems(r.Context(), req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(resp)
}

func (h *ReviewHandlerImpl) GetTotalItems(w http.ResponseWriter, r *http.Request) {
	var req dtos.GetTotalReviewsByProductRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	resp, err := h.reviewService.GetTotalItems(r.Context(), req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(resp)
}

func (h *ReviewHandlerImpl) Update(w http.ResponseWriter, r *http.Request) {
	var req dtos.UpdateReviewRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = h.reviewService.Update(r.Context(), req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}
