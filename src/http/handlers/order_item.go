package handlers

import (
	"encoding/json"
	"net/http"
	"strconv"

	dtos "eholidays.monolith/src/domain/DTOs"
	"eholidays.monolith/src/services"
	"github.com/go-chi/chi"
)

type OrderItemHandler interface {
	Create(w http.ResponseWriter, r *http.Request)
	Delete(w http.ResponseWriter, r *http.Request)
	Get(w http.ResponseWriter, r *http.Request)
	GetItems(w http.ResponseWriter, r *http.Request)
}

type OrderItemHandlerImpl struct {
	orderItemService services.OrderItemService
}

func NewOrderItemHandler(orderItemService services.OrderItemService) OrderItemHandler {
	return &OrderItemHandlerImpl{
		orderItemService: orderItemService,
	}
}

func (h *OrderItemHandlerImpl) Create(w http.ResponseWriter, r *http.Request) {
	var req dtos.CreateOrderItemRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	resp, err := h.orderItemService.Create(r.Context(), req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(resp)
}

func (h *OrderItemHandlerImpl) Delete(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.Atoi(chi.URLParam(r, "id"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	req := dtos.DeleteOrderItemRequest{ID: uint(id)}
	err = h.orderItemService.Delete(r.Context(), req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func (h *OrderItemHandlerImpl) Get(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.Atoi(chi.URLParam(r, "id"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	req := dtos.GetOrderItemRequest{ID: uint(id)}
	resp, err := h.orderItemService.Get(r.Context(), req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(resp)
}

func (h *OrderItemHandlerImpl) GetItems(w http.ResponseWriter, r *http.Request) {
	var req dtos.GetOrderItemsByOrderRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	resp, err := h.orderItemService.GetItems(r.Context(), req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(resp)
}
