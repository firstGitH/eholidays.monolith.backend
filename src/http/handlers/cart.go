package handlers

import (
	"encoding/json"
	"net/http"
	"strconv"

	dtos "eholidays.monolith/src/domain/DTOs"
	"eholidays.monolith/src/services"
	"github.com/go-chi/chi"
)

type CartHandler interface {
	Create(w http.ResponseWriter, r *http.Request)
	Delete(w http.ResponseWriter, r *http.Request)
	GetItems(w http.ResponseWriter, r *http.Request)
	GetTotalItems(w http.ResponseWriter, r *http.Request)
}

type CartHandlerImpl struct {
	cartService services.CartService
}

func NewCartHandler(cartService services.CartService) CartHandler {
	return &CartHandlerImpl{
		cartService: cartService,
	}
}

func (h *CartHandlerImpl) Create(w http.ResponseWriter, r *http.Request) {
	var req dtos.CreateCartItemRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	resp, err := h.cartService.Create(r.Context(), req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(resp)
}

func (h *CartHandlerImpl) Delete(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.Atoi(chi.URLParam(r, "id"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	req := dtos.DeleteCartItemRequest{ID: uint(id)}
	err = h.cartService.Delete(r.Context(), req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func (h *CartHandlerImpl) GetItems(w http.ResponseWriter, r *http.Request) {
	userID, err := strconv.Atoi(chi.URLParam(r, "userID"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	req := dtos.GetCartItemsRequest{UserID: uint(userID)}
	resp, err := h.cartService.GetItems(r.Context(), req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(resp)
}

func (h *CartHandlerImpl) GetTotalItems(w http.ResponseWriter, r *http.Request) {
	userID, err := strconv.Atoi(chi.URLParam(r, "userID"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	req := dtos.GetTotalCartItemRequest{UserID: uint(userID)}
	resp, err := h.cartService.GetTotalItems(r.Context(), req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(resp)
}
