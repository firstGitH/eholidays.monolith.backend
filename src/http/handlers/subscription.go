package handlers

import (
	"encoding/json"
	"net/http"

	dtos "eholidays.monolith/src/domain/DTOs"
	"eholidays.monolith/src/services"
)

type SubscriptionHandler interface {
	Create(w http.ResponseWriter, r *http.Request)
	Delete(w http.ResponseWriter, r *http.Request)
	GetItems(w http.ResponseWriter, r *http.Request)
	GetItemsByUser(w http.ResponseWriter, r *http.Request)
	GetTotalItems(w http.ResponseWriter, r *http.Request)
	GetTotalItemsByUser(w http.ResponseWriter, r *http.Request)
}

type SubscriptionHandlerImpl struct {
	subscriptionService services.SubscriptionService
}

func NewSubscriptionHandler(subscriptionService services.SubscriptionService) SubscriptionHandler {
	return &SubscriptionHandlerImpl{
		subscriptionService: subscriptionService,
	}
}

func (h *SubscriptionHandlerImpl) Create(w http.ResponseWriter, r *http.Request) {
	var req dtos.CreateSubscriptionRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	resp, err := h.subscriptionService.Create(r.Context(), req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(resp)
}

func (h *SubscriptionHandlerImpl) Delete(w http.ResponseWriter, r *http.Request) {
	var req dtos.DeleteSubscriptionRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = h.subscriptionService.Delete(r.Context(), req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func (h *SubscriptionHandlerImpl) GetItems(w http.ResponseWriter, r *http.Request) {
	var req dtos.GetSubscriptionsRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	resp, err := h.subscriptionService.GetItems(r.Context(), req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(resp)
}

func (h *SubscriptionHandlerImpl) GetItemsByUser(w http.ResponseWriter, r *http.Request) {
	var req dtos.GetSubscriptionsByUserRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	resp, err := h.subscriptionService.GetItemsByUser(r.Context(), req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(resp)
}

func (h *SubscriptionHandlerImpl) GetTotalItems(w http.ResponseWriter, r *http.Request) {
	resp, err := h.subscriptionService.GetTotalItems(r.Context())
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(resp)
}

func (h *SubscriptionHandlerImpl) GetTotalItemsByUser(w http.ResponseWriter, r *http.Request) {
	var req dtos.GetTotalSubscriptionsByUserRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	resp, err := h.subscriptionService.GetTotalItemsByUser(r.Context(), req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(resp)
}
