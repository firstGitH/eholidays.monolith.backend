package handlers

import (
	"encoding/json"
	"net/http"
	"strconv"

	dtos "eholidays.monolith/src/domain/DTOs"
	"eholidays.monolith/src/services"
	"github.com/go-chi/chi"
)

type CategoryHandler interface {
	Create(w http.ResponseWriter, r *http.Request)
	Delete(w http.ResponseWriter, r *http.Request)
	GetItems(w http.ResponseWriter, r *http.Request)
	GetTotalItems(w http.ResponseWriter, r *http.Request)
}

type CategoryHandlerImpl struct {
	categoryService services.CategoryService
}

func NewCategoryHandler(categoryService services.CategoryService) CategoryHandler {
	return &CategoryHandlerImpl{
		categoryService: categoryService,
	}
}

func (h *CategoryHandlerImpl) Create(w http.ResponseWriter, r *http.Request) {
	var req dtos.CreateCategoryRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	resp, err := h.categoryService.Create(r.Context(), req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(resp)
}

func (h *CategoryHandlerImpl) Delete(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.Atoi(chi.URLParam(r, "id"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	req := dtos.DeleteCategoryRequest{ID: uint(id)}
	err = h.categoryService.Delete(r.Context(), req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func (h *CategoryHandlerImpl) GetItems(w http.ResponseWriter, r *http.Request) {
	var req dtos.GetCategoriesRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	resp, err := h.categoryService.GetItems(r.Context(), req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(resp)
}

func (h *CategoryHandlerImpl) GetTotalItems(w http.ResponseWriter, r *http.Request) {
	resp, err := h.categoryService.GetTotalItems(r.Context())
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(resp)
}
