package errors

import "errors"

var (
	ErrIncorrectData       = errors.New("invalid data")
	ErrIncorrectDataFormat = errors.New("invalid data format")

	ErrUserAlreadyExists    = errors.New("user with this login or nickname already exists")
	ErrUserUsernameExists   = errors.New("user with this username already exists")
	ErrUserLoginExists      = errors.New("login is not available")
	ErrUserPasswordMismatch = errors.New("invalid password")
	ErrUserNotFound         = errors.New("user not found")
	ErrUserDeleteFailed     = errors.New("failed to delete user ")

	ErrTokenExpired     = errors.New("token expired")
	ErrTokenInvalid     = errors.New("user is not authorized")
	ErrTokenParseFailed = errors.New("token parse failed")

	ErrProductExists                 = errors.New("product already exists")
	ErrProductNotFound               = errors.New("product not found")
	ErrProductDelete                 = errors.New("product deletion failed")
	ErrProductQuantityNotEnough      = errors.New("not enough product quantity")
	ErrProductQuantityIncreaseFailed = errors.New("failed to increase product quantity")
	ErrProductQuantityZero           = errors.New("change product quantity 0")
	ErrProductQuantityDecreaseFailed = errors.New("failed to decrease product quantity")
	ErrProductGetFailed              = errors.New("failed to get product")
	ErrCategoryGetFailed             = errors.New("failed to get category")

	ErrReviewAddFailed          = errors.New("failed to add review")
	ErrReviewDeleteFailed       = errors.New("failed to remove review")
	ErrReviewExists             = errors.New("user has already reviewed this product")
	ErrReviewGetByProductFailed = errors.New("failed to get reviews by product")
	ErrReviewGetByUserFailed    = errors.New("failed to get reviews by user")
	ErrReviewGetFailed          = errors.New("failed to get reviews")
	ErrRatingGetFailed          = errors.New("failed to get ratings")
	ErrRatingNotFound           = errors.New("rating not found")

	ErrSubscriptionAddFailed               = errors.New("failed to add subscription")
	ErrSubscriptionDeleteFailed            = errors.New("failed to remove subscription")
	ErrSubscriptionGetByDateFailed         = errors.New("failed to get subscriptions by date")
	ErrSubscriptionGetByUserFailed         = errors.New("failed to get subscriptions by user")
	ErrSubscriptionGetByProductFailed      = errors.New("failed to get subscriptions by product")
	ErrNotificationAddFailed               = errors.New("failed to add notification")
	ErrNotificationDeleteFailed            = errors.New("failed to remove notification")
	ErrNotificationGetBySubscriptionFailed = errors.New("failed to get notifications by subscription")

	ErrInternalServiceError  = errors.New("internal service error")
	ErrInvalidOperationError = errors.New("invalid operation")
)
