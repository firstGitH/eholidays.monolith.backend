package dtos

import (
	"io"
	"time"

	_ "github.com/go-playground/validator/v10"
)

type ProductImage struct {
	Name string
	Data io.Reader
	Ext  string
}

type CreateProductRequest struct {
	CategoryID  uint                       `validate:"required,gt=0"`
	Name        string                     `validate:"required,min=3,max=50"`
	Price       float64                    `validate:"required,gt=0"`
	Description string                     `validate:"omitempty,max=350"`
	Quantity    uint                       `validate:"omitempty,min=0"`
	Image       ProductImage               `validate:"required"`
	Items       []CreateProductItemRequest `validate:"omitempty,dive"`
}

type CreateProductResponse struct {
	ID    uint
	Items []CreateProductItemResponse
}

type GetProductFullInfoRequest struct {
	ProductID uint `validate:"required,gt=0"`
}

type GetProductFullInfoResponse struct {
	ID                uint
	CategoryID        uint
	Name              string
	Price             float64
	Description       string
	Quantity          uint
	Rating            float64
	Image             ProductImage
	CreatedDate       time.Time
	UpdatedDate       time.Time
	ItemCount         uint
	SubscriptionCount uint
	ReviewCount       uint
	NotificationCount uint
}

type GetProductsByCategoryRequest struct {
	CategoryID uint `validate:"required,gt=0"`
	Offset     uint `validate:"omitempty"`
	Limit      uint `validate:"omitempty"`
}

type GetProductsByCategoryResponse struct {
	Products []ProductResponse
}

type ProductResponse struct {
	ID         uint
	CategoryID uint
	Name       string
	Price      float64
	Rating     float64
	Image      ProductImage
}

type GetTotalProductsByCategoryRequest struct {
	CategoryID uint `validate:"required,gt=0"`
}

type GetTotalProductsByCategoryResponse struct {
	Total uint
}

type GetProductsRequest struct {
	Offset uint `validate:"required"`
	Limit  uint `validate:"required"`
}

type GetProductsResponse struct {
	Products []ProductResponse
}

type GetTotalProductsResponse struct {
	Total uint
}

type GetProductsByUserRequest struct {
	UserID uint `validate:"required,gt=0"`
	Limit  uint `validate:"required"`
	Offset uint `validate:"required"`
}

type GetProductsByUserResponse struct {
	Products []ProductResponse
}

type GetTotalProductsByUserRequest struct {
	UserID uint `validate:"required,gt=0"`
}

type GetTotalProductsByUserResponse struct {
	Total uint
}

type UpdateProductDescriptionRequest struct {
	ID          uint   `validate:"required,gt=0"`
	Description string `validate:"required,max=350"`
}

type UpdateProductPriceRequest struct {
	ID    uint    `validate:"required,gt=0"`
	Price float64 `validate:"required,gt=0"`
}

type UpdateProductCategoryRequest struct {
	ID         uint `validate:"required,gt=0"`
	CategoryID uint `validate:"required,gt=0"`
}

type UpdateProductNameRequest struct {
	ID   uint   `validate:"required,gt=0"`
	Name string `validate:"required,min=3,max=50"`
}

type IncreaseProductQuantityRequest struct {
	ID       uint `validate:"required,gt=0"`
	Quantity uint `validate:"required,gt=0"`
}

type IncreaseProductQuantityResponse struct {
	NewQuantity int
}

type DecreaseProductQuantityRequest struct {
	ID       uint `validate:"required,gt=0"`
	Quantity uint `validate:"required,gt=0"`
}

type DecreaseProductQuantityResponse struct {
	NewQuantity int
}

type UpdateProductImageRequest struct {
	ProductID uint         `validate:"required,gt=0"`
	OldName   string       `validate:"required"`
	NewImage  ProductImage `validate:"required"`
}

type DeleteProductRequest struct {
	ID        uint   `validate:"required,gt=0"`
	ImageName string `validate:"omitempty"`
}
