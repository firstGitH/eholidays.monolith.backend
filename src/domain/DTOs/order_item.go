package dtos

import (
	_ "github.com/go-playground/validator/v10"
)

type CreateOrderItemRequest struct {
	OrderID   uint `validate:"required,gt=0"`
	ProductID uint `validate:"required,gt=0"`
	Quantity  uint `validate:"required,gte=0"`
}

type CreateOrderItemResponse struct {
	ID uint
}

type GetOrderItemRequest struct {
	ID uint `validate:"required,gt=0"`
}

type GetOrderItemResponse struct {
	ID           uint
	OrderID      uint
	ProductID    uint
	Quantity     uint
	Price        float64
	Name         string
	CurrentPrice float64
}

type GetOrderItemsByOrderRequest struct {
	OrderID uint `validate:"required,gt=0"`
	Offset  uint `validate:"required,gte=0"`
	Limit   uint `validate:"required,gte=0"`
}

type GetOrderItemsByOrderResponse struct {
	Items []GetOrderItemResponse
}

type UpdateOrderItemRequest struct {
	ID       uint    `validate:"required,gt=0"`
	Quantity uint    `validate:"required,gt=0"`
	Price    float64 `validate:"required,gte=0"`
}

type DeleteOrderItemRequest struct {
	ID uint `validate:"required,gt=0"`
}
