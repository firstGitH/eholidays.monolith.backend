package dtos

import (
	_ "github.com/go-playground/validator/v10"
)

type GetRatingsRequest struct {
	Limit  uint `validate:"required,gte=0"`
	Offset uint `validate:"required,gte=0"`
}

type GetRatingsResponse struct {
	Ratings []RatingResponse
}

type RatingResponse struct {
	ID    uint
	Value uint
}
