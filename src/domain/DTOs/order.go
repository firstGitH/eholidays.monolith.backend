package dtos

import (
	"time"

	_ "github.com/go-playground/validator/v10"
)

// Payment method
const (
	PaymentMethodCash = "cash"
	PaymentMethodCard = "card"
)

// Order status
const (
	OrderStatusCreated   = "created"
	OrderStatusDrafted   = "drafted"
	OrderStatusEmpty     = "empty"
	OrderStatusCancelled = "cancelled"
	OrderStatusConfirmed = "confirmed"
	OrderStatusPaid      = "paid"
	OrderStatusReturned  = "returned"
)

type CreateOrderRequest struct {
	UserID           uint   `validate:"required,gt=0"`
	PaymentMethod    string `validate:"required,oneof=cash card"`
	DeliveryLocation string `validate:"required"`
}

type CreateOrderResponse struct {
	ID uint
}

type GetOrdersRequest struct {
	Offset uint `validate:"required,gte=0"`
	Limit  uint `validate:"required,get=0"`
}

type GetOrdersResponse struct {
	Orders []OrderReponse
}

type OrderReponse struct {
	ID               uint
	UserID           uint
	CreatedDate      time.Time
	TotalCost        float64
	Status           string
	PaymentMethod    string
	DeliveryLocation string
	UpdatedDate      time.Time
}

type UpdateOrderStatusRequest struct {
	ID     uint   `validate:"required,gt=0"`
	Status string `validate:"required,oneof=created drafted empty cancelled confirmed paid returned"`
}

type DeleteOrderRequest struct {
	ID uint `validate:"required,gt=0"`
}

type CancelOrderRequest struct {
	ID uint `validate:"required,gt=0"`
}

type ReturnOrderRequest struct {
	ID uint `validate:"required,gt=0"`
}

type ConfirmOrderRequest struct {
	ID               uint   `validate:"required,gt=0"`
	PaymentMethod    string `validate:"required,oneof=cash card"`
	DeliveryLocation string `validate:"required"`
}

type GetOrdersByUserRequest struct {
	UserID uint `validate:"required,gt=0"`
	Offset uint `validate:"required,gte=0"`
	Limit  uint `validate:"required,gte=0"`
}

type GetTotalOrdersResponse struct {
	Total uint
}

type GetTotalOrdersByUserRequest struct {
	UserID uint `validate:"required,gt=0"`
}

type GetTotalOrdersByUserResponse struct {
	Total uint
}
