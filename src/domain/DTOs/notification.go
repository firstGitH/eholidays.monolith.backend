package dtos

import "time"

type CreateNotificationRequest struct {
	ProductID uint   `validate:"required,gt=0"`
	Type      string `validate:"required,oneof=created increased decreased updated deleted"`
	Title     string `validate:"required,max=35"`
	Content   string `validate:"required,max=250"`
}

type CreateNotificationResponse struct {
	ID uint
}

type GetNotificationsRequest struct {
	Limit  uint `validate:"required,gte=0"`
	Offset uint `validate:"required,gte=0"`
}

type GetNotificationsResponse struct {
	Notifications []NotificationResponse
}

type GetTotalNotificationsResponse struct {
	Total uint
}

type GetNotificationsByProductRequest struct {
	ProductID uint `validate:"required,gt=0"`
	Limit     uint `validate:"required,gte=0"`
	Offset    uint `validate:"required,gte=0"`
}

type GetNotificationsByProductResponse struct {
	Notifications []NotificationResponse
}

type GetTotalNotificationsByProductRequest struct {
	ProductID uint `validate:"required,gt=0"`
}

type GetTotalNotificationsByProductResponse struct {
	Total uint
}

type UpdateNotificationRequest struct {
	ID      uint   `validate:"required,gt=0"`
	Title   string `validate:"max=35"`
	Content string `validate:"max=250"`
}

type DeleteNotificationRequest struct {
	ID uint `validate:"required,gt=0"`
}

type NotificationResponse struct {
	ID        uint
	ProductID uint
	Type      string
	Date      time.Time
	Title     string
	Content   string
}
