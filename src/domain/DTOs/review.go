package dtos

import (
	"time"

	_ "github.com/go-playground/validator/v10"
)

type CreateReviewRequest struct {
	UserID    uint      `validate:"required,gt=0"`
	ProductID uint      `validate:"required,gt=0"`
	RatingID  uint      `validate:"required,gt=0"`
	Date      time.Time `validate:"omitempty"`
	Content   string    `validate:"required,min=2,max=250"`
}

type CreateReviewResponse struct {
	ID uint
}

type GetReviewsByProductRequest struct {
	ProductID uint `validate:"required,gt=0"`
	Limit     uint `validate:"required"`
	Offset    uint `validate:"required"`
}

type GetReviewsByProductResponse struct {
	Reviews []ReviewResponse
}

type GetTotalReviewsByProductRequest struct {
	ProductID uint `validate:"required,gt=0"`
}

type GetTotalReviewsByProductResponse struct {
	Total uint
}

type UpdateReviewRequest struct {
	ID       uint   `validate:"required,gt=0"`
	RatingID uint   `validate:"required,gt=0"`
	Content  string `validate:"required,min=2,max=250"`
}

type DeleteReviewRequest struct {
	ID uint `validate:"required,gt=0"`
}

type ReviewResponse struct {
	ID        uint
	UserID    uint
	ProductID uint
	RatingID  uint
	Username  string
	Rating    uint
	Date      time.Time
	Content   string
}
