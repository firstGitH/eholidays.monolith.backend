package dtos

import (
	"time"

	_ "github.com/go-playground/validator/v10"
)

type CreateSubscriptionRequest struct {
	UserID    uint `validate:"required,gt=0"`
	ProductID uint `validate:"required,gt=0"`
}

type CreateSubscriptionResponse struct {
	ID uint
}

type GetSubscriptionsByUserRequest struct {
	UserID uint `validate:"required,gt=0"`
	Offset uint `validate:"omitempty"`
	Limit  uint `validate:"omitempty"`
}

type GetSubscriptionsByUserResponse struct {
	Subscriptions []SubscriptionResponse
}

type GetTotalSubscriptionsByUserRequest struct {
	UserID uint `validate:"required,gt=0"`
}

type GetTotalSubscriptionsByUserResponse struct {
	Total uint
}

type GetSubscriptionsRequest struct {
	Offset uint `validate:"omitempty"`
	Limit  uint `validate:"omitempty"`
}

type GetSubscriptionsResponse struct {
	Subscriptions []SubscriptionResponse
}

type GetTotalSubscriptionsResponse struct {
	Total uint
}

type DeleteSubscriptionRequest struct {
	UserID    uint `validate:"required,gt=0"`
	ProductID uint `validate:"required,gt=0"`
}

type SubscriptionResponse struct {
	ID          uint
	UserID      uint
	ProductID   uint
	ProductName string
	CreatedDate time.Time
}

type HasSubscriptionRequest struct {
	UserID    uint `validate:"required,gt=0"`
	ProductID uint `validate:"required,gt=0"`
}

type HasSubscriptionResponse struct {
	Has bool
}
