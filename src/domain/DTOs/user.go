package dtos

import (
	_ "github.com/go-playground/validator/v10"
)

type CreateUserRequest struct {
	Login    string `validate:"required,min=5,max=25"`
	Password []byte `validate:"required"`
	Username string `validate:"required,min=1,max=25"`
	Fullname string `validate:"omitempty,max=50"`
	Gender   string `validate:"omitempty,oneof=m w"`
	Age      uint   `validate:"omitempty,gte=0,lte=120"`
	Phone    string `validate:"omitempty,max=40"`
	AboutMe  string `validate:"omitempty,max=250"`
}

type CreateUserResponse struct {
	UserID uint
}

type ContainsUserRequest struct {
	Login    string `validate:"required,min=1,max=25"`
	Password []byte `validate:"required"`
}

type ContainsUserResponse struct {
	UserID uint
}

type UserHasRoleByIDRequest struct {
	UserID uint `validate:"required,gt=0"`
	RoleID uint `validate:"required,gt=0"`
}

type UserHasRoleByIDResponse struct {
	Exists bool
}

type UserHasRoleByNameRequest struct {
	UserID   uint   `validate:"required,gt=0"`
	RoleName string `validate:"required,min=1,max=25"`
}

type UserHasRoleByNameResponse struct {
	Exists bool
}

type GetSelfInfoRequest struct {
	UserID uint `validate:"required,gt=0"`
}

type GetSelfInfoResponse struct {
	User UserSelfInfoResponse
}

type GetUserInfoRequest struct {
	UserID uint `validate:"required,gt=0"`
}

type GetUserInfoResponse struct {
	User UserInfoResponse
}

type GetUsersRequest struct {
	Offset uint `validate:"required"`
	Limit  uint `validate:"required"`
}

type GetUsersResponse struct {
	Users []UserInfoResponse
}

type GetTotalUsersResponse struct {
	Total uint
}

type UpdateUserRequest struct {
	ID       uint   `validate:"required,gt=0"`
	Fullname string `validate:"max=50"`
	Gender   string `validate:"omitempty,oneof=m w"`
	Age      uint   `validate:"gte=0,lte=120"`
	Phone    string `validate:"max=40"`
	AboutMe  string `validate:"max=250"`
}

type DeleteUserRequest struct {
	UserID uint `validate:"required,gt=0"`
}

type UpdateLoginRequest struct {
	UserID   uint   `validate:"required,gt=0"`
	NewLogin string `validate:"required,min=1,max=25"`
	Password []byte `validate:"required"`
}

type UpdatePasswordRequest struct {
	UserID      uint   `validate:"required,gt=0"`
	OldPassword []byte `validate:"required"`
	NewPassword []byte `validate:"required"`
}

type UpdateUsernameRequest struct {
	UserID   uint   `validate:"required,gt=0"`
	Username string `validate:"required,min=1,max=25"`
	Password []byte `validate:"required"`
}

type UserSelfInfoResponse struct {
	ID       uint
	Login    string
	Username string
	Fullname string
	Gender   string
	Age      uint
	Phone    string
	AboutMe  string
}

type UserInfoResponse struct {
	ID       uint
	Username string
	Fullname string
	Gender   string
	Age      uint
	Phone    string
	AboutMe  string
}
