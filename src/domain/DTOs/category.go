package dtos

import (
	_ "github.com/go-playground/validator/v10"
)

type CreateCategoryRequest struct {
	Name        string `validate:"required,min=3,max=35"`
	Description string `validate:"min=2,max=100"`
}

type CreateCategoryResponse struct {
	ID uint
}

type GetCategoryRequest struct {
	ID uint `validate:"required,gt=0"`
}

type GetCategoryResponse struct {
	ID          uint
	Name        string
	Description string
}

type GetCategoriesRequest struct {
	Limit  uint `validate:"required,gte=0"`
	Offset uint `validate:"required,gte=0"`
}

type GetCategoriesResponse struct {
	Categories []CategoryResponse
}

type CategoryResponse struct {
	ID          uint
	Name        string
	Description string
}

type GetTotalCategoriesResponse struct {
	Total uint
}

type UpdateCategoryRequest struct {
	ID          uint   `validate:"required,gt=0"`
	Name        string `validate:"required,min=3,max=35"`
	Description string `validate:"max=100"`
}

type DeleteCategoryRequest struct {
	ID uint `validate:"required,gt=0"`
}
