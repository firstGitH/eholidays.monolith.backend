package dtos

import (
	_ "github.com/go-playground/validator/v10"
)

type CreateProductItemRequest struct {
	Name  string `validate:"required,min=3,max=35"`
	Value string `validate:"required,min=3,max=100"`
}

type CreateSingleProductItemRequest struct {
	ProductID uint   `validate:"required,gt=0"`
	Name      string `validate:"required,min=3,max=35"`
	Value     string `validate:"required,min=3,max=100"`
}

type CreateProductItemResponse struct {
	ID uint
}

type GetProductItemRequest struct {
	ID uint `validate:"required,gt=0"`
}

type ProductItemResponse struct {
	ID        uint
	ProductID uint
	Name      string
	Value     string
}

type GetProductItemsByProductRequest struct {
	ProductID uint `validate:"required,gt=0"`
	Offset    uint `validate:"omitempty"`
	Limit     uint `validate:"omitempty"`
}

type GetProductItemsByProductResponse struct {
	Items []ProductItemResponse
}

type GetTotalItemsByProductRequest struct {
	ProductID uint `validate:"required,gt=0"`
}

type GetTotalItemsByProductResponse struct {
	Total uint
}

type UpdateProductItemRequest struct {
	ID    uint   `validate:"required,gt=0"`
	Name  string `validate:"omitempty"`
	Value string `validate:"omitempty"`
}

type DeleteProductItemRequest struct {
	ID uint `validate:"required,gt=0"`
}
