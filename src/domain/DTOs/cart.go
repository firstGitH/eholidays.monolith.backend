package dtos

import (
	_ "github.com/go-playground/validator/v10"
)

type CreateCartItemRequest struct {
	UserID    uint `validate:"required,gt=0"`
	ProductID uint `validate:"required,gt=0"`
}
type CreateCartItemResponse struct {
	ID uint
}

type DeleteCartItemRequest struct {
	ID uint `validate:"required,gt=0"`
}

type GetCartItemsRequest struct {
	UserID uint `validate:"required,gt=0"`
	Offset uint `validate:"required,gte=0"`
	Limit  uint `validate:"required,gte=0"`
}

type GetCartItemsResponse struct {
	Items []GetCartItemResponse
}

type GetCartItemResponse struct {
	ID        uint
	UserID    uint
	ProductID uint
	Name      string
	Price     float64
}

type GetTotalCartItemRequest struct {
	UserID uint `validate:"required,gt=0"`
}

type GetTotalCartItemsResponse struct {
	Total uint
}
