package services

import (
	"context"
	"fmt"
	"log/slog"

	errors "eholidays.monolith/src/domain"
	dtos "eholidays.monolith/src/domain/DTOs"
	"eholidays.monolith/src/storage/postgres/repositories"
	"github.com/go-playground/validator/v10"
)

type TmpInterface interface {
}

type OrderService struct {
	log       *slog.Logger
	rep       repositories.OrderRepository
	validator *validator.Validate
}

func NewOrderService(
	log *slog.Logger,
	repOrder repositories.OrderRepository,
	repOrderItem repositories.OrderItemRepository,
) *OrderService {
	return &OrderService{
		log:       log,
		rep:       repOrder,
		validator: validator.New(),
	}
}

func (s *OrderService) Create(ctx context.Context,
	req dtos.CreateOrderRequest) (dtos.CreateOrderResponse, error) {
	const op = "OrderService.Create"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.CreateOrderResponse{}, err
	}

	resp, err := s.rep.Create(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.CreateOrderResponse{}, err
	}

	s.log.Info(fmt.Sprintf("%s: order created successfully", op))
	return resp, nil
}

func (s *OrderService) GetItems(ctx context.Context, req dtos.GetOrdersRequest) (
	dtos.GetOrdersResponse, error) {
	const op = "OrderService.GetItems"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetOrdersResponse{}, err
	}

	resp, err := s.rep.GetItems(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetOrdersResponse{}, err
	}

	s.log.Info(fmt.Sprintf("%s: items retrieved successfully", op))
	return resp, nil
}

func (s *OrderService) GetTotalItems(ctx context.Context) (dtos.GetTotalOrdersResponse, error) {
	const op = "OrderService.GetTotalItems"
	s.log.Info(fmt.Sprintf("%s: start", op))

	resp, err := s.rep.GetTotalItems(ctx)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetTotalOrdersResponse{}, err
	}

	s.log.Info(fmt.Sprintf("%s: items retrieved successfully", op))
	return resp, nil
}

func (s *OrderService) GetItemsByUser(ctx context.Context,
	req dtos.GetOrdersByUserRequest) (dtos.GetOrdersResponse, error) {
	const op = "OrderService.GetItemsByUser"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetOrdersResponse{}, err
	}

	resp, err := s.rep.GetItemsByUser(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetOrdersResponse{}, err
	}

	s.log.Info(fmt.Sprintf("%s: items retrieved successfully", op))
	return resp, nil
}

func (s *OrderService) GetTotalItemsByUser(ctx context.Context,
	req dtos.GetTotalOrdersByUserRequest) (dtos.GetTotalOrdersByUserResponse, error) {
	const op = "OrderService.GetTotalItemsByUser"

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetTotalOrdersByUserResponse{}, err
	}

	resp, err := s.rep.GetTotalItemsByUser(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetTotalOrdersByUserResponse{}, err
	}

	s.log.Info(fmt.Sprintf("%s: items retrieved successfully", op))
	return resp, nil
}

func (s *OrderService) Delete(ctx context.Context, req dtos.DeleteOrderRequest) error {
	const op = "OrderService.Delete"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return err
	}

	err = s.rep.Delete(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return err
	}

	s.log.Info(fmt.Sprintf("%s: order deleted successfully", op))
	return nil
}

func (s *OrderService) Confirm(ctx context.Context, req dtos.ConfirmOrderRequest) error {
	const op = "OrderService.Confirm"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return err
	}

	err = s.rep.Confirm(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return err
	}

	s.log.Info(fmt.Sprintf("%s: order confirmed successfully", op))
	return nil
}

func (s *OrderService) Cancel(ctx context.Context, req dtos.CancelOrderRequest) error {
	const op = "OrderService.Cancel"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return err
	}

	err = s.rep.Cancel(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return err
	}

	s.log.Info(fmt.Sprintf("%s: order cancelled successfully", op))
	return nil
}

func (s *OrderService) Return(ctx context.Context, req dtos.ReturnOrderRequest) error {
	const op = "OrderService.Return"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return err
	}

	err = s.rep.Return(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return err
	}

	s.log.Info(fmt.Sprintf("%s: order returned successfully", op))
	return nil
}
