package services

import (
	"context"
	"io"
	"os"
	"path/filepath"
	"syscall"

	dtos "eholidays.monolith/src/domain/DTOs"
	"eholidays.monolith/src/storage"
)

type ImageService interface {
	Contains(ctx context.Context, imageName string) bool
	Save(ctx context.Context, image *dtos.ProductImage) error
	Get(ctx context.Context, image *dtos.ProductImage) error
	Update(ctx context.Context, image *dtos.ProductImage) error
	Remove(ctx context.Context, imageName string) error
}

type ImageFileService struct {
	storagePath string
}

func NewImageService(storagePath string) *ImageFileService {
	return &ImageFileService{storagePath: storagePath}
}

func (s *ImageFileService) Contains(ctx context.Context, imageName string) bool {
	filePath := filepath.Join(s.storagePath, imageName)
	_, err := os.Stat(filePath)

	return os.IsExist(err)
}

func (s *ImageFileService) Save(ctx context.Context,
	image *dtos.ProductImage) error {
	filePath := filepath.Join(s.storagePath, image.Name)
	file, err := os.Create(filePath)
	if err != nil {
		return checkFileError(err)
	}
	defer file.Close()

	_, err = io.Copy(file, image.Data)
	if err != nil {
		return checkFileError(err)
	}

	return nil
}

func (s *ImageFileService) Get(ctx context.Context,
	image *dtos.ProductImage) error {
	filePath := filepath.Join(s.storagePath, image.Name)
	file, err := os.Open(filePath)
	if err != nil {
		return checkFileError(err)
	}

	image.Data = file

	return nil
}

func (s *ImageFileService) Update(ctx context.Context, image *dtos.ProductImage) error {
	filePath := filepath.Join(s.storagePath, image.Name)
	file, err := os.Create(filePath)
	if err != nil {
		return checkFileError(err)
	}
	defer file.Close()

	_, err = io.Copy(file, image.Data)
	if err != nil {
		return checkFileError(err)
	}

	return nil
}

func (s *ImageFileService) Remove(ctx context.Context, imageName string) error {
	filePath := filepath.Join(s.storagePath, imageName)
	err := os.Remove(filePath)
	if err != nil {
		return checkFileError(err)
	}

	return nil
}

func checkFileError(err error) error {
	if os.IsExist(err) {
		return storage.ErrFileExists
	}
	if os.IsNotExist(err) {
		return storage.ErrFileDoesNotExist
	}
	if os.IsPermission(err) {
		return storage.ErrNoPermission
	}
	if pathError, ok := err.(*os.PathError); ok {
		if pathError.Err == syscall.ENOSPC {
			return storage.ErrNoSpace
		}
		if pathError.Err == syscall.EBUSY {
			return storage.ErrFileBusy
		}
	}
	return err
}
