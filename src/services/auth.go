package services

import (
	"context"
	"fmt"
	"log/slog"

	errors "eholidays.monolith/src/domain"
	dtos "eholidays.monolith/src/domain/DTOs"
	"eholidays.monolith/src/lib/hashers"
	"eholidays.monolith/src/storage/postgres/repositories"
	"github.com/go-playground/validator/v10"
)

type AuthService struct {
	log       *slog.Logger
	rep       repositories.UserRepository
	hasher    hashers.PasswordHasher
	validator *validator.Validate
}

func NewAuthService(
	log *slog.Logger,
	rep repositories.UserRepository,
) *AuthService {
	return &AuthService{
		log:       log,
		rep:       rep,
		validator: validator.New(),
	}
}

func (s *AuthService) SignUp(ctx context.Context,
	req dtos.CreateUserRequest) (dtos.CreateUserResponse, error) {
	const op = "AuthService.SignUp"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.CreateUserResponse{}, err
	}

	req.Password, err = s.hasher.HashPassword(req.Password)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.CreateUserResponse{}, err
	}

	contains, err := s.rep.Contains(ctx, dtos.ContainsUserRequest{
		Login:    req.Login,
		Password: req.Password,
	})
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.CreateUserResponse{}, err
	}
	if contains.UserID != 0 {
		s.log.Info(fmt.Sprintf("%s: user already exists", op))
		return dtos.CreateUserResponse{}, errors.ErrUserAlreadyExists
	}

	resp, err := s.rep.Create(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.CreateUserResponse{}, err
	}

	s.log.Info(fmt.Sprintf("%s: user created successfully", op))
	return resp, nil
}

func (s *AuthService) SignIn(ctx context.Context,
	req dtos.ContainsUserRequest) (dtos.ContainsUserResponse, error) {
	const op = "AuthService.SignIn"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.ContainsUserResponse{}, err
	}

	req.Password, err = s.hasher.HashPassword(req.Password)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.ContainsUserResponse{}, err
	}

	contains, err := s.rep.Contains(ctx, dtos.ContainsUserRequest{
		Login:    req.Login,
		Password: req.Password,
	})
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.ContainsUserResponse{}, err
	}

	if contains.UserID == 0 {
		s.log.Info(fmt.Sprintf("%s: user not found", op))
		return dtos.ContainsUserResponse{}, errors.ErrUserNotFound
	}

	resp, err := s.rep.Contains(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op,
			fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)))
		return dtos.ContainsUserResponse{}, err
	}

	s.log.Info(fmt.Sprintf("%s: user found successfully", op))
	return resp, nil
}
