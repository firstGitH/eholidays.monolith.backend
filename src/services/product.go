package services

import (
	"context"
	"errors"
	"fmt"
	"log/slog"
	"sync"
	"time"

	projerrors "eholidays.monolith/src/domain"
	dtos "eholidays.monolith/src/domain/DTOs"
	"eholidays.monolith/src/storage"
	"eholidays.monolith/src/storage/postgres/repositories"
	"github.com/go-playground/validator/v10"
	"github.com/google/uuid"
)

type ProductService struct {
	log          *slog.Logger
	imageService ImageService
	repProduct   repositories.ProductRepository
	repRating    repositories.RatingRepository
	validator    *validator.Validate
}

func NewProductService(
	log *slog.Logger,
	imageService ImageService,
	repProduct repositories.ProductRepository,
	repRating repositories.RatingRepository,
) *ProductService {
	return &ProductService{
		log:          log,
		imageService: imageService,
		repProduct:   repProduct,
		validator:    validator.New(),
	}
}

func (s *ProductService) Create(ctx context.Context,
	req dtos.CreateProductRequest) (dtos.CreateProductResponse, error) {
	const op = "ProductService.CreateProduct"
	s.log.Info(fmt.Sprintf("%s: start", op))

	for {
		imageName := uuid.New().String()
		if !s.imageService.Contains(ctx, imageName) {
			req.Image.Name = imageName
			break
		}
	}

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", projerrors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.CreateProductResponse{}, err
	}

	var resp dtos.CreateProductResponse
	if len(req.Items) > 0 {
		resp, err = s.repProduct.CreateWithItems(ctx, req)
		if err != nil {
			s.log.Error(fmt.Sprintf("%s: %v", op, err))
			return dtos.CreateProductResponse{}, err
		}
	} else {
		resp, err = s.repProduct.Create(ctx, req)
		if err != nil {
			s.log.Error(fmt.Sprintf("%s: %v", op, err))
			return dtos.CreateProductResponse{}, err
		}
	}

	err = s.imageService.Save(ctx, &req.Image)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return resp, err
	}

	s.log.Info(fmt.Sprintf("%s: product created successfully", op))

	return resp, nil
}

func (s *ProductService) GetFullInfo(ctx context.Context,
	req dtos.GetProductFullInfoRequest) (
	dtos.GetProductFullInfoResponse, error) {
	const op = "ProductService.GetFullInfo"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", projerrors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetProductFullInfoResponse{}, err
	}

	resp, err := s.repProduct.GetFullInfo(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetProductFullInfoResponse{}, err
	}

	err = s.imageService.Get(ctx, &resp.Image)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return resp, storage.ErrImageGetFailed
	}

	s.log.Info(fmt.Sprintf("%s: got full product info successfully", op))

	return resp, nil
}

func (s *ProductService) GetItemsByCategory(ctx context.Context,
	req dtos.GetProductsByCategoryRequest) (
	dtos.GetProductsByCategoryResponse, error) {
	const op = "ProductService.GetItemsByCategory"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", projerrors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetProductsByCategoryResponse{}, err
	}

	resp, err := s.repProduct.GetItemsByCategory(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetProductsByCategoryResponse{}, err
	}

	var wg sync.WaitGroup
	for _, v := range resp.Products {
		wg.Add(1)
		go func(v *dtos.ProductResponse) {
			defer wg.Done()
			if err = s.imageService.Get(ctx, &v.Image); err != nil {
				if errors.Is(err, storage.ErrFileBusy) {
					time.Sleep(time.Millisecond * 120)
					err = s.imageService.Get(ctx, &v.Image)
					if err != nil {
						s.log.Error(fmt.Sprintf("%s: retry %v", op, err))

						return
					}
				}
				s.log.Error(fmt.Sprintf("%s: %v", op, err))
			}
		}(&v)
	}
	wg.Wait()

	s.log.Info(fmt.Sprintf("%s: got items by category successfully", op))
	return resp, nil
}

func (s *ProductService) GetTotalItemsByCategory(ctx context.Context,
	req dtos.GetTotalProductsByCategoryRequest) (
	dtos.GetTotalProductsByCategoryResponse, error) {
	const op = "ProductService.GetTotalItemsByCategory"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", projerrors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetTotalProductsByCategoryResponse{}, err
	}

	resp, err := s.repProduct.GetTotalItemsByCategory(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetTotalProductsByCategoryResponse{}, err
	}

	s.log.Info(fmt.Sprintf("%s: got total items by category successfully", op))
	return resp, nil
}

func (s *ProductService) GetItems(ctx context.Context,
	req dtos.GetProductsRequest) (dtos.GetProductsResponse, error) {
	const op = "ProductService.GetItems"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", projerrors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetProductsResponse{}, err
	}

	resp, err := s.repProduct.GetItems(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetProductsResponse{}, err
	}

	var wg sync.WaitGroup
	for _, v := range resp.Products {
		wg.Add(1)
		go func(v *dtos.ProductResponse) {
			defer wg.Done()
			if err = s.imageService.Get(ctx, &v.Image); err != nil {
				if errors.Is(err, storage.ErrFileBusy) {
					time.Sleep(time.Millisecond * 120)
					err = s.imageService.Get(ctx, &v.Image)
					if err != nil {
						s.log.Error(fmt.Sprintf("%s: retry %v", op, err))

						return
					}
				}
				s.log.Error(fmt.Sprintf("%s: %v", op, err))
			}
		}(&v)
	}
	wg.Wait()

	s.log.Info(fmt.Sprintf("%s: got items successfully", op))
	return resp, nil
}

func (s *ProductService) GetTotalItems(ctx context.Context) (
	dtos.GetTotalProductsResponse, error) {
	const op = "ProductService.GetTotalItems"
	s.log.Info(fmt.Sprintf("%s: start", op))

	resp, err := s.repProduct.GetTotalItems(ctx)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetTotalProductsResponse{}, err
	}

	s.log.Info(fmt.Sprintf("%s: got total items successfully", op))
	return resp, nil
}

func (s *ProductService) GetItemsByUser(ctx context.Context,
	req dtos.GetProductsByUserRequest) (dtos.GetProductsByUserResponse, error) {
	const op = "ProductService.GetItemsByUser"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", projerrors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetProductsByUserResponse{}, err
	}

	resp, err := s.repProduct.GetItemsByUser(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetProductsByUserResponse{}, err
	}

	var wg sync.WaitGroup
	for _, v := range resp.Products {
		wg.Add(1)
		go func(v *dtos.ProductResponse) {
			defer wg.Done()
			if err = s.imageService.Get(ctx, &v.Image); err != nil {
				if errors.Is(err, storage.ErrFileBusy) {
					time.Sleep(time.Millisecond * 120)
					err = s.imageService.Get(ctx, &v.Image)
					if err != nil {
						s.log.Error(fmt.Sprintf("%s: retry %v", op, err))

						return
					}
				}
				s.log.Error(fmt.Sprintf("%s: %v", op, err))
			}
		}(&v)
	}
	wg.Wait()

	s.log.Info(fmt.Sprintf("%s: got items by user successfully", op))
	return resp, nil
}

func (s *ProductService) GetTotalItemsByUser(ctx context.Context,
	req dtos.GetTotalProductsByUserRequest) (
	dtos.GetTotalProductsByUserResponse, error) {
	const op = "ProductService.GetTotalItemsByUser"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", projerrors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetTotalProductsByUserResponse{}, err
	}

	resp, err := s.repProduct.GetTotalItemsByUser(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetTotalProductsByUserResponse{}, err
	}

	s.log.Info(fmt.Sprintf("%s: got total items by user successfully", op))
	return resp, nil
}

func (s *ProductService) Delete(ctx context.Context,
	req dtos.DeleteProductRequest) error {
	const op = "ProductService.DeleteProduct"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", projerrors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return err
	}

	err = s.repProduct.Delete(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return err
	}

	err = s.imageService.Remove(ctx, req.ImageName)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		if errors.Is(err, storage.ErrFileBusy) {
			go func() {
				retries := 0
				for {
					if retries < 3 {
						time.Sleep(time.Second * 5)
						err = s.imageService.Remove(ctx, req.ImageName)
						if err != nil {
							s.log.Error(fmt.Sprintf("%s: retry %v", op, err))
							if errors.Is(err, storage.ErrFileDoesNotExist) {
								break
							}
						}
						retries++
					} else {
						break
					}
				}
			}()
		} else if errors.Is(err, storage.ErrFileDoesNotExist) {
			s.log.Info("%s: %v", storage.ErrFileDoesNotExist)
			return nil
		}

		return err
	}

	s.log.Info(fmt.Sprintf("%s: product deleted successfully", op))
	return nil
}

func (s *ProductService) GetRatings(ctx context.Context,
	req dtos.GetRatingsRequest) (dtos.GetRatingsResponse, error) {
	const op = "ProductService.GetRatings"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", projerrors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetRatingsResponse{}, err
	}

	resp, err := s.repRating.GetItems(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetRatingsResponse{}, err
	}

	s.log.Info(fmt.Sprintf("%s: product ratings retrieved successfully", op))
	return resp, nil
}
