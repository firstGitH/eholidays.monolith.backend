package services

import (
	"context"
	"fmt"
	"log/slog"

	errors "eholidays.monolith/src/domain"
	dtos "eholidays.monolith/src/domain/DTOs"
	"eholidays.monolith/src/storage/postgres/repositories"
	"github.com/go-playground/validator/v10"
)

type ProductItemService struct {
	log *slog.Logger
	rep repositories.ProductItemRepository
	val *validator.Validate
}

func NewProductItemService(
	log *slog.Logger,
	rep repositories.ProductItemRepository,
) *ProductItemService {
	return &ProductItemService{
		log: log,
		rep: rep,
		val: validator.New(),
	}
}

func (s *ProductItemService) Create(ctx context.Context,
	req dtos.CreateSingleProductItemRequest) (
	dtos.CreateProductItemResponse, error) {

	const op = "ProductItemService.Create"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.val.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.CreateProductItemResponse{}, err
	}

	resp, err := s.rep.Create(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.CreateProductItemResponse{}, err
	}

	s.log.Info(fmt.Sprintf("%s: product item created successfully", op))
	return resp, nil
}

func (s *ProductItemService) Get(ctx context.Context,
	req dtos.GetProductItemRequest) (dtos.ProductItemResponse, error) {
	const op = "ProductItemService.Get"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.val.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.ProductItemResponse{}, err
	}

	resp, err := s.rep.Get(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.ProductItemResponse{}, err
	}

	s.log.Info(fmt.Sprintf("%s: product item retrieved successfully", op))
	return resp, nil
}

func (s *ProductItemService) GetItems(ctx context.Context,
	req dtos.GetProductItemsByProductRequest) (
	dtos.GetProductItemsByProductResponse, error) {
	const op = "ProductItemService.GetItems"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.val.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetProductItemsByProductResponse{}, err
	}

	resp, err := s.rep.GetItems(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetProductItemsByProductResponse{}, err
	}

	s.log.Info(fmt.Sprintf("%s: product items retrieved successfully", op))
	return resp, nil
}

func (s *ProductItemService) GetTotalItems(ctx context.Context,
	req dtos.GetTotalItemsByProductRequest) (
	dtos.GetTotalItemsByProductResponse, error) {
	const op = "ProductItemService.GetTotalItems"
	s.log.Info(fmt.Sprintf("%s: start", op))
	err := s.val.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetTotalItemsByProductResponse{}, err
	}

	resp, err := s.rep.GetTotalItems(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetTotalItemsByProductResponse{}, err
	}

	s.log.Info(fmt.Sprintf("%s: items retrieved successfully", op))
	return resp, nil
}

func (s *ProductItemService) Update(ctx context.Context,
	req dtos.UpdateProductItemRequest) error {
	const op = "ProducItemService.Update"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.val.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return err
	}

	err = s.rep.Update(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return err
	}

	s.log.Info(fmt.Sprintf("%s: product item updated successfully", op))
	return nil
}

func (s *ProductItemService) Delete(ctx context.Context,
	req dtos.DeleteProductItemRequest) error {
	const op = "ProductItemService.Delete"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.val.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return err
	}

	err = s.rep.Delete(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return err
	}

	s.log.Info(fmt.Sprintf("%s: product item deleted successfully", op))
	return nil
}
