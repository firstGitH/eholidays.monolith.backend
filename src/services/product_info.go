package services

import (
	"context"
	"fmt"
	"log/slog"

	errors "eholidays.monolith/src/domain"
	dtos "eholidays.monolith/src/domain/DTOs"
	"eholidays.monolith/src/storage"
	"eholidays.monolith/src/storage/postgres/repositories"
	"github.com/go-playground/validator/v10"
	"github.com/google/uuid"
)

type ProductInfoService struct {
	log          *slog.Logger
	imageService ImageService
	repProduct   repositories.ProductUpdateRepository
	validator    *validator.Validate
}

func NewProductInfoService(
	log *slog.Logger,
	imageService ImageService,
	repRating repositories.RatingRepository,
	repProduct repositories.ProductUpdateRepository,
) *ProductInfoService {
	return &ProductInfoService{
		log:          log,
		imageService: imageService,
		repProduct:   repProduct,
		validator:    validator.New(),
	}
}

func (s *ProductInfoService) UpdateDescription(ctx context.Context,
	req dtos.UpdateProductDescriptionRequest) error {
	const op = "ProductInfoService.UpdateDescription"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return err
	}

	err = s.repProduct.UpdateDescription(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return err
	}

	s.log.Info(fmt.Sprintf("%s: product description updated successfully", op))
	return nil
}

func (s *ProductInfoService) UpdatePrice(ctx context.Context,
	req dtos.UpdateProductPriceRequest) error {
	const op = "ProductInfoService.UpdatePrice"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return err
	}

	err = s.repProduct.UpdatePrice(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return err
	}

	s.log.Info(fmt.Sprintf("%s: product price updated successfully", op))
	return nil
}

func (s *ProductInfoService) UpdateCategory(ctx context.Context,
	req dtos.UpdateProductCategoryRequest) error {
	const op = "ProductInfoService.UpdateCategory"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return err
	}

	err = s.repProduct.UpdateCategory(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return err
	}

	s.log.Info(fmt.Sprintf("%s: product category updated successfully", op))

	return nil
}

func (s *ProductInfoService) UpdateName(ctx context.Context,
	req dtos.UpdateProductNameRequest) error {
	const op = "ProductInfoService.UpdateName"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return err
	}

	err = s.repProduct.UpdateName(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return err
	}

	s.log.Info(fmt.Sprintf("%s: product name updated successfully", op))

	return nil
}

func (s *ProductInfoService) IncreaseQuantity(ctx context.Context,
	req dtos.IncreaseProductQuantityRequest) (dtos.IncreaseProductQuantityResponse, error) {
	const op = "ProductInfoService.IncreaseQuantity"
	s.log.Info(fmt.Sprintf("%s: start", op))

	if req.Quantity == 0 {
		s.log.Info(fmt.Sprintf("%s: no quantity to decrease", op))
		return dtos.IncreaseProductQuantityResponse{}, fmt.Errorf("%w: %v",
			errors.ErrProductQuantityIncreaseFailed, errors.ErrProductQuantityZero)
	}

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.IncreaseProductQuantityResponse{}, err
	}

	resp, err := s.repProduct.IncreaseQuantity(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.IncreaseProductQuantityResponse{}, err
	}

	s.log.Info(fmt.Sprintf("%s: product quantity increased successfully", op))

	return resp, nil
}

func (s *ProductInfoService) DecreaseQuantity(ctx context.Context,
	req dtos.DecreaseProductQuantityRequest) (dtos.DecreaseProductQuantityResponse, error) {
	const op = "ProductInfoService.DecreaseQuantity"
	s.log.Info(fmt.Sprintf("%s: start", op))

	if req.Quantity == 0 {
		s.log.Info(fmt.Sprintf("%s: no quantity to decrease", op))
		return dtos.DecreaseProductQuantityResponse{}, fmt.Errorf("%w: %v",
			errors.ErrProductQuantityDecreaseFailed, errors.ErrProductQuantityZero)
	}

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.DecreaseProductQuantityResponse{}, err
	}

	resp, err := s.repProduct.DecreaseQuantity(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.DecreaseProductQuantityResponse{}, err
	}

	s.log.Info(fmt.Sprintf("%s: product quantity decreased successfully", op))

	return resp, nil
}

func (s *ProductInfoService) UpdateImage(ctx context.Context,
	req dtos.UpdateProductImageRequest) error {
	const op = "ProductInfoService.UpdateImage"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return err
	}

	if s.imageService.Contains(ctx, req.OldName) {
		err = s.imageService.Remove(ctx, req.OldName)
		if err != nil {
			s.log.Error(fmt.Sprintf("%s: %v", op, err))
			return err
		}
	}

	for {
		imageName := uuid.New().String()
		if !s.imageService.Contains(ctx, imageName) {
			req.NewImage.Name = imageName
			break
		}
	}

	err = s.repProduct.UpdateImage(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return err
	}

	err = s.imageService.Save(ctx, &req.NewImage)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return storage.ErrImageUpdateFailed
	}

	s.log.Info(fmt.Sprintf("%s: product image updated successfully", op))

	return nil
}
