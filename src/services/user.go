package services

import (
	"context"
	"fmt"
	"log/slog"

	errors "eholidays.monolith/src/domain"
	dtos "eholidays.monolith/src/domain/DTOs"
	"eholidays.monolith/src/storage/postgres/repositories"
	"github.com/go-playground/validator/v10"
)

type UserService struct {
	log       *slog.Logger
	rep       repositories.UserRepository
	validator *validator.Validate
}

func NewUserService(
	log *slog.Logger,
	rep repositories.UserRepository,
) *UserService {
	return &UserService{
		log:       log,
		rep:       rep,
		validator: validator.New(),
	}
}

func (s *UserService) GetSelfInfo(ctx context.Context,
	req dtos.GetSelfInfoRequest) (dtos.GetSelfInfoResponse, error) {
	const op = "UserService.GetSelfInfo"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetSelfInfoResponse{}, err
	}

	resp, err := s.rep.GetSelfInfo(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetSelfInfoResponse{}, err
	}

	s.log.Info(fmt.Sprintf("%s: user retrieved successfully", op))
	return resp, nil
}

func (s *UserService) GetUserInfo(ctx context.Context,
	req dtos.GetUserInfoRequest) (dtos.GetUserInfoResponse, error) {
	const op = "UserService.GetUserInfo"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetUserInfoResponse{}, err
	}

	resp, err := s.rep.GetUserInfo(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetUserInfoResponse{}, err
	}

	s.log.Info(fmt.Sprintf("%s: user retrieved successfully", op))
	return resp, nil
}

func (s *UserService) GetUsers(ctx context.Context,
	req dtos.GetUsersRequest) (dtos.GetUsersResponse, error) {
	const op = "UserService.GetUsers"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetUsersResponse{}, err
	}

	resp, err := s.rep.GetUsers(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetUsersResponse{}, err
	}

	s.log.Info(fmt.Sprintf("%s: users retrieved successfully", op))
	return resp, nil
}

func (s *UserService) GetTotalUsers(ctx context.Context) (
	dtos.GetTotalUsersResponse, error) {
	const op = "UserService.GetTotalUsers"
	s.log.Info(fmt.Sprintf("%s: start", op))

	resp, err := s.rep.GetTotalUsers(ctx)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetTotalUsersResponse{}, err
	}

	s.log.Info(fmt.Sprintf("%s: users retrieved successfully", op))
	return resp, nil
}

func (s *UserService) Update(ctx context.Context,
	req dtos.UpdateUserRequest) error {

	const op = "UserService.Update"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return err
	}

	err = s.rep.Update(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return err
	}

	s.log.Info(fmt.Sprintf("%s: user updated successfully", op))
	return nil
}

func (s *UserService) Delete(ctx context.Context,
	req dtos.DeleteUserRequest) error {
	const op = "UserService.Delete"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return err
	}

	err = s.rep.Delete(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return err
	}

	s.log.Info(fmt.Sprintf("%s: user deleted successfully", op))
	return nil
}

func (s *UserService) UpdateLogin(ctx context.Context,
	req dtos.UpdateLoginRequest) error {
	const op = "UserService.UpdateLogin"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return err
	}

	err = s.rep.UpdateLogin(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return err
	}

	s.log.Info(fmt.Sprintf("%s: user updated successfully", op))
	return nil
}

func (s *UserService) UpdatePassword(ctx context.Context,
	req dtos.UpdatePasswordRequest) error {
	const op = "UserService.UpdatePassword"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return err
	}

	err = s.rep.UpdatePassword(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return err
	}

	s.log.Info(fmt.Sprintf("%s: user updated successfully", op))
	return nil
}

func (s *UserService) UpdateUsername(ctx context.Context,
	req dtos.UpdateUsernameRequest) error {
	const op = "UserService.UpdateUsername"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return err
	}

	err = s.rep.UpdateUsername(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return err
	}

	s.log.Info(fmt.Sprintf("%s: user updated successfully", op))
	return nil
}
