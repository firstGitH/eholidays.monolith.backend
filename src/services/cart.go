package services

import (
	"context"
	"fmt"
	"log/slog"

	errors "eholidays.monolith/src/domain"
	dtos "eholidays.monolith/src/domain/DTOs"
	"eholidays.monolith/src/storage/postgres/repositories"
	"github.com/go-playground/validator/v10"
)

type CartService struct {
	log       *slog.Logger
	rep       repositories.CartRepository
	validator *validator.Validate
}

func NewCartService(
	log *slog.Logger,
	rep repositories.CartRepository,
) *CartService {
	return &CartService{
		log:       log,
		rep:       rep,
		validator: validator.New(),
	}
}

func (s *CartService) Create(ctx context.Context,
	req dtos.CreateCartItemRequest) (dtos.CreateCartItemResponse, error) {
	const op = "CartService.Create"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.CreateCartItemResponse{}, err
	}

	resp, err := s.rep.Create(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.CreateCartItemResponse{}, err
	}

	s.log.Info(fmt.Sprintf("%s: user created successfully", op))
	return resp, nil
}

func (s *CartService) Delete(ctx context.Context,
	req dtos.DeleteCartItemRequest) error {
	const op = "CartService.Delete"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return err
	}

	err = s.rep.Delete(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return err
	}

	s.log.Info(fmt.Sprintf("%s: user deleted successfully", op))
	return nil
}

func (s *CartService) GetItems(ctx context.Context,
	req dtos.GetCartItemsRequest) (dtos.GetCartItemsResponse, error) {
	const op = "CartService.GetItems"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetCartItemsResponse{}, err
	}

	resp, err := s.rep.GetItems(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetCartItemsResponse{}, err
	}

	s.log.Info(fmt.Sprintf("%s: items retrieved successfully", op))
	return resp, nil
}

func (s *CartService) GetTotalItems(ctx context.Context,
	req dtos.GetTotalCartItemRequest) (dtos.GetTotalCartItemsResponse, error) {
	const op = "CartService.GetTotalItems"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetTotalCartItemsResponse{}, err
	}

	resp, err := s.rep.GetTotalItems(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetTotalCartItemsResponse{}, err
	}

	s.log.Info(fmt.Sprintf("%s: items retrieved successfully", op))
	return resp, nil
}
