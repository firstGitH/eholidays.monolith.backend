package services

import (
	"context"
	"fmt"
	"log/slog"

	errors "eholidays.monolith/src/domain"
	dtos "eholidays.monolith/src/domain/DTOs"
	"eholidays.monolith/src/storage/postgres/repositories"
	"github.com/go-playground/validator/v10"
)

type NotificationService struct {
	log       *slog.Logger
	rep       repositories.NotificationRepository
	validator *validator.Validate
}

func NewNotificationService(
	log *slog.Logger,
	rep repositories.NotificationRepository,
) *NotificationService {
	return &NotificationService{
		log: log,
		rep: rep,
	}
}

func (s *NotificationService) Create(ctx context.Context,
	req dtos.CreateNotificationRequest) (dtos.CreateNotificationResponse, error) {
	const op = "NotificationService.Create"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.CreateNotificationResponse{}, err
	}

	resp, err := s.rep.Create(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.CreateNotificationResponse{}, err
	}

	s.log.Info(fmt.Sprintf("%s: notification created successfully", op))
	return resp, nil
}

func (s *NotificationService) GetItems(ctx context.Context,
	req dtos.GetNotificationsRequest) (dtos.GetNotificationsResponse, error) {
	const op = "NotificationService.GetItems"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetNotificationsResponse{}, err
	}

	resp, err := s.rep.GetItems(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetNotificationsResponse{}, err
	}

	s.log.Info(fmt.Sprintf("%s: notification retrieved successfully", op))
	return resp, nil
}

func (s *NotificationService) GetTotalItems(ctx context.Context) (
	dtos.GetTotalNotificationsResponse, error) {
	const op = "NotificationService.GetTotalItems"
	s.log.Info(fmt.Sprintf("%s: start", op))

	resp, err := s.rep.GetTotalItems(ctx)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetTotalNotificationsResponse{}, err
	}

	s.log.Info(fmt.Sprintf("%s: items retrieved successfully", op))
	return resp, nil
}

func (s *NotificationService) GetItemsByProduct(ctx context.Context,
	req dtos.GetNotificationsByProductRequest) (dtos.GetNotificationsByProductResponse, error) {
	const op = "NotificationService.GetItemsByProduct"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetNotificationsByProductResponse{}, err
	}

	resp, err := s.rep.GetItemsByProduct(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetNotificationsByProductResponse{}, err
	}

	s.log.Info(fmt.Sprintf("%s: items retrieved successfully", op))
	return resp, nil
}

func (s *NotificationService) GetTotalItemsByProduct(ctx context.Context,
	req dtos.GetTotalNotificationsByProductRequest) (
	dtos.GetTotalNotificationsByProductResponse, error) {
	const op = "NotificationService.GetTotalItemsByProduct"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetTotalNotificationsByProductResponse{}, err
	}

	resp, err := s.rep.GetTotalItemsByProduct(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetTotalNotificationsByProductResponse{}, err
	}

	s.log.Info(fmt.Sprintf("%s: items retrieved successfully", op))
	return resp, nil
}

func (s *NotificationService) Update(ctx context.Context,
	req dtos.UpdateNotificationRequest) error {
	const op = "CategoryService.Update"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return err
	}

	err = s.rep.Update(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return err
	}

	s.log.Info(fmt.Sprintf("%s: category updated successfully", op))
	return nil
}

func (s *NotificationService) Delete(ctx context.Context,
	req dtos.DeleteNotificationRequest) error {
	const op = "NotificationService.Delete"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return err
	}

	err = s.rep.Delete(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return err
	}

	s.log.Info(fmt.Sprintf("%s: notification deleted successfully", op))
	return nil
}
