package services

import (
	"context"
	"fmt"
	"log/slog"

	errors "eholidays.monolith/src/domain"
	dtos "eholidays.monolith/src/domain/DTOs"
	"eholidays.monolith/src/storage/postgres/repositories"
	"github.com/go-playground/validator/v10"
)

type CategoryService struct {
	log       *slog.Logger
	rep       repositories.CategoryRepository
	validator *validator.Validate
}

func NewCategoryService(
	log *slog.Logger,
	rep repositories.CategoryRepository,
) *CategoryService {
	return &CategoryService{
		log: log,
		rep: rep,
	}
}

func (s *CategoryService) Create(ctx context.Context,
	req dtos.CreateCategoryRequest) (dtos.CreateCategoryResponse, error) {
	const op = "CategoryService.Create"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.CreateCategoryResponse{}, err
	}

	resp, err := s.rep.Create(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.CreateCategoryResponse{}, err
	}

	s.log.Info(fmt.Sprintf("%s: category created successfully", op))
	return resp, nil
}

func (s *CategoryService) Delete(ctx context.Context,
	req dtos.DeleteCategoryRequest) error {
	const op = "CategoryService.Delete"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return err
	}

	err = s.rep.Delete(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return err
	}

	s.log.Info(fmt.Sprintf("%s: category deleted successfully", op))
	return nil
}

func (s *CategoryService) GetItems(ctx context.Context,
	req dtos.GetCategoriesRequest) (dtos.GetCategoriesResponse, error) {
	const op = "CategoryService.GetItems"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetCategoriesResponse{}, err
	}

	resp, err := s.rep.GetItems(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetCategoriesResponse{}, err
	}

	s.log.Info(fmt.Sprintf("%s: items retrieved successfully", op))
	return resp, nil
}

func (s *CategoryService) GetTotalItems(ctx context.Context) (dtos.GetTotalCategoriesResponse, error) {
	const op = "CategoryService.GetTotalItems"
	s.log.Info(fmt.Sprintf("%s: start", op))

	resp, err := s.rep.GetTotalItems(ctx)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetTotalCategoriesResponse{}, err
	}

	s.log.Info(fmt.Sprintf("%s: items retrieved successfully", op))
	return resp, nil
}
