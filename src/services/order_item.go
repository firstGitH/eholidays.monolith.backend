package services

import (
	"context"
	"fmt"
	"log/slog"

	errors "eholidays.monolith/src/domain"
	dtos "eholidays.monolith/src/domain/DTOs"
	"eholidays.monolith/src/storage/postgres/repositories"
	"github.com/go-playground/validator/v10"
)

type OrderItemService struct {
	log       *slog.Logger
	rep       repositories.OrderItemRepository
	validator *validator.Validate
}

func NewOrderItemService(
	log *slog.Logger,
	rep repositories.OrderItemRepository,
) *OrderItemService {
	return &OrderItemService{
		log:       log,
		rep:       rep,
		validator: validator.New(),
	}
}

func (s *OrderItemService) Create(ctx context.Context,
	req dtos.CreateOrderItemRequest) (dtos.CreateOrderItemResponse, error) {
	const op = "OrderItemService.Create"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.CreateOrderItemResponse{}, err
	}

	resp, err := s.rep.Create(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.CreateOrderItemResponse{}, err
	}

	s.log.Info(fmt.Sprintf("%s: order item created successfully", op))
	return resp, nil
}

func (s *OrderItemService) Get(ctx context.Context,
	req dtos.GetOrderItemRequest) (dtos.GetOrderItemResponse, error) {
	const op = "OrderItemService.Get"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetOrderItemResponse{}, err
	}

	resp, err := s.rep.Get(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetOrderItemResponse{}, err
	}

	s.log.Info(fmt.Sprintf("%s: order item retrieved successfully", op))
	return resp, nil
}

func (s *OrderItemService) GetItems(ctx context.Context,
	req dtos.GetOrderItemsByOrderRequest) (dtos.GetOrderItemsByOrderResponse, error) {
	const op = "OrderItemService.GetItems"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetOrderItemsByOrderResponse{}, err
	}

	resp, err := s.rep.GetItems(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetOrderItemsByOrderResponse{}, err
	}

	s.log.Info(fmt.Sprintf("%s: order item retrieved successfully", op))
	return resp, nil
}

func (s *OrderItemService) Update(ctx context.Context,
	req dtos.UpdateOrderItemRequest) error {
	const op = "OrderItemService.Update"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return err
	}

	err = s.rep.Update(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return err
	}

	s.log.Info(fmt.Sprintf("%s: order item updated successfully", op))
	return nil
}

func (s *OrderItemService) Delete(ctx context.Context,
	req dtos.DeleteOrderItemRequest) error {
	const op = "OrderItemService.Delete"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return err
	}

	err = s.rep.Delete(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return err
	}

	s.log.Info(fmt.Sprintf("%s: order item deleted successfully", op))
	return nil
}
