package services

import (
	"context"
	"fmt"
	"log/slog"

	errors "eholidays.monolith/src/domain"
	dtos "eholidays.monolith/src/domain/DTOs"
	"eholidays.monolith/src/storage/postgres/repositories"
	"github.com/go-playground/validator/v10"
)

type ReviewService struct {
	log       *slog.Logger
	repReview repositories.ReviewRepository
	validator *validator.Validate
}

func NewReviewService(
	log *slog.Logger,
	repReview repositories.ReviewRepository,
) *ReviewService {
	return &ReviewService{
		log:       log,
		repReview: repReview,
		validator: validator.New(),
	}
}

func (s *ReviewService) Create(ctx context.Context,
	req dtos.CreateReviewRequest) (dtos.CreateReviewResponse, error) {
	const op = "ReviewService.Create"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.CreateReviewResponse{}, err
	}

	resp, err := s.repReview.Create(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.CreateReviewResponse{}, err
	}

	s.log.Info(fmt.Sprintf("%s: review created successfully", op))

	return resp, nil
}

func (s *ReviewService) GetItems(ctx context.Context,
	req dtos.GetReviewsByProductRequest) (
	dtos.GetReviewsByProductResponse, error) {
	const op = "ReviewService.GetItems"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetReviewsByProductResponse{}, err
	}

	resp, err := s.repReview.GetItems(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetReviewsByProductResponse{}, err
	}

	s.log.Info(fmt.Sprintf("%s: product reviews retrieved successfully", op))

	return resp, nil
}

func (s *ReviewService) GetTotalItems(ctx context.Context,
	req dtos.GetTotalReviewsByProductRequest) (
	dtos.GetTotalReviewsByProductResponse, error) {
	const op = "ReviewService.GetTotalItems"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetTotalReviewsByProductResponse{}, err
	}

	resp, err := s.repReview.GetTotalItems(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetTotalReviewsByProductResponse{}, err
	}

	s.log.Info(fmt.Sprintf("%s: product reviews retrieved successfully", op))

	return resp, nil
}

func (s *ReviewService) Update(ctx context.Context,
	req dtos.UpdateReviewRequest) error {
	const op = "ReviewService.Update"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return err
	}

	err = s.repReview.Update(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return err
	}

	s.log.Info(fmt.Sprintf("%s: review updated successfully", op))

	return nil
}

func (s *ReviewService) Delete(ctx context.Context,
	req dtos.DeleteReviewRequest) error {
	const op = "ReviewService.Delete"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return err
	}

	err = s.repReview.Delete(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return err
	}

	s.log.Info(fmt.Sprintf("%s: review deleted successfully", op))
	return nil
}
