package services

import (
	"context"
	"fmt"
	"log/slog"

	errors "eholidays.monolith/src/domain"
	dtos "eholidays.monolith/src/domain/DTOs"
	"eholidays.monolith/src/storage/postgres/repositories"
	"github.com/go-playground/validator/v10"
)

type SubscriptionService struct {
	log       *slog.Logger
	rep       repositories.SubscriptionRepository
	validator *validator.Validate
}

func NewSubscriptionService(
	log *slog.Logger,
	rep repositories.SubscriptionRepository,
) *SubscriptionService {
	return &SubscriptionService{
		log:       log,
		rep:       rep,
		validator: validator.New(),
	}
}

func (s *SubscriptionService) Create(ctx context.Context,
	req dtos.CreateSubscriptionRequest) (dtos.CreateSubscriptionResponse, error) {
	const op = "SubscriptionService.Create"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.CreateSubscriptionResponse{}, err
	}

	resp, err := s.rep.Create(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.CreateSubscriptionResponse{}, err
	}

	s.log.Info(fmt.Sprintf("%s: subscription created successfully", op))
	return resp, nil
}

func (s *SubscriptionService) GetItemsByUser(ctx context.Context,
	req dtos.GetSubscriptionsByUserRequest) (dtos.GetSubscriptionsByUserResponse, error) {
	const op = "SubscriptionService.GetItemsByUser"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetSubscriptionsByUserResponse{}, err
	}

	resp, err := s.rep.GetItemsByUser(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetSubscriptionsByUserResponse{}, err
	}

	s.log.Info(fmt.Sprintf("%s: items retrieved successfully", op))
	return resp, nil
}

func (s *SubscriptionService) GetTotalItemsByUser(ctx context.Context,
	req dtos.GetTotalSubscriptionsByUserRequest) (
	dtos.GetTotalSubscriptionsByUserResponse, error) {
	const op = "SubscriptionService.GetTotalItemsByUser"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetTotalSubscriptionsByUserResponse{}, err
	}

	resp, err := s.rep.GetTotalItemsByUser(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetTotalSubscriptionsByUserResponse{}, err
	}

	s.log.Info(fmt.Sprintf("%s: total items retrieved successfully", op))
	return resp, nil
}

func (s *SubscriptionService) GetItems(ctx context.Context,
	req dtos.GetSubscriptionsRequest) (dtos.GetSubscriptionsResponse, error) {
	const op = "SubscriptionService.GetItems"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetSubscriptionsResponse{}, err
	}

	resp, err := s.rep.GetItems(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetSubscriptionsResponse{}, err
	}

	s.log.Info(fmt.Sprintf("%s: items retrieved successfully", op))
	return resp, nil
}

func (s *SubscriptionService) GetTotalItems(ctx context.Context) (
	dtos.GetTotalSubscriptionsResponse, error) {
	const op = "SubscriptionService.GetTotalItems"
	s.log.Info(fmt.Sprintf("%s: start", op))

	resp, err := s.rep.GetTotalItems(ctx)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.GetTotalSubscriptionsResponse{}, err
	}

	s.log.Info(fmt.Sprintf("%s: total items retrieved successfully", op))
	return resp, nil
}

func (s *SubscriptionService) Delete(ctx context.Context,
	req dtos.DeleteSubscriptionRequest) error {
	const op = "SubscriptionService.Delete"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return err
	}

	err = s.rep.Delete(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return err
	}

	s.log.Info(fmt.Sprintf("%s: subscription deleted successfully", op))
	return nil
}

func (s *SubscriptionService) HasSubscription(ctx context.Context, req dtos.HasSubscriptionRequest) (
	dtos.HasSubscriptionResponse, error) {
	const op = "SubscriptionService.HasSubscription"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.HasSubscriptionResponse{}, err
	}

	resp, err := s.rep.HasSubscription(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.HasSubscriptionResponse{}, err
	}

	s.log.Info(fmt.Sprintf("%s: check subscription successfully", op))
	return resp, nil
}
