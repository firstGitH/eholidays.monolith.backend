package services

import (
	"context"
	"fmt"
	"log/slog"

	errors "eholidays.monolith/src/domain"
	dtos "eholidays.monolith/src/domain/DTOs"
	"eholidays.monolith/src/storage/postgres/repositories"
	"github.com/go-playground/validator/v10"
)

type RoleCheckerService struct {
	log       *slog.Logger
	rep       repositories.UserRepository
	validator *validator.Validate
}

func NewRoleCheckerService(
	log *slog.Logger,
	rep repositories.UserRepository,
) *RoleCheckerService {
	return &RoleCheckerService{
		log:       log,
		rep:       rep,
		validator: validator.New(),
	}
}

func (s *RoleCheckerService) HasRoleByID(ctx context.Context,
	req dtos.UserHasRoleByIDRequest) (dtos.UserHasRoleByIDResponse, error) {
	const op = "RoleCheckerService.HasRoleByID"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.UserHasRoleByIDResponse{}, err
	}

	resp, err := s.rep.HasRoleByID(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.UserHasRoleByIDResponse{}, err
	}

	s.log.Info(fmt.Sprintf("%s: user role by ID found successfully", op))
	return resp, nil
}

func (s *RoleCheckerService) HasRoleByName(ctx context.Context,
	req dtos.UserHasRoleByNameRequest) (dtos.UserHasRoleByNameResponse, error) {
	const op = "RoleCheckerService.HasRoleByName"
	s.log.Info(fmt.Sprintf("%s: start", op))

	err := s.validator.Struct(req)
	if err != nil {
		err = fmt.Errorf("%w: %v", errors.ErrIncorrectData, err)
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.UserHasRoleByNameResponse{}, err
	}

	resp, err := s.rep.HasRoleByName(ctx, req)
	if err != nil {
		s.log.Error(fmt.Sprintf("%s: %v", op, err))
		return dtos.UserHasRoleByNameResponse{}, err
	}

	s.log.Info(fmt.Sprintf("%s: user role by name found successfully", op))
	return resp, nil
}
