package hashers

import "golang.org/x/crypto/bcrypt"

type PasswordHasher interface {
	HashPassword(password []byte) ([]byte, error)
	ComparePassword(password []byte, hashedPassword []byte) bool
}

type BcryptPasswordHasher struct{}

func (h *BcryptPasswordHasher) HashPassword(password []byte) ([]byte, error) {
	bytes, err := bcrypt.GenerateFromPassword(password, bcrypt.DefaultCost)
	return bytes, err
}

func (h *BcryptPasswordHasher) ComparePassword(password []byte, hashedPassword []byte) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password))
	return err == nil
}
