package tokens

import (
	"time"

	errors "eholidays.monolith/src/domain"
	"github.com/golang-jwt/jwt/v5"
)

type JwtUserDate struct {
	ID uint
}

type JwtService interface {
	Create(userData *JwtUserDate) (string, error)
	Check(token string) (userId uint, err error)
	Refresh(token string) (string, error)
}

type TokenService struct {
	secret string
	expiry time.Duration
}

func NewTokenService(secret string, expiry time.Duration) JwtService {
	return &TokenService{secret: secret, expiry: expiry}
}

func (tm *TokenService) Create(userData *JwtUserDate) (string, error) {

	if userData.ID == 0 {
		return "", errors.ErrUserNotFound
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"id":  userData.ID,
		"exp": time.Now().Add(tm.expiry).Unix(),
	})

	tokenString, err := token.SignedString([]byte(tm.secret))
	if err != nil {
		return "", err
	}

	return tokenString, nil
}

func (tm *TokenService) Check(token string) (userId uint, err error) {
	parsedToken, err := jwt.Parse(token, func(token *jwt.Token) (interface{}, error) {
		return []byte(tm.secret), nil
	})
	if err != nil {
		return 0, errors.ErrTokenParseFailed
	}

	claims, ok := parsedToken.Claims.(jwt.MapClaims)
	if !ok || !parsedToken.Valid {
		return 0, errors.ErrTokenInvalid
	}

	expTime := int64(claims["exp"].(float64))
	if time.Now().Unix() > expTime {
		return 0, errors.ErrTokenExpired
	}

	userId = uint(claims["id"].(float64))
	return userId, nil
}

func (tm *TokenService) Refresh(token string) (string, error) {
	userId, err := tm.Check(token)
	if err != nil {
		return "", err
	}

	return tm.Create(&JwtUserDate{ID: userId})
}
