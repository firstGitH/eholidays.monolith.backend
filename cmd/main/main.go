package main

import (
	"log"
	"log/slog"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"eholidays.monolith/src/config"
	"eholidays.monolith/src/http/middlewares"
	"eholidays.monolith/src/lib/tokens"
	"eholidays.monolith/src/services"
	"eholidays.monolith/src/storage/postgres"
	"eholidays.monolith/src/storage/postgres/repositories"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/cors"
)

func main() {

	// 1) init configuration: cleanenv
	cfg := config.MustLoad()

	// 2) init logger: slog
	logger := setupLogger(cfg.Environment)

	logger.Info("starting server...", slog.String("env", cfg.Environment))

	// 3) init storage: postgres
	db := postgres.NewDatabase(cfg.StoragePath)

	// 4) init repositories
	repUser := repositories.NewUserPgRepository(db)

	// 5) init services
	jwtService := tokens.NewTokenService(cfg.TokenSecret, cfg.TokenTTL)
	roleChecker := services.NewRoleCheckerService(logger, repUser)

	// 6) init handlers

	// 7) init router: chi
	router := setupRouter()

	corsCfg := setupCors()
	router.Use(corsCfg.Handler)

	router.Use(middleware.RequestID)
	router.Use(middleware.Logger)
	router.Use(middleware.Recoverer)
	router.Use(middleware.URLFormat)
	middlewares.NewAuthMiddleware(jwtService, roleChecker)
	// _) init routes
	// handlers.CartHandler(router)

	// _) run server
	logger.Info("starting server", slog.String("address", cfg.HttpServer.Adress))

	done := make(chan os.Signal, 1)
	signal.Notify(done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	srv := &http.Server{
		Addr:         cfg.HttpServer.Adress,
		Handler:      router,
		ReadTimeout:  cfg.HttpServer.Timeout,
		WriteTimeout: cfg.HttpServer.Timeout,
		IdleTimeout:  cfg.HttpServer.IdleTimeout,
	}

	go func() {
		if err := srv.ListenAndServe(); err != nil {
			log.Fatalf("failed to start server")
		}
	}()

	log.Println("server started")

	<-done

	log.Println("stopping server")
}

func setupRouter() *chi.Mux {
	router := chi.NewRouter()

	return router
}

func setupCors() *cors.Cors {
	config := cors.New(cors.Options{
		AllowedOrigins:   []string{"http://localhost"},
		AllowedMethods:   []string{"GET", "POST", "DELETE"},
		AllowedHeaders:   []string{"Origin", "Content-Length", "Content-Type", "Authorization"},
		ExposedHeaders:   []string{"Content-Length"},
		AllowCredentials: true,
	})

	return config
}

func setupLogger(env string) *slog.Logger {
	var log *slog.Logger

	switch env {
	case "local":
		log = slog.New(
			slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelDebug}),
		)
	case "dev":
		log = slog.New(
			slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelDebug}),
		)
	case "prod":
		log = slog.New(
			slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelInfo}),
		)
	}

	return log
}
